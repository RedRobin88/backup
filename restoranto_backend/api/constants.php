<?php 

// Login API
define('LOGIN_ERROR_EMAIL', 'Please enter email');
define('LOGIN_ERROR_PASSWORD', 'Please enter password');
define('LOGIN_ERROR_BAD_REQUEST', 'Invalid Email address or Password');
define('LOGIN_ERROR_USER_NOT_EXIST', 'User does not exist');
define('LOGIN_ERROR_INVALID_INPUT', 'Email or Password is not correct');
define('LOGIN_SUCCESS', 'Login Successfully');

// check_mail API
define('EMAIL_NOT_VALID', 'Email not valid');

// createUser API
define('USER_ENTER_NAME', 'Please enter your name');
define('USER_ENTER_PHONE', 'Please enter your phone number');
define('USER_ENTER_PHONE_VALID', 'Phone number must be 10 degites only');
define('USER_ENTER_PHONE_VALID_INT', 'Phone number must be integer');
define('USER_ENTER_EMAIL', 'Please enter email');
define('USER_ENTER_PASSWORD', 'Please enter password');
define('USER_SELECT_INTEREST', 'Please select interest');
define('USER_EXISTS', 'User already exists');
define('USER_CREATE_SUCCESS', 'User registered successfully');
define('USER_NOT_CREATE', 'User does not created');
define('USER_SELECT_DEVICE_TOKEN', 'Please enter device token');
define('USER_SELECT_DEVICE_TYPE', 'Please enter device type');

// getInterest API
define('USER_INTEREST_LIST', 'List of Interest');
define('USER_NO_INTEREST', 'No Interest Record');

// showDeals API
define('DEAL_ENTER_LATITUDE', 'Enter latitude');
define('DEAL_ENTER_LONGITUTE', 'Enter Longitute');
define('DEAL_ENTER_INTEREST_ID', 'Enter Interest ID');
define('DEAL_NOT_FOUND', 'No Deal Found');


// claimedDeal API
define('CLAIMED_DEAL_ENTER_ID', 'Please enter deal id');
define('CLAIMED_DEAL_ENTER_USERID', 'Please enter Userid');
define('CLAIMED_DEAL_SUCCESS', 'Deal claimed successfully');
define('CLAIMED_DEAL_FAILED', 'Deal not claimed');
define('CLAIMED_DEAL_EXPIRED', 'Deal expired');
define('CLAIMED_DEAL_NOT_VALID_LOGIN', 'Please login again');
define('CLAIMED_DEAL_ENTER_PEOPLE', 'Please enter number of people');


// Email sent API
define('EMAIL_SENT_SUCCESS', 'Email successfully sent');
define('EMAIL_SENT_FAILED', 'Email could not be sent');
define('CLAIMED_DEAL_ENTER_ID', 'Please enter deal id');

// forgotPassword API
define('FORGOT_ENTER_EMAIL', 'Please enter email');
define('FORGOT_EMAIL_SENT_SUCCESS', 'Email successfully sent');
define('FORGOT_EMAIL_SENT_FAILED', 'Email could not be sent');
define('FORGOT_EMAIL_NOT_EXIST', 'Email ID not exist');
define('FORGOT_INVALID_EMAIL', 'Invalid Email address');


// setPassword
define('SET_PWD_ENTER_OTP', 'Please enter OTP');
define('SET_PWD_ENTER_PWD', 'Please enter Password');
define('SET_PWD_SUCCESS', 'Password set successfully');
define('SET_PWD_WRONG_OTP', 'You entered wrong OTP');

// Choose lang
define('SET_LANG', 'Please enter langauge');
?>

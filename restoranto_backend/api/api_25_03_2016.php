<?php


	/* 
		This is an example class script proceeding secured API
		To use this class you should keep same as query string and function name
		Ex: If the query string value rquest=delete_user Access modifiers doesn't matter but function should be
		     function delete_user(){
				 You code goes here
			 }
		Class will execute the function dynamically;
		
		usage :
		
		    $object->response(output_data, status_code);
			$object->_request	- to get santinized input 	
			
			output_data : JSON (I am using)
			status_code : Send status message for headers
			
		Add This extension for localhost checking :
			Chrome Extension : Advanced REST client Application
			URL : https://chrome.google.com/webstore/detail/hgmloofddffdnphfgcellkdfbfbjeloo
		
		I used the below table for demo purpose.
		
		CREATE TABLE IF NOT EXISTS `users` (
		  `user_id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_fullname` varchar(25) NOT NULL,
		  `user_email` varchar(50) NOT NULL,
		  `user_password` varchar(50) NOT NULL,
		  `user_status` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`user_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 	*/
	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "etpl2012_resto";
		const DB_PASSWORD = "1n16L[y@,N@E";
		const DB = "etpl2012_restoranto";
		const apikey = 'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk';
		const ASSETS_URL = "http://exceptionaire.co/restoranto/";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->checkApiKey();
			$this->dbConnect();					// Initiate Database connection
		}
		public function checkApiKey(){ 
				$headers = getallheaders();
				
				$token = (isset($headers['X-Api-Token']))?$headers['X-Api-Token']:'';
				if($token != self::apikey){
					$error['Status'] = 'Failed';
					$error['msg'] = 'Api key not matches';
					$this->response($this->json($error),404);
				}
		}
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *	Simple login API
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		
		private function login(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];		
			$password = $this->_request['password'];
			
			// Input validations
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email,phoneno,interest FROM restoranto_users WHERE status = 1 and email = '$email' AND password = '".md5($password)."' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$result['status'] = "Success";
						$result['msg'] = "Login Successfully";
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result), 200);
					} else {
						
						$sql1 = mysql_query("SELECT id,name,email,phoneno,interest FROM restoranto_users WHERE status = 1 and email = '$email' OR password = '".md5($password)."' LIMIT 1", $this->db);
						if(mysql_num_rows($sql1) > 0){
							$error['status'] = 'Failed';	
							$error['msg'] = 'Email or Password is not correct';
							$this->response($this->json($error), 404);
						} else {
							$error['status'] = 'Failed';	
							$error['msg'] = 'User not exist';
							$this->response($this->json($error), 404);
						}	
						//$this->response('', 204);	// If no records "No Content" status
					}
				}
			}
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter email ';
				$this->response($this->json($error), 404);
			}	
			if(empty($password)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter password ';
				$this->response($this->json($error), 404);
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}
		
		//function for echeck user email id
		private function check_mail(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter email ';
				$this->response($this->json($error), 404);
			}
			
			if(!empty($email) ){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email FROM restoranto_users WHERE email = '$email'  LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$result['status'] = "Success";
						$result['msg'] = "Login Successfully";
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result), 200);
					} else {
						$error['status'] = 'Failed';	
						$error['msg'] = 'User not exist';
						$this->response($this->json($error), 404);
						//$this->response('', 204);	// If no records "No Content" status
					}
				}else{
					$error['status'] = 'Failed';	
					$error['msg'] = 'Email not valid';
					$this->response($this->json($error), 404);
				}
			}else{
				$error['status'] = 'Failed';	
				$error['msg'] = 'Email not valid';
				$this->response($this->json($error), 404);
			}
		} 
		/*
		private function users(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$sql = mysql_query("SELECT user_id, user_fullname, user_email FROM users WHERE user_status = 1", $this->db);
			if(mysql_num_rows($sql) > 0){
				$result = array();
				while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
					$result[] = $rlt;
				}
				// If success everythig is good send header as "OK" and return list of users in JSON format
				$this->response($this->json($result), 200);
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
		private function deleteUser(){
			// Cross validation if the request method is DELETE else it will return "Not Acceptable" status
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){				
				mysql_query("DELETE FROM users WHERE user_id = $id");
				$success = array('status' => "Success", "msg" => "Successfully one record deleted.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}
		*/
		/*
		 *	Encode array into JSON
		*/
		
		//-------------- function for insert Records -----------------------------------------------//
		
		private function createUser(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			//print_r($this->_request);die;mysql_num_rows
			$mobileno = (isset($this->_request['phoneno']) && trim($this->_request['phoneno']) !='')?$this->_request['phoneno']:'';
			$email = (isset($this->_request['email']) && trim($this->_request['email']) !='')?$this->_request['email']:'';
			$name = (isset($this->_request['name']) && trim($this->_request['name']) !='')?$this->_request['name']:'';
			$password = (isset($this->_request['password']) && trim($this->_request['password']) !='')?$this->_request['password']:'';
			$password = md5($password);
			$status = 1;	
			$userType = 1;
			$interest = $this->_request['interest'];
			$registerDate = date('Y-m-d H:i:s');
			$lastVisitDate  = date('Y-m-d H:i:s');
				
			//validation to check empty value
			if(empty($name)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter your name ';
				$this->response($this->json($error), 404);
			}else if(empty($mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter your phone number ';
				$this->response($this->json($error), 404);
			}else if(strlen($mobileno) !=10){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Phone number must be 10 degites only';
				$this->response($this->json($error), 404);
			}else if(!is_numeric($mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Phone number must be intiger';
				$this->response($this->json($error), 404);
			}else if(!preg_match("/^[0-9]{10}$/i", $mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Phone number must be intiger';
				$this->response($this->json($error), 404);
			}
			if(empty($email)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter email ';
				$this->response($this->json($error), 404);
			} 
			if(empty($password)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter password ';
				$this->response($this->json($error), 404);
			} 
			if(empty($interest)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please select interest ';
				$this->response($this->json($error), 404);
			}
			
			

			if(!empty($name) && !empty($mobileno) && !empty($email) && !empty($password)){ // check recorrd already exits in table
				
				//echo "SELECT id, username, username,dob,monumber FROM wad_users WHERE monumber = '".$mobileno."' AND dob='".$newDate."' LIMIT 1";die;
				$sql = mysql_query("SELECT * FROM restoranto_users WHERE email = '".$email."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$rowRes = mysql_fetch_assoc($sql);
						
						$success['status'] = 'Failed';	
						$success['msg'] = 'User already exists';
						
						$userdata['id'] = $rowRes['id'];
						$userdata['name'] = $rowRes['name'];
						$userdata['email'] = $rowRes['email'];
						$userdata['phoneno'] = $rowRes['phoneno'];
						
						
						$success['data'] = $userdata;
						$this->response($this->json($success), 200);
					}
					
				$sql = "INSERT INTO restoranto_users (name,email,phoneno,password,interest,registerDate,lastVisitDate,userType,status) values('".$name."','".$email."','".$phoneno."','".$password."','".$interest."','".$registerDate."','".$lastVisitDate."','".$userType."','".$status."')";
				$userdata = array();
				if(mysql_query($sql)){
					$userid = mysql_insert_id(); 
					
					$userdata['userid'] = $userid;
					
					self::sendNotificationEmail( $name, $email ); 
					
					$success = array('status' => "Success", "msg" => "User created successfully",'data'=>$userdata);
					$this->response($this->json($success),200);
				}else{
					$error = array('status' => "Failed", "msg" => "User does not created ");
					$this->response($this->json($error),404);
				}
				
			}	
		}
		
		//get Interest list 
		private function getInterest(){ 
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
				
			$sql_interest = mysql_query("SELECT id,name FROM restoranto_interest where status = 1");
			//$res_interest = mysql_num_rows($sql_interest);
			
			if(mysql_num_rows($sql_interest) > 0){ 
				$flag = 1;
				while($interestAll = mysql_fetch_array($sql_interest,MYSQL_ASSOC)){
					$resultInterestAll[] = $interestAll;
				}
			} else {
				$flag = 0;
			}		
			if($flag){
					$result['status'] = 'Success';	
					$result['msg'] 	  = 'List of Interest';
					$result['data'] = $resultInterestAll;
					// If success everythig is good send header as "OK" and return list of users in JSON format
					$this->response($this->json($result), 200);
			}else{
					$result['status'] = 'Failed';	
					$result['msg'] 	  = 'No Interest Record';
					$this->response($this->json($result), 204);
			}
			// If no records "No Content" status
		}
		
		

				
		private function uploadDeal(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			//print_r($this->_request);die;mysql_num_rows
			$type_of_deal = (isset($this->_request['type_of_deal']) && trim($this->_request['type_of_deal']) !='')?$this->_request['type_of_deal']:'';
			$other_type_of_deal = (isset($this->_request['other_type_of_deal']) && trim($this->_request['other_type_of_deal']) !='')?$this->_request['other_type_of_deal']:'';
			$discount = (isset($this->_request['discount']) && trim($this->_request['discount']) !='')?$this->_request['discount']:'';
			$image = (isset($_FILES['image']['name']) && trim($_FILES['image']['name']) !='')?$_FILES['image']['name']:'';
			
			$n_of_d_claimed = (isset($this->_request['n_of_d_claimed']) && trim($this->_request['n_of_d_claimed']) !='')?$this->_request['n_of_d_claimed']:'';
			$n_of_people_claimed = (isset($this->_request['n_of_people_claimed']) && trim($this->_request['n_of_people_claimed']) !='')?$this->_request['n_of_people_claimed']:'';
			$dealdate = (isset($this->_request['dealdate']) && trim($this->_request['dealdate']) !='')?$this->_request['dealdate']:'';
			$lat = (isset($this->_request['lat']) && trim($this->_request['lat']) !='')?$this->_request['lat']:'';
			$lng = (isset($this->_request['lng']) && trim($this->_request['lng']) !='')?$this->_request['lng']:'';
						
			$status = 1;	
			$added_date = date('Y-m-d H:i:s');
				
			//validation to check empty value
			if(empty($type_of_deal)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Select type of deal ';
				$this->response($this->json($error), 404);
			}else if(empty($discount)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter discount ';
				$this->response($this->json($error), 404);
			}else if(empty($image)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please upload deal image ';
				$this->response($this->json($error), 404);
			} 
			if(empty($n_of_d_claimed)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Number of deals which can be claimed ';
				$this->response($this->json($error), 404);
			} 
			if(empty($n_of_people_claimed)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Number of people for a deal which can be claimed ';
				$this->response($this->json($error), 404);
			}
			if(empty($dealdate)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please select deal date ';
				$this->response($this->json($error), 404);
			}
			if(empty($lat)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please select deal location properly ';
				$this->response($this->json($error), 404);
			}
			if(empty($lng)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please Please select deal location properly ';
				$this->response($this->json($error), 404);
			}
			
				$document_root = '/home/etpl2012/public_html/restoranto/assets/images';
					$target_dir = $document_root.'/deals/';
					$path_parts 	= pathinfo($_FILES["postimage"]["name"]);
					//$this->response($this->json(array("file name"=>$_FILES)), 404);die;
					$imageFileType 	= $path_parts['extension'];
					$create_new_name = basename($path_parts['filename']).'_'.time().'.'.$imageFileType;
					$target_file 	= $target_dir.$create_new_name ;
					if(is_dir($target_dir) == false){
						chmod($document_root,0777);
						mkdir($target_dir,0777);
					}
					$target_thumbs_http_file ='';
					if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
						$target_http_file = self::ASSETS_URL.'assets/images/deals/'.$create_new_name;
						include_once("function.php");
						$thum_path =  $target_dir.'thumbs/';
						$target_file_thumb 	= $thum_path.$create_new_name;
						if(is_dir($thum_path) == false){
								mkdir($thum_path,0777);
						}
						$wmax = 100;
						$hmax = 100;
						ak_img_resize($target_file, $target_file_thumb, $wmax, $hmax, $imageFileType);
						$target_thumbs_http_file = self::ASSETS_URL.'assets/images/deals/thumbs/'.$create_new_name;
					}else{
						$error['status'] = 'Failed';	
						$error['msg'] = 'Image is not uploded';
						$this->response($this->json($error), 404);
					}	
					
				$sql = "INSERT INTO restoranto_upload_deals (type_of_deal,other_type_of_deal,discount,image,n_of_d_claimed,n_of_people_claimed,status,dealdate,added_date,lat,lng) values('".$type_of_deal."','".$other_type_of_deal."','".$discount."','".$target_http_file."','".$n_of_d_claimed."','".$n_of_people_claimed."','".$status."','".$dealdate."','".$added_date."','".$lat."','".$lng."')";
				$dealdata = array();
				if(mysql_query($sql)){
					$dealid = mysql_insert_id(); 
					
					$dealdata['dealid'] = $dealid;
					
					//self::sendNotificationEmail( $name, $email ); 
					
					$success = array('status' => "Success", "msg" => "Deal uploaded successfully",'data'=>$dealdata);
					$this->response($this->json($success),200);
				}else{
					$error = array('status' => "Failed", "msg" => "Deal not uploaded ");
					$this->response($this->json($error),404);
				}				
		}
		
		
		private function showDeals(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			//$userid = $this->_request['userid'];	
			$interestid = $this->_request['interestid'];	
			$lat = $this->_request['lat'];
			$lng = $this->_request['lng'];
			
			if(empty($lat)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Enter latitude ';
				$this->response($this->json($error), 404);
			}else if(empty($lng)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Enter Longitute ';
				$this->response($this->json($error), 404);
			}else if(empty($interestid)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Enter Interest ID ';
				$this->response($this->json($error), 404);
			} 
			
			$time = date('Y/m/d H:i:s', strtotime('+4 hour'));
			$sql = mysql_query("SELECT *, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals WHERE type_of_meal = ".$interestid." HAVING distance < 60 ORDER BY distance", $this->db);
			
			if(mysql_num_rows($sql) > 0){
				$result = array();
				$result_deal = array();
				$i = 0;
				
				$resultlatlng = array();
				$j=0;
				while($result_latlng = mysql_fetch_array($sql,MYSQL_ASSOC)){	
					
					$timedif = explode(":",$result_latlng['timedif']);
					//print_r($timedif);
					if($timedif[0] <= '04') {  
						$resultlatlng[$j]['lat'] = $result_latlng['lat'];
						$resultlatlng[$j]['lng'] = $result_latlng['lng'];
						$resultlatlng[$j]['distance'] = $result_latlng['distance'];
						$j++;
					}
					
				}	
				
				if($j == 0) {
					// If no records "No Content" status
					$error = array('status' => "Failed", "msg" => "No Deal Found");
					$this->response($this->json($error), 400);
				}		
				
				$output = array_map("unserialize",array_unique(array_map("serialize", $resultlatlng)));
				//echo "<pre>"; print_r(array_values($output)); 
				$latlang_unq_array = array_values($output);
				$k=0;
				foreach($latlang_unq_array as $latlng) {
					$lat1 = $latlng['lat'];
					$lng1 = $latlng['lng'];
					$distance = $latlng['distance'];
					$sql1 = mysql_query("SELECT * FROM restoranto_upload_deals where lat = $lat1 and lng = $lng1", $this->db);
					$result1[$k]['lat'] = $latlng['lat'];
					$result1[$k]['lng'] = $latlng['lng'];
					$result1[$k]['distance'] = $latlng['distance'];
					if(mysql_num_rows($sql1) > 0){
						$result_deal1 = array(); 
						$l =0; 
						while($result_qry1 = mysql_fetch_array($sql1,MYSQL_ASSOC)){
							// Check whether user can able to claimed the deal or not
							$sql_claimed_cnt = mysql_query("SELECT count(*) as total_claimed_deals FROM restoranto_claimed_deals WHERE deal_id = '".$result_qry1['id']."'", $this->db);
							if(mysql_num_rows($sql_claimed_cnt) > 0){
									$result_claimed_cnt = mysql_fetch_array($sql_claimed_cnt,MYSQL_ASSOC);
									$total_claimed_deals = $result_claimed_cnt['total_claimed_deals'];
							}		
							$total_available_deals = $result_qry1['n_of_d_claimed']-$total_claimed_deals;
							if($total_available_deals > 0) {			
								$result_deal1['deal'][$l]['id'] = $result_qry1['id'];
								$result_deal1['deal'][$l]['restaurant'] = $result_qry1['restaurant'];
								$result_deal1['deal'][$l]['no_of_deals_can_be_claimed'] = $result_qry1['n_of_d_claimed'];
								$result_deal1['deal'][$l]['no_of_people_for_deal'] = $result_qry1['n_of_people_claimed'];
								$result_deal1['deal'][$l]['discount'] = $result_qry1['discount'];
								$result_deal1['deal'][$l]['image'] = $result_qry1['image'];
								$result_deal1['deal'][$l]['dealdate'] = $result_qry1['dealdate'];
								$result_deal1['deal'][$l]['currenttime'] = $time;
								$perdealdate = $result_qry1['dealdate'];
								$result_deal1['deal'][$l]['dealendtime'] = date('Y/m/d H:i:s', strtotime("$perdealdate+4 hour"));
								$dealenddatetime = date('Y/m/d H:i:s', strtotime("$perdealdate+4 hour"));
								$to_time = strtotime("$dealenddatetime");
								$from_time = strtotime("$time");
								$minutes_left = round(abs($to_time - $from_time) / 60,2);
								$result_deal1['deal'][$l]['deal_left_minutes'] = $minutes_left;
								
								$result1[$k]['offer'] = $result_deal1;
							}
							$l++;
						}	
					}	
					$k++;
				}		
				$this->response($this->json($result1), 200);
				/*while($result_qry = mysql_fetch_array($sql,MYSQL_ASSOC)){	
					
					//$result_qry = mysql_fetch_array($sql,MYSQL_ASSOC);
					// If success everythig is good send header as "OK" and user details
					$result[$i]['id'] = $result_qry['id'];
					$result[$i]['lat'] = $result_qry['lat'];
					$result[$i]['lng'] = $result_qry['lng'];
					$result_deal['deal']['restaurant'] = $result_qry['restaurant'];
					$result_deal['deal']['discount'] = $result_qry['discount'];
					$result_deal['deal']['image'] = $result_qry['image'];
					$result_deal['deal']['dealdate'] = $result_qry['dealdate'];
					$result[$i]['offer'] = $result_deal;
					$i++;
				}	
					$this->response($this->json($result), 200);
				*/
			} else {
				// If no records "No Content" status
				$error = array('status' => "Failed", "msg" => "No Deal Found");
				$this->response($this->json($error), 400);
			}	
			
			
			
		}
		
		private function forgotPassword(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];		
			//$name = $this->_request['name'];	
			//$userid = $this->_request['userid'];		
					
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter email ';
				$this->response($this->json($error), 404);
			}				
			// Input validations
			if(!empty($email)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email,phoneno,interest FROM restoranto_users WHERE status = 1 and email = '$email'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$name = $result['name'];
						$userid = $result['id'];
						$otp = mt_rand();
						$to 	     = $email;
					    $subject  = "Forgot Password";
					    $message  = "Hello ".$name.", <br /><br />\r\n";
					    //$message .= "Thank you for registering at Restoranto.<br />\r\n";
					    $message .= "Your OTP is ".$otp."<br />\r\n";
					    $message .= "You can use this OTP to set new password in Restoranto APP<br /><br /><br />\r\n";
					    $message .= "Thanks and Regards,<br />Restoranto<br /><br /><br />\r\n";
					    $headers  = 'MIME-Version: 1.0' . PHP_EOL;
					    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					  // More headers
					   $headers .= "From:shobha@exceptionaire.co \r\n";
					   $retval 	 = mail($to,$subject,$message,$headers);
							   
					   if( $retval == true )  
					   {
						    $query_update 		= "UPDATE restoranto_users SET otp = '".$otp."' WHERE id = $userid ";
							$sql_update 		= mysql_query($query_update) or die(mysql_error());
							
							$result['status'] = 'Success';	
							$result['msg'] 	  = 'Email successfully sent..';			
							$this->response($this->json($result), 200);			
					   }
					   else
					   {
							$error['status'] = 'Failed';	
							$error['msg'] 	 = 'Email could not be sent..';
							$this->response($this->json($error), 509);
					   }
					} else {
						$error['status'] = 'Failed';	
						$error['msg'] = 'Email ID not exist';
						$this->response($this->json($error), 404);
						//$this->response('', 204);	// If no records "No Content" status
					}
				} else {
					// If invalid inputs "Bad Request" status message and reason
					$error = array('status' => "Failed", "msg" => "Invalid Email address");
					$this->response($this->json($error), 400);
				}	
			}
			
			
		}
		private function setPassword(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$otp = $this->_request['otp'];		
			$password = $this->_request['password'];	
					
			if(empty($otp)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter OTP ';
				$this->response($this->json($error), 404);
			} else if(empty($password)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Password ';
				$this->response($this->json($error), 404);
			}	
			
			if(!empty($otp) && !empty($password)){
				$sql = mysql_query("SELECT id,name,email,phoneno,interest FROM restoranto_users WHERE status = 1 and otp = '$otp'", $this->db);
				if(mysql_num_rows($sql) > 0){
					$result = mysql_fetch_array($sql,MYSQL_ASSOC);
					
					$userid = $result['id'];
					$query_update 		= "UPDATE restoranto_users SET password = '".md5($password)."',otp='' WHERE id = $userid";
					$sql_update 		= mysql_query($query_update) or die(mysql_error());
							
					$result['status'] = 'Success';	
					$result['msg'] 	  = 'Password set successfully.';			
					$this->response($this->json($result), 200);	
				} else {
					$error['status'] = 'Failed';	
					$error['msg'] = 'You entered wrong OTP ';
					$this->response($this->json($error), 404);
				}		
			}					
		}	
		
		private function claimedDeal(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			
			$deal_id = $this->_request['deal_id'];		
			$userid = $this->_request['userid'];
			$claimed_datetime = date('Y-m-d H:i:s');
								
			if(empty($deal_id)) {
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter deal id ';
				$this->response($this->json($error), 404);
			} else if(empty($userid)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Userid ';
				$this->response($this->json($error), 404);
			}	
			
			// Fetch merchant and user email id to send email for verification
			$sql = mysql_query("SELECT id,name,email,phoneno FROM restoranto_users WHERE status = 1 and id = '$userid'", $this->db);
			if(mysql_num_rows($sql) > 0){
					$result = mysql_fetch_array($sql,MYSQL_ASSOC);
					$username = $result['name'];
					$useremail = $result['email'];
					$userphoneno = $result['phoneno'];
			}	
			
			$sql1 = mysql_query("SELECT restoranto_upload_deals.n_of_d_claimed as n_of_d_claimed,restoranto_admin_merchant_users.email as email,restoranto_admin_merchant_users.company_name as company_name,restoranto_admin_merchant_users.company_phoneno as company_phoneno,restoranto_upload_deals.restaurant as restaurant,restoranto_upload_deals.discount as discount,restoranto_upload_deals.dealdate as dealdate 
								FROM restoranto_upload_deals,restoranto_admin_merchant_users 
								WHERE restoranto_upload_deals.merchant_id = restoranto_admin_merchant_users.id and restoranto_upload_deals.id = '$deal_id'", $this->db);
			if(mysql_num_rows($sql1) > 0){
					$result1 = mysql_fetch_array($sql1,MYSQL_ASSOC);
					$merchant_email = $result1['email'];
					$merchant_company = $result1['company_name'];
					$merchant_company_phoneno = $result1['company_phoneno'];
					$deal_restaurant = $result1['restaurant'];
					$deal_discount = $result1['discount'];
					$deal_dealdate = $result1['dealdate'];
					$deal_n_of_d_claimed = $result1['n_of_d_claimed'];
			}
				
			// Check whether user can able to claimed the deal or not
			$sql_claimed_cnt = mysql_query("SELECT count(*) as total_claimed_deals FROM restoranto_claimed_deals WHERE deal_id = '$deal_id'", $this->db);
			if(mysql_num_rows($sql_claimed_cnt) > 0){
					$result_claimed_cnt = mysql_fetch_array($sql_claimed_cnt,MYSQL_ASSOC);
					$total_claimed_deals = $result_claimed_cnt['total_claimed_deals'];
			}		
			
			$total_available_deals = $deal_n_of_d_claimed-$total_claimed_deals;
				
			if($total_available_deals >0) {	
					$sql = "INSERT INTO restoranto_claimed_deals (id,deal_id,userid,claimed_datetime) values('','".$deal_id."','".$userid."','".$claimed_datetime."')";
					$userdata = array();
					if(mysql_query($sql)){
						$userid = mysql_insert_id(); 
						
					   $userdata['deal_id'] = $deal_id;
						
					   $useremail = "dattu@exceptionaire.co";
					   $merchant_email = "dattulondhe@gmail.com";
					   if($useremail) { 	
							// For User email content
						   $subject_user  = "$merchant_company deal confirmed!";
						   $message_user  = "Hello ".$merchant_company.", <br /><br />\r\n";
						   $message_user .= "$deal_restaurant deal confirmed by you at $claimed_datetime.<br />\r\n<br />";
						   $message_user .= "Deal Details  <br />Discount : $deal_discount<br /> Date : $deal_dealdate<br />\r\n<br />";
						   $message_user .= "$deal_restaurant Phone No. $merchant_company_phoneno .<br />\r\n<br />";
						   $message_user .= "Regards,<br />Restoranto Team.";
						  
						  self::sendVerificationEmail( $useremail, $subject_user, $message_user ); 
						}
					
					   if($merchant_email) {
						   // For Merchant email contnet	
						   $subject_merchant  = "Deal claimed by $username - $userphoneno !";
						   $message_merchant  = "Hello ".$username.", <br /><br />\r\n";
						   $message_merchant .= "Deal claimed by $username - $userphoneno.<br />\r\n<br />";
						   $message_merchant .= "Deal Details  <br />Discount : $deal_discount<br /> Date : $deal_dealdate<br />\r\n<br />";
						   $message_merchant .= "User Details <br />  Name : $username<br /> Email : $useremail <br />Phone no. : $userphoneno<br/> \r\n<br />";
						   $message_merchant .= "Regards,<br />Restoranto Team.";
						   self::sendVerificationEmail( $merchant_email, $subject_merchant, $message_merchant); 
						}
						
						$success = array('status' => "Success", "msg" => "Deal claimed successfully",'data'=>$userdata);
						$this->response($this->json($success),200);
					}else{
						$error = array('status' => "Failed", "msg" => "Deal not claimed ");
						$this->response($this->json($error),404);
					}
			} else {
				$error = array('status' => "Failed", "msg" => "Deal expired");
				$this->response($this->json($error),404);
			}	
		}	
		
		
		private function sendNotificationEmail( $name, $email )
		{
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$error['status'] 	= 'Failed';	
				$error['msg'] 		= 'Not Acceptable';
				$this->response($this->json($error), 400);
			}
		   $to 	     = $email;
		   $subject  = "User Registration";
		   $message  = "Hello ".$name.", <br /><br />\r\n";
		   $message .= "Thank you for registering at Restoranto.<br />\r\n";
		   $message .= "Your account is created and must be verified before you can use it.<br />\r\n";
		   $message .= "Once that account has been activated you may login to http://exceptionaire.co/restoranto/ using your email and password:<br /><br /><br />\r\n";
		   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
		   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		  // More headers
		   $headers .= "From:shobha@exceptionaire.co \r\n";
		   $retval 	 = mail($to,$subject,$message,$headers);
				   
		   if( $retval == true )  
		   {
				$result['status'] = 'Success';	
				$result['msg'] 	  = 'Email successfully sent..';						
		   }
		   else
		   {
				$error['status'] = 'Failed';	
				$error['msg'] 	 = 'Email could not be sent..';
				$this->response($this->json($error), 509);
		   }
			
		}
		
		private function sendVerificationEmail( $to, $subject, $content )
		{
		   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
		   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		  // More headers
		   $headers .= "From:shobha@exceptionaire.co \r\n";
		   $retval 	 = mail($to,$subject,$content,$headers);
				   
		   if( $retval == true )  
		   {
				$result['status'] = 'Success';	
				$result['msg'] 	  = 'Email successfully sent..';						
		   }
		   else
		   {
				$error['status'] = 'Failed';	
				$error['msg'] 	 = 'Email could not be sent..';
				$this->response($this->json($error), 509);
		   }
			
		}
		
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>

<?php
$passphrase = "123456";
$message = "Sample notification ETPL test.";
$tokenArr[]["device_token"]	= '3cd75301f4f5f1b8b5f3bc83f535d043a6fce732ec613f3c4a520fa3efbeb577';		
//$tokenArr[]["device_token"]	= '7508e1c1e660466f658f0974263a0b30709e8a554cfeca1f72343b7203592d3a';		

/*
$tokenArr[0]["device_token"]	= '3cd75301f4f5f1b8b5f3bc83f535d043a6fce732ec613f3c4a520fa3efbeb577';		
$tokenArr[1]["device_token"]	= '3cd75301f4f5f1b8b5f3bc83f535d043a6fce732ec613f3c4a520fa3efbeb577';
*/
foreach($tokenArr AS $token){
	$deviceToken = $token["device_token"];
	$ctx = stream_context_create();
	stream_context_set_option($ctx, "ssl", "local_cert", "pushcert.pem");
	//stream_context_set_option($ctx, "ssl", "local_cert", "Brilliantel_DEV.pem");
	stream_context_set_option($ctx, "ssl", "passphrase", $passphrase);

	$fp = stream_socket_client("ssl://gateway.sandbox.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	//$fp = stream_socket_client("ssl://gateway.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);
	
	$body["aps"] = array("alert" => $message,"sound" => "default");
	$payload = json_encode($body);
	$msg = chr(0) . pack("n", 32) . pack("H*", $deviceToken) . pack("n", strlen($payload)) . $payload;
	$result = fwrite($fp, $msg, strlen($msg));
	if(!$result){   
		$ip++;
	}else{   
		$jp++;
	}
	fclose($fp);
}
exit;
?>

<?php 

// Login API
define('LOGIN_ERROR_EMAIL', 'Vul e-mail');
define('LOGIN_ERROR_PASSWORD', 'Voer wachtwoord in alstublieft');
define('LOGIN_ERROR_BAD_REQUEST', 'Ongeldig e-mail adres of wachtwoord');
define('LOGIN_ERROR_USER_NOT_EXIST', 'Gebruiker bestaat niet');
define('LOGIN_ERROR_INVALID_INPUT', 'E-mailadres of wachtwoord is niet correct');
define('LOGIN_SUCCESS', 'login succesvol');

// check_mail API
define('EMAIL_NOT_VALID', 'Email niet geldig');

// createUser API
define('USER_ENTER_NAME', 'Voer uw naam in');
define('USER_ENTER_PHONE', 'Vul hier uw telefoonnummer');
define('USER_ENTER_PHONE_VALID', 'Telefoonnummer mag slechts 10 cijfers bestaan');
define('USER_ENTER_PHONE_VALID_INT', 'Telefoonnummer moet integer zijn');
define('USER_ENTER_EMAIL', 'Vul e-mail');
define('USER_ENTER_PASSWORD', 'Voer wachtwoord in alstublieft');
define('USER_SELECT_INTEREST', 'Maak uw keuze rente');
define('USER_EXISTS', 'Gebruiker bestaat al');
define('USER_CREATE_SUCCESS', 'De registratie is voltooid');
define('USER_NOT_CREATE', 'De gebruiker heeft niet gemaakt');
define('USER_SELECT_DEVICE_TOKEN', 'Vul apparaat token');
define('USER_SELECT_DEVICE_TYPE', 'Vul hier het type apparaat');

// getInterest API
define('USER_INTEREST_LIST', 'Lijst van Interest');
define('USER_NO_INTEREST', 'Geen interesse Record');

// showDeals API
define('DEAL_ENTER_LATITUDE', 'Voer Latitude');
define('DEAL_ENTER_LONGITUTE', 'Voer Longitude');
define('DEAL_ENTER_INTEREST_ID', 'Voer Interest ID');
define('DEAL_NOT_FOUND', 'No Deal gevonden');

// claimedDeal API
define('CLAIMED_DEAL_ENTER_ID', 'Vul deal id');
define('CLAIMED_DEAL_ENTER_USERID', 'Vul Gebruiker Id');
define('CLAIMED_DEAL_SUCCESS', 'Deal beweerde succes');
define('CLAIMED_DEAL_FAILED', 'Deal niet geclaimd');
define('CLAIMED_DEAL_EXPIRED', 'deal vervalt');
define('CLAIMED_DEAL_NOT_VALID_LOGIN', 'Gelieve opnieuw in te loggen');
define('CLAIMED_DEAL_ENTER_PEOPLE', 'Vul het aantal personen');

// Email sent API
define('EMAIL_SENT_SUCCESS', 'E-mail verzonden');
define('EMAIL_SENT_FAILED', 'E-mail kan niet worden verzonden');
define('CLAIMED_DEAL_ENTER_ID', 'Vul deal id');


// forgotPassword API
define('FORGOT_ENTER_EMAIL', 'Vul e-mail');
define('FORGOT_EMAIL_SENT_SUCCESS', 'E-mail verzonden');
define('FORGOT_EMAIL_SENT_FAILED', 'E-mail kan niet worden verzonden');
define('FORGOT_EMAIL_NOT_EXIST', 'E-mail ID niet bestaat');
define('FORGOT_INVALID_EMAIL', 'Ongeldig email adres');


// setPassword
define('SET_PWD_ENTER_OTP', 'Vul OTP');
define('SET_PWD_ENTER_PWD', 'Voer wachtwoord in alstublieft');
define('SET_PWD_SUCCESS', 'Wachtwoord opnieuw succesvol');
define('SET_PWD_WRONG_OTP', 'U hebt ingevoerd verkeerde OTP');

// Choose lang
define('SET_LANG', 'Vul taal');
?>

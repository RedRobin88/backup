<?php
error_reporting(0);

	/* 
		This is an example class script proceeding secured API
		To use this class you should keep same as query string and function name
		Ex: If the query string value rquest=delete_user Access modifiers doesn't matter but function should be
		     function delete_user(){
				 You code goes here
			 }
		Class will execute the function dynamically;
		
		usage :
		
		    $object->response(output_data, status_code);
			$object->_request	- to get santinized input 	
			
			output_data : JSON (I am using)
			status_code : Send status message for headers
			
		Add This extension for localhost checking :
			Chrome Extension : Advanced REST client Application
			URL : https://chrome.google.com/webstore/detail/hgmloofddffdnphfgcellkdfbfbjeloo
		
		I used the below table for demo purpose.
		
		CREATE TABLE IF NOT EXISTS `users` (
		  `user_id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_fullname` varchar(25) NOT NULL,
		  `user_email` varchar(50) NOT NULL,
		  `user_password` varchar(50) NOT NULL,
		  `user_status` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`user_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 	*/
	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "etpl2012_resto";
		const DB_PASSWORD = "1n16L[y@,N@E";
		const DB = "etpl2012_restoranto";
		const apikey = 'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk';
		const ASSETS_URL = "http://exceptionaire.co/restoranto/";
		const GOOGLE_API_KEY = "AIzaSyDLfJwL3BTsHPl8l49rX4QaGoPF_L-PU9U";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			date_default_timezone_set('Europe/Amsterdam');
			$lang = $this->_request['lang'];
			if($lang == 'en') { 
				//$document_root = '/home/etpl2012/public_html/restoranto/api/'; 
				include('constants.php');
			} else if($lang == 'dt') {
				include($document_root.'constants_dt.php');
			} else if($lang == '') {
				include('constants.php');
			}	
				
			$this->checkApiKey();
			$this->dbConnect();					// Initiate Database connection
		}
		public function checkApiKey(){ 
				$headers = getallheaders();
				
				$token = (isset($headers['X-Api-Token']))?$headers['X-Api-Token']:'';
				if($token != self::apikey){
					$error['Status'] = 'Failed';
					$error['msg'] = 'Api key not matches';
					$this->response($this->json($error),404);
				}
		}
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *	Simple login API
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		
		private function login(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];		
			$password = $this->_request['password'];
			$device_token = (isset($this->_request['device_token']) && trim($this->_request['device_token']) !='')?$this->_request['device_token']:'';
			$device_type = (isset($this->_request['device_type']) && trim($this->_request['device_type']) !='')?$this->_request['device_type']:'';
			// Input validations
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email,phoneno FROM restoranto_users WHERE status = 1 and email = '$email' AND password = '".md5($password)."' LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
					
						$userid = $result['id'];
						$query_update 		= "UPDATE restoranto_users SET device_token = '".$device_token."',device_type = '".$device_type."' WHERE id = $userid";
						$sql_update 		= mysql_query($query_update) or die(mysql_error());
				
						$result['status'] = "Success";
						$result['msg'] = LOGIN_SUCCESS;
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result), 200);
					} else {
						
						$sql1 = mysql_query("SELECT id,name,email,phoneno FROM restoranto_users WHERE status = 1 and email = '$email' OR password = '".md5($password)."' LIMIT 1", $this->db);
						if(mysql_num_rows($sql1) > 0){
							$error['status'] = 'Failed';	
							$error['msg'] = LOGIN_ERROR_INVALID_INPUT;
							$this->response($this->json($error), 404);
						} else {
							$error['status'] = 'Failed';	
							$error['msg'] = LOGIN_ERROR_USER_NOT_EXIST;
							$this->response($this->json($error), 404);
						}	
						//$this->response('', 204);	// If no records "No Content" status
					}
				}
			}
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = LOGIN_ERROR_EMAIL;
				$this->response($this->json($error), 404);
			}	
			if(empty($password)) {
				$error['status'] = 'Failed';	
				$error['msg'] = LOGIN_ERROR_PASSWORD;
				$this->response($this->json($error), 404);
			}
			if(empty($device_token)) {
				$error['status'] = 'Failed';	
				$error['msg'] = USER_SELECT_DEVICE_TOKEN;
				$this->response($this->json($error), 404);
			}
			if(empty($device_type)) {
				$error['status'] = 'Failed';	
				$error['msg'] = USER_SELECT_DEVICE_TYPE;
				$this->response($this->json($error), 404);
			}
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "LOGIN_ERROR_BAD_REQUEST");
			$this->response($this->json($error), 400);
		}
		
		//function for echeck user email id
		private function check_mail(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = LOGIN_ERROR_EMAIL;
				$this->response($this->json($error), 404);
			}
			
			if(!empty($email) ){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email FROM restoranto_users WHERE email = '$email'  LIMIT 1", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$result['status'] = "Success";
						$result['msg'] = LOGIN_SUCCESS;
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result), 200);
					} else {
						$error['status'] = 'Failed';	
						$error['msg'] = LOGIN_ERROR_USER_NOT_EXIST;
						$this->response($this->json($error), 404);
						//$this->response('', 204);	// If no records "No Content" status
					}
				}else{
					$error['status'] = 'Failed';	
					$error['msg'] = EMAIL_NOT_VALID;
					$this->response($this->json($error), 404);
				}
			}else{
				$error['status'] = 'Failed';	
				$error['msg'] = LOGIN_ERROR_EMAIL;
				$this->response($this->json($error), 404);
			}
		} 
		/*
		private function users(){	
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$sql = mysql_query("SELECT user_id, user_fullname, user_email FROM users WHERE user_status = 1", $this->db);
			if(mysql_num_rows($sql) > 0){
				$result = array();
				while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
					$result[] = $rlt;
				}
				// If success everythig is good send header as "OK" and return list of users in JSON format
				$this->response($this->json($result), 200);
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
		private function deleteUser(){
			// Cross validation if the request method is DELETE else it will return "Not Acceptable" status
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){				
				mysql_query("DELETE FROM users WHERE user_id = $id");
				$success = array('status' => "Success", "msg" => "Successfully one record deleted.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}
		*/
		/*
		 *	Encode array into JSON
		*/
		
		//-------------- function for insert Records -----------------------------------------------//
		
		private function createUser(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			//print_r($this->_request);die;mysql_num_rows
			$mobileno = (isset($this->_request['phoneno']) && trim($this->_request['phoneno']) !='')?$this->_request['phoneno']:'';
			$email = (isset($this->_request['email']) && trim($this->_request['email']) !='')?$this->_request['email']:'';
			$name = (isset($this->_request['name']) && trim($this->_request['name']) !='')?$this->_request['name']:'';
			$password = (isset($this->_request['password']) && trim($this->_request['password']) !='')?$this->_request['password']:'';
			$device_token = (isset($this->_request['device_token']) && trim($this->_request['device_token']) !='')?$this->_request['device_token']:'';
			$device_type = (isset($this->_request['device_type']) && trim($this->_request['device_type']) !='')?$this->_request['device_type']:'';
			$lang = $this->_request['lang'];
			$password = md5($password);
			$status = 1;	
			$userType = 1;
			//$interest = $this->_request['interest'];
			$registerDate = date('Y-m-d H:i:s');
			$lastVisitDate  = date('Y-m-d H:i:s');
				
			//validation to check empty value
			if(empty($name)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_NAME;
				$this->response($this->json($error), 404);
			}else if(empty($mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_PHONE;
				$this->response($this->json($error), 404);
			}else if(strlen($mobileno) !=10){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_PHONE_VALID;
				$this->response($this->json($error), 404);
			}else if(!is_numeric($mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_PHONE_VALID_INT;
				$this->response($this->json($error), 404);
			}else if(!preg_match("/^[0-9]{10}$/i", $mobileno)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_PHONE_VALID_INT;
				$this->response($this->json($error), 404);
			} if(empty($email)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_EMAIL;
				$this->response($this->json($error), 404);
			} if(empty($password)){
				$error['status'] = 'Failed';	
				$error['msg'] = USER_ENTER_PASSWORD;
				$this->response($this->json($error), 404);
			}
			if(empty($device_token)) {
				$error['status'] = 'Failed';	
				$error['msg'] = USER_SELECT_DEVICE_TOKEN;
				$this->response($this->json($error), 404);
			}
			if(empty($device_type)) {
				$error['status'] = 'Failed';	
				$error['msg'] = USER_SELECT_DEVICE_TYPE;
				$this->response($this->json($error), 404);
			}
			if(empty($lang)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = SET_LANG;
				$this->response($this->json($error), 404);
			}	
			
			

			if(!empty($name) && !empty($mobileno) && !empty($email) && !empty($password)){ // check recorrd already exits in table
				
				//echo "SELECT id, username, username,dob,monumber FROM wad_users WHERE monumber = '".$mobileno."' AND dob='".$newDate."' LIMIT 1";die;
				$sql = mysql_query("SELECT * FROM restoranto_users WHERE email = '".$email."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$rowRes = mysql_fetch_assoc($sql);
						
						$success['status'] = 'Failed';	
						$success['msg'] = USER_EXISTS;
						
						$userdata['id'] = $rowRes['id'];
						$userdata['name'] = $rowRes['name'];
						$userdata['email'] = $rowRes['email'];
						$userdata['phoneno'] = $rowRes['phoneno'];
						
						
						$success['data'] = $userdata;
						$this->response($this->json($success), 200);
					}
					
				$sql = "INSERT INTO restoranto_users (name,email,phoneno,password,registerDate,lastVisitDate,userType,status,device_token,device_type) values('".$name."','".$email."','".$phoneno."','".$password."','".$registerDate."','".$lastVisitDate."','".$userType."','".$status."','".$device_token."','".$device_type."')";
				$userdata = array();
				if(mysql_query($sql)){
					$userid = mysql_insert_id(); 
					
					$userdata['userid'] = $userid;	
					self::sendNotificationEmail( $name, $email, $lang); 
					
					$success = array('status' => "Success", "msg" => USER_CREATE_SUCCESS,'data'=>$userdata);
					$this->response($this->json($success),200);
				}else{
					$error = array('status' => "Failed", "msg" => USER_NOT_CREATE);
					$this->response($this->json($error),404);
				}
				
			}	
		}
		
		//get Interest list 
		private function getInterest(){ 
			// Cross validation if the request method is GET else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$lang = $this->_request['lang'];	
				
			$sql_interest = mysql_query("SELECT * FROM restoranto_interest where status = 1");
			//$res_interest = mysql_num_rows($sql_interest);
			
			if(mysql_num_rows($sql_interest) > 0){ 
				$flag = 1;
				$k = 0;
				while($interestAll = mysql_fetch_array($sql_interest,MYSQL_ASSOC)){
					if($lang == 'dt') {
						$resultInterestAll[$k]['name'] = $interestAll['name_dt'];
					} else {
						$resultInterestAll[$k]['name'] = $interestAll['name'];	
					}	
					$resultInterestAll[$k]['status'] = $interestAll['status'];
					$resultInterestAll[$k]['id'] = $interestAll['id'];
					$k++;
					//$resultInterestAll[] = $interestAll;
				}
			} else {
				$flag = 0;
			}		
			if($flag){
					$result['status'] = 'Success';	
					$result['msg'] 	  = USER_INTEREST_LIST;
					$result['data'] = $resultInterestAll;
					// If success everythig is good send header as "OK" and return list of users in JSON format
					$this->response($this->json($result), 200);
			}else{
					$result['status'] = 'Failed';	
					$result['msg'] 	  = USER_NO_INTEREST;
					$this->response($this->json($result), 204);
			}
			// If no records "No Content" status
		}
		
		

        // Not in use				
		private function uploadDeal(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			//print_r($this->_request);die;mysql_num_rows
			$type_of_deal = (isset($this->_request['type_of_deal']) && trim($this->_request['type_of_deal']) !='')?$this->_request['type_of_deal']:'';
			$other_type_of_deal = (isset($this->_request['other_type_of_deal']) && trim($this->_request['other_type_of_deal']) !='')?$this->_request['other_type_of_deal']:'';
			$discount = (isset($this->_request['discount']) && trim($this->_request['discount']) !='')?$this->_request['discount']:'';
			$image = (isset($_FILES['image']['name']) && trim($_FILES['image']['name']) !='')?$_FILES['image']['name']:'';
			
			$n_of_d_claimed = (isset($this->_request['n_of_d_claimed']) && trim($this->_request['n_of_d_claimed']) !='')?$this->_request['n_of_d_claimed']:'';
			//$n_of_people_claimed = (isset($this->_request['n_of_people_claimed']) && trim($this->_request['n_of_people_claimed']) !='')?$this->_request['n_of_people_claimed']:'';
			$dealdate = (isset($this->_request['dealdate']) && trim($this->_request['dealdate']) !='')?$this->_request['dealdate']:'';
			$lat = (isset($this->_request['lat']) && trim($this->_request['lat']) !='')?$this->_request['lat']:'';
			$lng = (isset($this->_request['lng']) && trim($this->_request['lng']) !='')?$this->_request['lng']:'';
						
			$status = 1;	
			$added_date = date('Y-m-d H:i:s');
				
			//validation to check empty value
			if(empty($type_of_deal)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Select type of deal ';
				$this->response($this->json($error), 404);
			}else if(empty($discount)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter discount ';
				$this->response($this->json($error), 404);
			}else if(empty($image)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please upload deal image ';
				$this->response($this->json($error), 404);
			} 
			if(empty($n_of_d_claimed)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Number of deals which can be claimed ';
				$this->response($this->json($error), 404);
			} 
			/*if(empty($n_of_people_claimed)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please enter Number of people for a deal which can be claimed ';
				$this->response($this->json($error), 404);
			}*/
			if(empty($dealdate)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please select deal date ';
				$this->response($this->json($error), 404);
			}
			if(empty($lat)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please select deal location properly ';
				$this->response($this->json($error), 404);
			}
			if(empty($lng)){
				$error['status'] = 'Failed';	
				$error['msg'] = 'Please Please select deal location properly ';
				$this->response($this->json($error), 404);
			}
			
				$document_root = '/home/etpl2012/public_html/restoranto/assets/images';
					$target_dir = $document_root.'/deals/';
					$path_parts 	= pathinfo($_FILES["postimage"]["name"]);
					//$this->response($this->json(array("file name"=>$_FILES)), 404);die;
					$imageFileType 	= $path_parts['extension'];
					$create_new_name = basename($path_parts['filename']).'_'.time().'.'.$imageFileType;
					$target_file 	= $target_dir.$create_new_name ;
					if(is_dir($target_dir) == false){
						chmod($document_root,0777);
						mkdir($target_dir,0777);
					}
					$target_thumbs_http_file ='';
					if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
						$target_http_file = self::ASSETS_URL.'assets/images/deals/'.$create_new_name;
						include_once("function.php");
						$thum_path =  $target_dir.'thumbs/';
						$target_file_thumb 	= $thum_path.$create_new_name;
						if(is_dir($thum_path) == false){
								mkdir($thum_path,0777);
						}
						$wmax = 100;
						$hmax = 100;
						ak_img_resize($target_file, $target_file_thumb, $wmax, $hmax, $imageFileType);
						$target_thumbs_http_file = self::ASSETS_URL.'assets/images/deals/thumbs/'.$create_new_name;
					}else{
						$error['status'] = 'Failed';	
						$error['msg'] = 'Image is not uploded';
						$this->response($this->json($error), 404);
					}	
					
				$sql = "INSERT INTO restoranto_upload_deals (type_of_deal,other_type_of_deal,discount,image,n_of_d_claimed,n_of_people_claimed,status,dealdate,added_date,lat,lng) values('".$type_of_deal."','".$other_type_of_deal."','".$discount."','".$target_http_file."','".$n_of_d_claimed."','".$n_of_people_claimed."','".$status."','".$dealdate."','".$added_date."','".$lat."','".$lng."')";
				$dealdata = array();
				if(mysql_query($sql)){
					$dealid = mysql_insert_id(); 
					
					$dealdata['dealid'] = $dealid;
					
					//self::sendNotificationEmail( $name, $email ); 
					
					$success = array('status' => "Success", "msg" => "Deal uploaded successfully",'data'=>$dealdata);
					$this->response($this->json($success),200);
				}else{
					$error = array('status' => "Failed", "msg" => "Deal not uploaded ");
					$this->response($this->json($error),404);
				}				
		}
		
		
		private function showDeals(){ 
			
			//echo date('Y-m-d H:i:s');
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			//$userid = $this->_request['userid'];	
			//$interestid = $this->_request['interestid'];	
			$lat = $this->_request['lat'];
			$lng = $this->_request['lng'];
			$lang = $this->_request['lang'];
			
			if(empty($lat)){
				$error['status'] = 'Failed';	
				$error['msg'] = DEAL_ENTER_LATITUDE;
				$this->response($this->json($error), 404);
			}else if(empty($lng)){
				$error['status'] = 'Failed';	
				$error['msg'] = DEAL_ENTER_LONGITUTE;
				$this->response($this->json($error), 404);
			}else if(empty($lang)){
				$error['status'] = 'Failed';	
				$error['msg'] = SET_LANG;
				$this->response($this->json($error), 404);
			}
			$current_time = date('Y/m/d H:i:s');
			$time = date('Y/m/d H:i:s');
			//$time = date('Y/m/d H:i:s', strtotime('+4 hour'));
			//echo "SELECT *, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals HAVING distance < 60 AND TIMEDIFF('$time',dealdate) >= 0 ORDER BY distance";
			//$sql = mysql_query("SELECT *, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals WHERE type_of_meal = ".$interestid." HAVING distance < 60 AND TIMEDIFF('$time',dealdate) >= 0 ORDER BY distance", $this->db);
			$sql = mysql_query("SELECT *, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals HAVING distance < 60 AND TIMEDIFF('$time',dealdate) >= 0 ORDER BY distance", $this->db);
			
			
			if(mysql_num_rows($sql) > 0){
				$result = array();
				$result1 = array();
				$result_deal = array();
				$i = 0;
				
				$resultlatlng = array();
				$j=0;
				while($result_latlng = mysql_fetch_array($sql,MYSQL_ASSOC)){	
					
					$timedif = explode(":",$result_latlng['timedif']);
					//print_r($timedif);
					if($timedif[0] <= 4) {  
						if($timedif[0] == 4 && $timedif[1] > 0) {
					    } else {	
							$resultlatlng[$j]['lat'] = $result_latlng['lat'];
							$resultlatlng[$j]['lng'] = $result_latlng['lng'];
							$resultlatlng[$j]['distance'] = $result_latlng['distance'];
							$j++;
						}	
					}
					
				}	
				
				if($j == 0) {
					// If no records "No Content" status
					$error = array('status' => "Failed", "msg" => DEAL_NOT_FOUND);
					$this->response($this->json($error), 400);
				}		
				
				$output = array_map("unserialize",array_unique(array_map("serialize", $resultlatlng)));
				//echo "<pre>"; print_r(array_values($output)); 
				$latlang_unq_array = array_values($output);
				$k=0;
				
				$deal_available_flag = 0;
				foreach($latlang_unq_array as $latlng) {
					$lat1 = $latlng['lat'];
					$lng1 = $latlng['lng'];
					$distance = $latlng['distance'];
					//echo "SELECT *,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals where lat = $lat1 and lng = $lng1 AND TIMEDIFF('$time',dealdate) >= 0";
					$sql1 = mysql_query("SELECT *,TIMEDIFF('$time',dealdate) as timedif FROM restoranto_upload_deals where lat = $lat1 and lng = $lng1 AND TIMEDIFF('$time',dealdate) >= 0", $this->db);
					$result1[$k]['lat'] = $latlng['lat'];
					$result1[$k]['lng'] = $latlng['lng'];
					$result1[$k]['distance'] = $latlng['distance'];
					
					
					if(mysql_num_rows($sql1) > 0){				
						$result_deal1 = array(); 
						$l =0; 

						while($result_qry1 = mysql_fetch_array($sql1,MYSQL_ASSOC)){ 
							$timedealdif = explode(":",$result_qry1['timedif']);
							
							// If deals 4 hrs is in current time 
							if($timedealdif[0] <= 4 && $timedealdif[0] >= 0) {  
								
								// Check whether user can able to claimed the deal or not
								$sql_claimed_cnt = mysql_query("SELECT sum(no_of_people) AS total_claimed_deals FROM restoranto_claimed_deals WHERE deal_id = '".$result_qry1['id']."'", $this->db);
								if(mysql_num_rows($sql_claimed_cnt) > 0){
										$result_claimed_cnt = mysql_fetch_array($sql_claimed_cnt,MYSQL_ASSOC);
										$total_claimed_deals = $result_claimed_cnt['total_claimed_deals'];
								}		
								$total_available_deals = $result_qry1['n_of_d_claimed']-$total_claimed_deals;
								if($total_available_deals > 0) {	
									//echo $result_qry1['id']."@";
									//$deal_available_cnt++;						
									// Total remaining deal to be claimed
									$result_deal1['deal'][$l]['remaining_deal'] = $total_available_deals;
											
									$result_deal1['deal'][$l]['id'] = $result_qry1['id'];
									if($lang == 'dt') { 
										$result_deal1['deal'][$l]['restaurant'] = $result_qry1['restaurant_dt'];
										$result_deal1['deal'][$l]['discount'] = $result_qry1['discount_dt'];
										$result_deal1['deal'][$l]['deal_title'] = $result_qry1['deal_title_dt'];
									} else {
										$result_deal1['deal'][$l]['restaurant'] = $result_qry1['restaurant'];
										$result_deal1['deal'][$l]['discount'] = $result_qry1['discount'];
										$result_deal1['deal'][$l]['deal_title'] = $result_qry1['deal_title'];
									}		
									 	
                                    $result_deal1['deal'][$l]['price_show_option'] = $result_qry1['price_show_option'];
                                    $result_deal1['deal'][$l]['old_price'] = $result_qry1['old_price'];
                                    $result_deal1['deal'][$l]['new_price'] = $result_qry1['new_price'];
                                    								
									$result_deal1['deal'][$l]['no_of_deals_can_be_claimed'] = $result_qry1['n_of_d_claimed'];
									//$result_deal1['deal'][$l]['no_of_people_for_deal'] = $result_qry1['n_of_people_claimed'];
									if($result_qry1['image']) {
										$result_deal1['deal'][$l]['image'] = $result_qry1['image'];
									} else {
										$result_deal1['deal'][$l]['image'] = self::ASSETS_URL."assets/images/deals/no-deal-image.jpg";
									}	
									$result_deal1['deal'][$l]['dealdate'] = $result_qry1['dealdate'];
									$result_deal1['deal'][$l]['currenttime'] = $current_time;
									$perdealdate = $result_qry1['dealdate'];
									$result_deal1['deal'][$l]['dealendtime'] = date('Y/m/d H:i:s', strtotime("$perdealdate+4 hour"));
									$dealenddatetime = date('Y/m/d H:i:s', strtotime("$perdealdate+4 hour"));
									$to_time = strtotime("$dealenddatetime");
									$from_time = strtotime("$current_time");
									$minutes_left = round(($to_time - $from_time) / 60,2);
									$result_deal1['deal'][$l]['deal_left_minutes'] = $minutes_left;
									
									// If deal left time is greater than 0 then only show deal
									if($minutes_left > 0) { 				
										$result1[$k]['offer'] = $result_deal1;
										$deal_available_flag = $deal_available_flag + 1;
									}
									$l++;
								}
								
							}// End of if deals 4 hrs is in current time 
						}// End of while loop
						
					}	
					$k++;	
				}		
				if($deal_available_flag > 0) {  
					$this->response($this->json($result1), 200);
				}else { 
					// If no records "No Content" status
					$error = array('status' => "Failed", "msg" => DEAL_NOT_FOUND);
					$this->response($this->json($error), 400);
				}	
				/*while($result_qry = mysql_fetch_array($sql,MYSQL_ASSOC)){	
					
					//$result_qry = mysql_fetch_array($sql,MYSQL_ASSOC);
					// If success everythig is good send header as "OK" and user details
					$result[$i]['id'] = $result_qry['id'];
					$result[$i]['lat'] = $result_qry['lat'];
					$result[$i]['lng'] = $result_qry['lng'];
					$result_deal['deal']['restaurant'] = $result_qry['restaurant'];
					$result_deal['deal']['discount'] = $result_qry['discount'];
					$result_deal['deal']['image'] = $result_qry['image'];
					$result_deal['deal']['dealdate'] = $result_qry['dealdate'];
					$result[$i]['offer'] = $result_deal;
					$i++;
				}	
					$this->response($this->json($result), 200);
				*/
			} else {
				// If no records "No Content" status
				$error = array('status' => "Failed", "msg" => DEAL_NOT_FOUND);
				$this->response($this->json($error), 400);
			}	
			
			
			
		}
		
		private function forgotPassword(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];		
			//$name = $this->_request['name'];	
			//$userid = $this->_request['userid'];		
					
			if(empty($email)) {
				$error['status'] = 'Failed';	
				$error['msg'] = FORGOT_ENTER_EMAIL;
				$this->response($this->json($error), 404);
			}				
			// Input validations
			if(!empty($email)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$sql = mysql_query("SELECT id,name,email,phoneno FROM restoranto_users WHERE status = 1 and email = '$email'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						
						$name = $result['name'];
						$userid = $result['id'];
						$otp = mt_rand();
						$to 	     = $email;
					    $subject  = "Forgot Password";
					    $message  = "Hello ".$name.", <br /><br />\r\n";
					    //$message .= "Thank you for registering at Restoranto.<br />\r\n";
					    $message .= "Your OTP is ".$otp."<br />\r\n";
					    $message .= "You can use this OTP to set new password in Restoranto APP<br /><br /><br />\r\n";
					    $message .= "Thanks and Regards,<br />Restoranto<br /><br /><br />\r\n";
					    $headers  = 'MIME-Version: 1.0' . PHP_EOL;
					    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					  // More headers
					   $headers .= "From:noreply@restoranto.com";
					   $retval 	 = mail($to,$subject,$message,$headers);
							   
					   if( $retval == true )  
					   {
						    $query_update 		= "UPDATE restoranto_users SET otp = '".$otp."' WHERE id = $userid";
							$sql_update 		= mysql_query($query_update) or die(mysql_error());
							
							$result['status'] = 'Success';	
							$result['msg'] 	  = FORGOT_EMAIL_SENT_SUCCESS;			
							$this->response($this->json($result), 200);			
					   }
					   else
					   {
							$error['status'] = 'Failed';	
							$error['msg'] 	 = FORGOT_EMAIL_SENT_FAILED;
							$this->response($this->json($error), 509);
					   }
					} else {
						$error['status'] = 'Failed';	
						$error['msg'] = FORGOT_EMAIL_NOT_EXIST;
						$this->response($this->json($error), 404);
						//$this->response('', 204);	// If no records "No Content" status
					}
				} else {
					// If invalid inputs "Bad Request" status message and reason
					$error = array('status' => "Failed", "msg" => FORGOT_INVALID_EMAIL);
					$this->response($this->json($error), 400);
				}	
			}
			
			
		}
		private function setPassword(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$otp = $this->_request['otp'];		
			$password = $this->_request['password'];	
					
			if(empty($otp)) {
				$error['status'] = 'Failed';	
				$error['msg'] = SET_PWD_ENTER_OTP;
				$this->response($this->json($error), 404);
			} else if(empty($password)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = SET_PWD_ENTER_PWD;
				$this->response($this->json($error), 404);
			}	
			
			if(!empty($otp) && !empty($password)){
				$sql = mysql_query("SELECT id,name,email,phoneno FROM restoranto_users WHERE status = 1 and otp = '$otp'", $this->db);
				if(mysql_num_rows($sql) > 0){
					$result = mysql_fetch_array($sql,MYSQL_ASSOC);
					
					$userid = $result['id'];
					$query_update 		= "UPDATE restoranto_users SET password = '".md5($password)."',otp='' WHERE id = $userid";
					$sql_update 		= mysql_query($query_update) or die(mysql_error());
							
					$result['status'] = 'Success';	
					$result['msg'] 	  = SET_PWD_SUCCESS;			
					$this->response($this->json($result), 200);	
				} else {
					$error['status'] = 'Failed';	
					$error['msg'] = SET_PWD_WRONG_OTP;
					$this->response($this->json($error), 404);
				}		
			}					
		}	
		
		private function claimedDeal(){ 
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() !="POST"){
					$this->response('',406);
			}
			
			$deal_id = $this->_request['deal_id'];		
			$userid = $this->_request['userid'];
			$no_of_people = $this->_request['no_of_people'];
			$lang = $this->_request['lang'];
			$claimed_datetime = date('Y-m-d H:i:s');
								
			if(empty($deal_id)) {
				$error['status'] = 'Failed';	
				$error['msg'] = CLAIMED_DEAL_ENTER_ID;
				$this->response($this->json($error), 404);
			} else if(empty($userid)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = CLAIMED_DEAL_ENTER_USERID;
				$this->response($this->json($error), 404);
			} else if(empty($no_of_people)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = CLAIMED_DEAL_ENTER_PEOPLE;
				$this->response($this->json($error), 404);
			} else if(empty($lang)) {	
				$error['status'] = 'Failed';	
				$error['msg'] = SET_LANG;
				$this->response($this->json($error), 404);
			}	
			
			// Fetch merchant and user email id to send email for verification
			$sql = mysql_query("SELECT id,name,email,phoneno,device_token,device_type FROM restoranto_users WHERE status = 1 and id = '$userid'", $this->db);
			if(mysql_num_rows($sql) > 0)
			{
					$result = mysql_fetch_array($sql,MYSQL_ASSOC);
					$username = $result['name'];
					$useremail = $result['email'];
					$userphoneno = $result['phoneno'];
					$userdevice_token = $result['device_token'];
					$userdevice_type = $result['device_type'];
						
					
					$sql1 = mysql_query("SELECT d.deal_title,d.deal_title_dt,d.n_of_d_claimed as n_of_d_claimed,u.email as email,u.company_name as company_name,u.company_phoneno as company_phoneno,u.company_loc as company_loc,d.restaurant as restaurant,d.discount as discount,d.dealdate as dealdate 
										FROM restoranto_upload_deals as d,restoranto_admin_merchant_users as u
										WHERE d.merchant_id = u.id and d.id = '$deal_id'", $this->db);
					if(mysql_num_rows($sql1) > 0){
							$result1 = mysql_fetch_array($sql1,MYSQL_ASSOC);
							$merchant_email = $result1['email'];
							$merchant_company = $result1['company_name'];
							$merchant_company_phoneno = $result1['company_phoneno'];
							$merchant_company_loc = $result1['company_loc'];
							$deal_restaurant = $result1['restaurant'];
							$deal_discount = $result1['discount'];
							$deal_dealdate = $result1['dealdate'];
							$deal_n_of_d_claimed = $result1['n_of_d_claimed'];
							$deal_title_en = $result1['deal_title']; 
							$deal_title_dt = $result1['deal_title_dt']; 
					}
						
					// Check whether user can able to claimed the deal or not
					$sql_claimed_cnt = mysql_query("SELECT sum(no_of_people) AS total_claimed_deals FROM restoranto_claimed_deals WHERE deal_id = '$deal_id'", $this->db);
					if(mysql_num_rows($sql_claimed_cnt) > 0){
							$result_claimed_cnt = mysql_fetch_array($sql_claimed_cnt,MYSQL_ASSOC);
							$total_claimed_deals = $result_claimed_cnt['total_claimed_deals'];
					}		
					
					$total_available_deals = $deal_n_of_d_claimed-$total_claimed_deals;
						
					if($total_available_deals >0) {	 
							$sql = "INSERT INTO restoranto_claimed_deals (id,deal_id,userid,claimed_datetime,no_of_people) values('','".$deal_id."','".$userid."','".$claimed_datetime."','".$no_of_people."')";
							$userdata = array();
							if(mysql_query($sql)){
								$userid = mysql_insert_id(); 
								
								// To send push notification
								$userarray['userid'] = $userid;
								$userarray['device_token'] = $userdevice_token;
								$push_notification_message = "$deal_restaurant deal confirmed by you at $claimed_datetime";
								//$this->pushNotification($userarray,$message);
								
								$userdata['deal_id'] = $deal_id;
								
							   $deal_sep = explode(" ",$deal_dealdate);
							   $deal_sep_date = $deal_sep[0];
							   $deal_sep_hrs = $deal_sep[1];
							  /* $useremail = "dattu@exceptionaire.co";
							   $merchant_email = "dattulondhe@gmail.com";*/
							   //$useremail = "shobhakorade@gmail.com";
							   
							   
							   if($useremail) { 	
									// For User email content
								   /*$subject_user  = "$merchant_company deal confirmed!";
								   $message_user  = "Hello ".$merchant_company.", <br /><br />\r\n";
								   $message_user .= "$deal_restaurant deal confirmed by you at $claimed_datetime.<br />\r\n<br />";
								   $message_user .= "Deal Details  <br />Discount : $deal_discount<br /> Date : $deal_dealdate<br />\r\n<br />";
								   $message_user .= "$deal_restaurant Phone No. $merchant_company_phoneno .<br />\r\n<br />";
								   $message_user .= "Regards,<br />Restoranto Team.";*/
								   $subject_user_en  = "Your Restoranto booking confirmation";
								   $message_user_en  = "Hello ".$username.", <br /><br />\r\n";
								   $message_user_en .= "Restoranto is happy to confirm that you have successfully claimed the deal ​$deal_title_en for $no_of_people people.​ As your booking is time sensitive, we want to remind you that you are expected to be at the restaurant within one hour. <br /><br />";
								   $message_user_en .= "Your booking details are:  <br />​$deal_title_en at $merchant_company<br />$merchant_company_loc<br />$merchant_company_phoneno<br />$deal_sep_date ​at $deal_sep_hrs <br /><br />";
								   $message_user_en .= "We hope you have a great experience and bon apetit! If you enjoyed your meal, we would love if you would share it with your friends!<br /><br />";
								   $message_user_en .= "Team Restoranto<br /><br />";
								   $message_user_en .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Share on Facebook</a>";
								   
								   
								   $subject_user_dt  = "Uw Restaurant boekingsbevestiging";
								   $message_user_dt  = "Beste ".$username.", <br /><br />\r\n";
								   $message_user_dt .= "Hierbij de bevestiging van de deal ​$deal_title_dt gereserveerd voor $no_of_people personen​. Aangezien Restoranto deals tijdelijk geldig zijn willen we je er aan herinneren dat je binnen één uur bij het restaurant moet zijn.<br /><br />";
								   $message_user_dt .= "De details van je boeking zijn:  <br />​$deal_title_dt ​​bij $merchant_company<br />$merchant_company_loc<br />$merchant_company_phoneno<br />$deal_sep_date ​at $deal_sep_hrs<br /><br />";
								   $message_user_dt .= "We wensen je een fijne dag en vooral eet smakelijk! Als je van je maaltijd genoten hebt voel je vrij om dit aan je vrienden te vertellen en deel ons op social media.<br /><br />";
								   $message_user_dt .= "Team Restoranto<br /><br />";
								   $message_user_dt .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Klik hier om te delen op Facebook</a>";
								   
								   if($lang == 'en') { 
										self::sendVerificationEmail( $useremail, $subject_user_en, $message_user_en );
								   } else if($lang == 'dt') { 
									    self::sendVerificationEmail( $useremail, $subject_user_dt, $message_user_dt );
								   }   	
								}
							
							   if($merchant_email) {
								   if(!$userphoneno) $userphoneno_en = "Not Specified"; else $userphoneno_en = $userphoneno;
								   if(!$useremail) $useremail_en = "Not Specified"; else $useremail_en = $useremail;
								   
								   if(!$userphoneno) $userphoneno_dt = "Niet gespecificeerd"; else $userphoneno_dt = $userphoneno;
								   if(!$useremail) $useremail_dt = "Niet gespecificeerd"; else $useremail_dt = $useremail;
								   
								   $userphoneno_display_en = "Phone No. : ".$userphoneno_en;
								   $useremail_display_en = "Email : ".$useremail_en;
								   
								   $userphoneno_display_dt = "Telefoon nr. : ".$userphoneno_dt;
								   $useremail_display_dt = "E-mail : ".$useremail_dt;
								   
								   // For Merchant email content	
								   $subject_merchant_en  = "Confirmation of a booking by Restoranto";
								   $subject_merchant_dt  = "Bevestiging van een boeking via Restoranto";
								   //$subject_merchant_dt  = "Deal claimed by $username - $userphoneno !";
								   $message_merchant_en   = "Hello ".$deal_restaurant.", <br /><br />";
								   $message_merchant_en  .= "Restoranto is happy to inform you that your ​$deal_title_en has been successfully claimed by $no_of_people people.<br /><br />";
								   $message_merchant_en  .= "The booking details are:<br />$username<br />$userphoneno_display_en<br />$useremail_display_en<br />$deal_sep_date ​at $deal_sep_hrs<br /><br />";
								   $message_merchant_en  .= "We hope you have had a great experience and we like to thank you for choosing Restoranto. We look forward working together again, connecting people together through food.<br /><br />";
								   $message_merchant_en  .= "Do you have any questions or want to share with us your experience? Then just send us an email info@restoranto.com to give us feedback, share your experience or just to say hello.<br /><br />";
								   $message_merchant_en  .= "Team Restoranto<br /><br />";
								   $message_merchant_en .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Klik hier om te delen op Facebook</a>";
								   
								   $message_merchant_dt   = "Beste ".$deal_restaurant.", <br /><br />";
								   $message_merchant_dt  .= "Hierbij de bevestiging dat de deal ​$deal_title_dt is geclaimd door $no_of_people personen.<br /><br />";
								   $message_merchant_dt  .= "De details van de boeking zijn als volgend:<br />$username<br />$userphoneno_display_dt<br />$useremail_display_dt<br />$deal_sep_date om $deal_sep_hrs<br /><br />";
								   $message_merchant_dt  .= "We hopen dat het alles soepel verlopen is en we willen je bedanken voor het kiezen van Restoranto. We kijken er naar uit om meer samen te werken en mensen te verbinden door eten.<br /><br />";
								   $message_merchant_dt  .= "Heb je vragen of wil je een ervaring delen? Stuur ons een mail naar info@restoranto.com om ons feedback te geven of gewoon even hoi te zeggen.<br /><br />";
								   $message_merchant_dt  .= "Team Restoranto<br /><br />";
								   $message_merchant_dt .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Klik hier om te delen op Facebook</a>";
								  
								   if($lang == 'en') { 
									   self::sendVerificationEmail( $merchant_email, $subject_merchant_en, $message_merchant_en); 
								   } else if($lang == 'dt') { 
									   self::sendVerificationEmail( $merchant_email, $subject_merchant_dt, $message_merchant_dt); 
								   }   	
								}
								/* Push notification code **/
								if($userdevice_type == 'ios') { 
									$passphrase = "123456";
									$message = $push_notification_message;
									//$tokenArr[]["device_token"]	= '5bd677e8faec62d3afe2726ce9ad78e415dc26ef1985d0849b6f501ee72721f9';		
									$tokenArr[]["device_token"]	= $userdevice_token;
									if(count($tokenArr)>0) {
										foreach($tokenArr AS $token){
											$deviceToken = $token["device_token"];
											$ctx = stream_context_create();
											stream_context_set_option($ctx, "ssl", "local_cert", "pushcert.pem");
											stream_context_set_option($ctx, "ssl", "passphrase", $passphrase);

											$fp = stream_socket_client("ssl://gateway.sandbox.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
											//$fp = stream_socket_client("ssl://gateway.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
											if (!$fp)
												exit("Failed to connect: $err $errstr" . PHP_EOL);
											
											$body["aps"] = array("alert" => $message,"sound" => "default");
											$payload = json_encode($body);
											$msg = chr(0).pack("n",32).pack("H*",$deviceToken).pack("n",strlen($payload)).$payload;
											$result = fwrite($fp, $msg, strlen($msg));
											//print_r($result);
											if(!$result){   
												$ip++;
											}else{   
												$jp++;
											}
											fclose($fp);
										}
									}	
								}
								else if($userdevice_type == 'android'){
									$deviceidArr = array($userdevice_token);
									//$push_notification_message = "test message";
									$notificationArr = array("notification" => $push_notification_message);
									self::android_push_notification($deviceidArr,$push_notification_message);
								}	
								/* Push notification code End here**/
								
								$success = array('status' => "Success", "msg" => CLAIMED_DEAL_SUCCESS,'data'=>$userdata);
								$this->response($this->json($success),200);
							}else{
								$error = array('status' => "Failed", "msg" => CLAIMED_DEAL_FAILED);
								$this->response($this->json($error),404);
							}
					} else {
						$error = array('status' => "Failed", "msg" => CLAIMED_DEAL_EXPIRED);
						$this->response($this->json($error),404);
					}	
			} else { 
				$error = array('status' => "Failed", "msg" => CLAIMED_DEAL_NOT_VALID_LOGIN);
				$this->response($this->json($error),404);
			}	
		}	
		
		private function android_push_notification($deviceidArr,$notificationArr){
			
			$url = 'https://android.googleapis.com/gcm/send';
			$message = $notificationArr;
				$fields = array(
					'registration_ids' => $deviceidArr,
					'data' => array( "message" => $message ),
				);
			$headers = array(
					'Authorization: key=' . self::GOOGLE_API_KEY,
					'Content-Type: application/json'
				);
				// Open connection
				$ch = curl_init();
		 
				// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, $url);
		 
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		 
				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}
				
				// Close connection
				curl_close($ch);	
				//print_r($result); die;			
		}


		private function sendNotificationEmail( $name, $email, $lang )
		{
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$error['status'] 	= 'Failed';	
				$error['msg'] 		= 'Not Acceptable';
				$this->response($this->json($error), 400);
			}
		   $to 	     = $email;
		   $subject_en  = "Welcome on board";
		   $subject_dt  = "Welkom aan boord";
		   
		   $message_en  = "Hello ".$name.", <br /><br />\r\n";
		   $message_en .= "Thanks for joining Restoranto.<br /><br />\r\n";
		   $message_en .= "With Restoranto, you pay less, feed more​. Our application provides you with last-­minute discounts from 
			restaurants in your neighborhood and for every meal you book, you also donate a meal to Africa!<br /><br />";
		   $message_en .= "So here's how it works:<br /><br />";  
		   $message_en .= "&nbsp;&nbsp;&nbsp;&nbsp;1) Explore local food and drink deals from great places nearby<br />&nbsp;&nbsp;&nbsp;&nbsp;2) Pick a deal you like and make it yours by claiming it<br />&nbsp;&nbsp;&nbsp;&nbsp;3) You buy 1, you give 1. While you enjoy your meal, we buy and donate a meal in Uganda <br /><br />";
		   $message_en .= "Please note, as we provide last­minute offers, you are expected to be at the restaurant within an hour of the 
			booking.<br /><br />";
		   $message_en .= "Do you have any questions? Then just send us an email ​info@restoranto.com​ to give us feedback, share your stories or just to say hello!<br /><br />";
		   $message_en .= "Team Restoranto<br /><br />";
		   $message_en .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Share on Facebook</a>";
		   
		   $message_dt  = "Beste ".$name.", <br /><br />\r\n";
		   $message_dt .= "Bedankt voor het downloaden van Restoranto.<br /><br />\r\n";
		   $message_dt .= "Met Restoranto betaal je minder en geef je meer, Pay Less, Feed More ​is ons motto​. Met de Restoranto app krijg je last-­minute aanbiedingen van restaurants in de buurt en voor elke maaltijd die je boekt doneer je 
een maaltijd in Afrika!<br /><br />";
		   $message_dt .= "Dit is hoe het werkt:<br /><br />";  
		   $message_dt .= "&nbsp;&nbsp;&nbsp;&nbsp;1) Ontdek aanbiedingen van leuke lokale bars & restaurants<br />&nbsp;&nbsp;&nbsp;&nbsp;2) Vind een leuke aanbieding en claim deze voordat iemand anders dit doet<br />&nbsp;&nbsp;&nbsp;&nbsp;3) Jij koopt, jij geeft. Terwijl jij van je maaltijd geniet, kopen en doneren wij een maaltijd in Oeganda.<br /><br />";
		   $message_dt .= "Onthoud goed, aangezien het hier om last­minute kortingen gaat wordt er van je verwacht dat je binnen een 
uur na het boeken bij het restaurant verschijnt.<br /><br />";
		   $message_dt .= "Heb je vragen? Stuur een mail naar info@restoranto.com​. Als je feedback hebt, een ervaring wil delen of 
gewoon als je hoi wilt zeggen.<br /><br />";
		   $message_dt .= "Team Restoranto<br /><br />";
		   $message_dt .= "<a href='https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com' target='_blank'>Klik hier om te delen op Facebook</a>";
			
		   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
		   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		  // More headers
		  $headers .= "From:noreply@restoranto.com\r\n";
			if($lang == 'en') { 
				$retval 	 = mail($to,$subject_en,$message_en,$headers);
			} else if($lang == 'dt') { 
				$retval 	 = mail($to,$subject_dt,$message_dt,$headers);
			}
				   
		   if( $retval == true )
		   {
				$result['status'] = 'Success';	
				$result['msg'] 	  = EMAIL_SENT_SUCCESS;						
		   }
		   else
		   {
				$error['status'] = 'Failed';	
				$error['msg'] 	 = EMAIL_SENT_FAILED;
				$this->response($this->json($error), 509);
		   }
			
		}
		
		private function sendVerificationEmail( $to, $subject, $content )
		{	
		   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
		   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		  // More headers
		   $headers .= "From:booking@restoranto.com\r\n";
		   $retval 	 = mail($to,$subject,$content,$headers);
				   
		   if( $retval == true )  
		   {
				$result['status'] = 'Success';	
				$result['msg'] 	  = EMAIL_SENT_SUCCESS;						
		   }
		   else
		   {
				$error['status'] = 'Failed';	
				$error['msg'] 	 = EMAIL_SENT_FAILED;
				$this->response($this->json($error), 509);
		   }
			
		}
		
		private function pushNotification($userarray,$message){ 
			$passphrase = "123456";
			$tokenArr[]["device_token"]	= '3cd75301f4f5f1b8b5f3bc83f535d043a6fce732ec613f3c4a520fa3efbeb577';		
			foreach($tokenArr AS $token){
				$deviceToken = $token["device_token"];
				$ctx = stream_context_create();
				stream_context_set_option($ctx, "ssl", "local_cert", "pushcert.pem");
				stream_context_set_option($ctx, "ssl", "passphrase", $passphrase);

				$fp = stream_socket_client("ssl://gateway.sandbox.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				//$fp = stream_socket_client("ssl://gateway.push.apple.com:2195", $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				if (!$fp)
					exit("Failed to connect: $err $errstr" . PHP_EOL);
				
				$body["aps"] = array("alert" => $message,"sound" => "default");
				$payload = json_encode($body);
				$msg = chr(0) . pack("n", 32) . pack("H*", $deviceToken) . pack("n", strlen($payload)) . $payload;
				$result = fwrite($fp, $msg, strlen($msg));
				if(!$result){   
					$ip++;
				}else{   
					$jp++;
				}
				fclose($fp);
			}
			exit;
		}	
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>

function setMap($lat,$lng) {
	
	/*if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
       // x.innerHTML = "Geolocation is not supported by this browser.";
    }*/
    
	
	var myLatLng = {lat: $lat, lng: $lng};

		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 4,
		  center: myLatLng
		});

		var marker = new google.maps.Marker({
		  position: myLatLng,
		  map: map,
		  title: 'Drag Me',
		  draggable: true
		});
		
		 marker.addListener('drag',function(event) {
			document.getElementById('lat').value = event.latLng.lat();
			document.getElementById('lng').value = event.latLng.lng();
		});
}


//google.maps.event.addDomListener(window, 'load', initialize);

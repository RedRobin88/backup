//click event on delete user
function delete_user(userid){
	var result = confirm("Want to delete?");
	if (result) {
		//Logic to delete the item
		$.ajax({
			url:site_url+'user-management/delete',
			data:{id:userid},
			type:'post',
			success:function(data){
				window.location.reload();
				return false;
				
			}
		});
	}
	
}

function change_user_status(userid,status){
	var status_s = status.value;
	//Logic to delete the item
	$.ajax({
		url:site_url+'user-management/change_user_status',
		data:{id:userid,status:status_s},
		type:'post',
		success:function(data){
			//if(data.trim() == 1){  			
				window.location.reload(); 
				return false;
			//}
		}
	});
	
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		
		if($this->uri->segment(1) == 'logout') {
			$this->logout();
		}	
				
		if($this->session->userdata('userType') == 2) { 
			redirect("merchant-upload-deal");
		} else if($this->session->userdata('userType') == '1') { 
			redirect("admin-dashboard");
		}	
			
	}	  
	public function index(){  
		$data = array();
		$data['title'] = 'Restoranto Login';
        
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'emailid', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		if ($this->form_validation->run() == FALSE){//check validation
			
		}else{
			$result = $this->login_model->checkLogin();
			
			if(count($result)>0) {
				$this->session->set_userdata('userId',$result[0]->id);
				$this->session->set_userdata('company_name',$result[0]->company_name);
				$this->session->set_userdata('email',$result[0]->email);
				$this->session->set_userdata('userType',$result[0]->userType);
				$this->session->set_userdata('islogin',1);
				if($result[0]->userType == '2') { 
					redirect("merchant-upload-deal");
				} else if($result[0]->userType == '1') { 
					redirect("admin-dashboard");
				}	
			}else{
				
				$msgArr['loginerror'] = "Email ID or Password Incorrect.";
				$this->session->set_flashdata($msgArr);
				redirect("login");
			} 
		}
		$this->load->view('login', $data);
		
	}	
	public function checkLogin(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'emailid', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		if ($this->form_validation->run() == FALSE){//check validation
			redirect("login");
		}
		$result = $this->login_model->checkLogin();
		
		if(count($result)>0) {
			$this->session->set_userdata('userId',$result[0]->id);
			$this->session->set_userdata('company_name',$result[0]->company_name);
			$this->session->set_userdata('email',$result[0]->email);
			$this->session->set_userdata('userType',$result[0]->userType);
			$this->session->set_userdata('islogin',1);
			if($result[0]->userType == '2') { 
				redirect("merchant-upload-deal");
			} else if($result[0]->userType == '1') { 
				redirect("admin-dashboard");
			}	
		} else {
			redirect("login");
		}				
	}
	public function dashboard(){ 
		$data = array();
		$data['title'] = 'Restoranto Dashboard';
        $this->load->view('index', $data);
	}
	public function logout(){ 
		$this->session->sess_destroy();
		redirect("login");
	}
	
	public function change_password(){
			
	}	
	public function forgotpassword(){
		$this->load->model('user_model');
		$this->load->helper('general');
		if(!isset($_POST['emailid']) && trim($_POST['emailid']) ==''){
				echo "Please Enter Email id";die;
		}
		
		$emailid = trim($_POST['emailid']);
		$checkemilid = $this->user_model->check_user_email($emailid);
		if($checkemilid){
			//upadate password
			$res = $this->user_model->update_password_by_emial($emailid);
			if($res){
				$subject = 'Your Passord Reset Successfully';	
				
				$message = 'Hello '.$checkemilid[0]->owner_name.',<br><br>Your Password has been reset successfully.<br><br>Your login name : '.$emailid.'<br>Your password : '.$res.'<br><br>You may login to '.site_url('login').' using your e-mail and your password.<br><br><br>Do you have any questions? Then just send us an email info@restoranto.com to give us feedback, share your stories or just to say hello!<br><br>Team Restoranto <br><br><a href="https://www.facebook.com/sharer/sharer.php?u=www.restoranto.com" target="_blank"> Share on Facebook </a>';
				
				emailSendtoUser($emailid,$subject,$message);
				$msg = 'Your Password is reset Successfully. Please Check your Mail';
				$msgArr['loginerror'] = $msg;
				$this->session->set_flashdata($msgArr);
			}else{
				$msg ='Opps! Error please Check';
				$msgArr['loginerror'] = $msg;
				$this->session->set_flashdata($msgArr);
			}
			echo 1;die;
		}else{
			echo "Entered email id is not valid email id";die;
		}
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

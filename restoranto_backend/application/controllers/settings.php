<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		
		if(!$this->session->userdata('islogin')) {  
			redirect("login");
		} 
		
		$this->load->model('setting_model');	
	}
	
	public function index(){
		$data['setting_credit'] = $this->setting_model->get_settings();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('credit', 'Credit Limit', 'trim|required|numeric');
		$this->form_validation->set_rules('time_slot', 'Fixed Time SLot', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE){//check validation
			
		}else{
			$resc = $this->setting_model->update_setting();
			$msgArr['settingmsg'] = "Settings Updated successfully.";
			$this->session->set_flashdata($msgArr);
			redirect("settings");
		}
		
		$data['title'] = 'Setting';
		$this->template->write('title','Setting');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'settings', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
		
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

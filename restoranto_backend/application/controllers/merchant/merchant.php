<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->helper('general');
		$this->load->model('merchant_model');
		set_time_zone('Europe/Amsterdam');
		
		if(!$this->session->userdata('islogin')) {  
			redirect("login");
		} 
	}	  
	 
	 
	/*public function index()
	{	
		//$this->load->view('index');
		$data = array();
		$this->template->write('title','Whataday-Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view', $data, TRUE);
        $this->template->write_view('content', 'main-content', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->render();
	}*/
	
	public function deal_image($str){
		
			$this->load->library('form_validation');
			$this->form_validation->set_rules('deal_image', 'Please Select Deal Image');
			return false;
	}
	
	public function dashboard(){ 
		$data = array();
		$data['title'] = 'Restoranto Merchant Login';
        //$this->load->view('dashbord', $data);
        $this->template->write('title','Restoranto Merchant Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
        $this->template->write_view('content', 'main-content-merchant', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}	
	public function valid_price($str)
	{
		$old_price = str_replace(",","",$this->input->post('old_price'));
		$new_price = str_replace(",","",$this->input->post('new_price'));
		$CI = &get_instance();
		$CI->load->library('form_validation');
		
		if ($old_price < $new_price)
		{  
			$CI->form_validation->set_message('valid_price', 'New price should be less than old price');
			return false;
		}else{
			
			return 	true;
		}
	}
	public function uploadDeal($id=false){
		$data = array();
		$data['dealtype'] = $this->merchant_model->dealType();
		if($id){
				$data['dealRes'] = $this->merchant_model->get_deals($id);
		}
		
		
		if($this->input->post('submit')) { 
			 // Total Amount Paid by merchant
				$paid_amount_result = $this->merchant_model->total_amount_paid();
				$paid_amount = str_replace(",","",$paid_amount_result[0]->totalamount);
				
				// To set 1 credit value is equal to euro
				$setting_details = $this->merchant_model->setting_details();
				$one_credit_value = $setting_details[0]->setting_value;
				
				// Total credits available for paid amount of merchant
				$total_credits_available_for_paidamount = ($paid_amount/$one_credit_value);
				 
				//$total_credits_used_result = $this->merchant_model->total_credits_used();
				//$total_used_credits = $total_credits_used_result[0]->totalpeople;
				 
				// Remaining Credits 
				//$remaining_credits = ($total_credits_available_for_paidamount - $total_used_credits); 
				$no_of_people_limit = $this->input->post('noof_deals_claimed');
				
				// Total credites used by merchant to upload deal
				 $merchant_used_credits_toupload_deal = $this->merchant_model->total_credits_used_bymerchant_toupload_deal();
				
				// Merchant remaining credits to upload deal
				$remining_credits_merchant_toupload_deal = ($total_credits_available_for_paidamount - $merchant_used_credits_toupload_deal[0]->totalpeople);
				
				
				if($no_of_people_limit > $remining_credits_merchant_toupload_deal) { 
							if($remining_credits_merchant_toupload_deal < 0) {
								$remining_credits_merchant_toupload_deal = 0;
							}	
							$msgArr['uploaddealmsg'] = "You can not upload deal since your remaining credits are $remining_credits_merchant_toupload_deal";
							$this->session->set_flashdata($msgArr);
							redirect("merchant-deals");
					
				}
				else { 
					
					/*if($no_of_people_limit > $remining_credits_merchant_toupload_deal) { 
							$msgArr['uploaddealmsg'] = "You can not upload deal since your remaining credits are $remining_credits_merchant_toupload_deal";
							$this->session->set_flashdata($msgArr);
							redirect("merchant-deals");
					
					} else { */
							$this->load->library('form_validation');
							$this->form_validation->set_rules('restaurant', 'Restaurant', 'trim|required');
							//$this->form_validation->set_rules('restaurant_dt', 'Restaurant', 'trim|required');
							$this->form_validation->set_rules('deal_type', 'Deal Type', 'trim|required');
							
							$this->form_validation->set_rules('deal_title', 'Deal Title', 'trim|required');
							$this->form_validation->set_rules('deal_title_dt', 'Deal Title', 'trim|required');
							
							$this->form_validation->set_rules('price_show_option', 'Select Price display option', 'trim|required');
							if($this->input->post('price_show_option') == 1) { 
								$this->form_validation->set_rules('old_price', 'Old Deal Price', 'trim|required');
								$this->form_validation->set_rules('new_price', 'New Deal Price', 'trim|required');
								$this->form_validation->set_rules('new_price', 'New Deal Price', 'trim|required|callback_valid_price');
								
							} else if($this->input->post('price_show_option') == 2) {
								$this->form_validation->set_rules('discount', 'Discount', 'trim|required');
								$this->form_validation->set_rules('discount_dt', 'Discount', 'trim|required');
							}
							
							/*if(!$id){
								$this->form_validation->set_rules('deal_image', 'Deal Image', 'trim|required');
							}*/	
							$this->form_validation->set_rules('noof_deals_claimed', 'Number of claimed deals', 'trim|numeric|required');
							//$this->form_validation->set_rules('noof_people_claimed_deal', 'Number of people which can be claimed deal', 'trim|required');
							$this->form_validation->set_rules('deal_date', 'Select deal date', 'trim|required');
							
							if ($this->form_validation->run() == FALSE)
							{	
							}
							else
							{
								if($id){ 
									$result = $this->merchant_model->update_deals($id);
									//echo $this->db->last_query();die;
									if(count($result)>0) {
										$msgArr['uploaddealmsg'] = "Deal updated successfully";
										$this->session->set_flashdata($msgArr);
										redirect("merchant-deals");	
									}
								}else{ 
										$result = $this->merchant_model->upload_deals();
									if(count($result)>0) {
										$msgArr['uploaddealmsg'] = "Deal uploaded successfully";
										$this->session->set_flashdata($msgArr);
										redirect("merchant-deals");	
									}
								}
								 
							}	
							
					//}		
				}	
		}	
		$data['title'] = 'Restoranto Merchant Upload Deal';
		$this->template->write('title','Restoranto Merchant Upload Deal');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
        $this->template->write_view('content', 'includes/upload-deal', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
        //$this->load->view('includes/header', $data);
        //$this->load->view('includes/upload-deal', $data);
	}
	
	//function for uploaded deals
	public function deals(){
			$data['title'] = 'Uploaded Deals';
			$this->template->write('title','Uploaded Deals');
			$data['result'] = $this->merchant_model->upload_deals_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'success-deals', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	//function for claimed deals
	public function claimeddeals(){
			$data['title'] = 'Claimed Deals';
			$this->template->write('title','Claimed Deals');
			$data['result'] = $this->merchant_model->claimed_deals_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'claimed-deals', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	//public function for deactive deals 
	public function change_status(){
		$id = $_POST['id'];
		$status = $_POST['status'];
		$statusRes = $this->merchant_model->change_status($id,$status);
		if($statusRes){
				$msg = 'Deal status change successfully';
				$msg_type = 'success';
				$msgArr = array(
									'msg'=>$msg,
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
				echo $this->session->flashdata('msg');die;
				echo 1;exit();
			}else{
				$msg ='OOps! error try again';
				$msg_type = 'error';
				$msgArr = array(
									'msg'=>$msg,
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
				echo 0;exit();
			}
	}	
	//function for delete deals
	public function delete(){
		$id = $_POST['id'];
			$delRes = $this->merchant_model->delete($id);
			
			if($delRes){
				$msg = 'User deleted successfully';
				$msg_type = 'success';
				$msgArr = array(
									'msg'=>$msg,
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
				$this->session->flashdata('msg');
				
			}else{
				$msg ='OOps! error try again';
				$msg_type = 'error';
				$msgArr = array(
									'msg'=>$msg,
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
				
			}
			echo 1;
			exit();
	}
	public function by_credits(){
			$data['title'] = 'By Credits';
			$this->template->write('title','By Credits');
			//$data['result'] = $this->merchant_model->claimed_deals_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'by-credits', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	public function payment_status(){
			$data['title'] = 'Ideal Payment Status';
			$this->template->write('title','Ideal Payment Status');
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'payment-status', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	public function merchant_payment_list(){
			$data['title'] = 'Uploaded Deals';
			$this->template->write('title','Uploaded Deals');
			$data['result'] = $this->merchant_model->merchant_payment_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'merchant-payment-list', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	public function credit_details(){
			$data['title'] = 'Credit Details';
			$this->template->write('title','Credit Details');
			//$data['result'] = $this->merchant_model->credit_details();
			$data['paid_amount_result'] = $this->merchant_model->total_amount_paid();
			$data['total_credits_used'] = $this->merchant_model->total_credits_used();
			$data['setting_details'] = $this->merchant_model->setting_details();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
			$this->template->write_view('content', 'credit-details', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->helper('general');
		$this->load->model('admin_model');
		if(!$this->session->userdata('islogin')) {  
			redirect("login");
		} 
	}	  
	 
	 
	/*public function index()
	{	
		//$this->load->view('index');
		$data = array();
		$this->template->write('title','Whataday-Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view', $data, TRUE);
        $this->template->write_view('content', 'main-content', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->render();
	}*/
	public function dashboard(){ 
		$data = array();
		$data['title'] = 'Restoranto Login';
        //$this->load->view('dashbord', $data);
        $this->template->write('title','Restoranto Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'main-content-admin', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}
	public function register(){ 
		$data = array();
		$result = $this->admin_model->foodCategory();
		$data['company_food_type'] = $result;
		if($this->input->post('register')) { 
			$this->load->helper('general');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('company_loc', 'Company Location', 'trim|required');
			$this->form_validation->set_rules('company_phoneno', 'Company Phone no', 'trim|required');
			$this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
			$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
			$this->form_validation->set_rules('owner_phoneno', 'Owner Phone no', 'trim|required');
			$this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique['.AMUSER.'.email]');			
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE)
			{
				
			}
			else
			{
				$result = $this->admin_model->merchantregistration();
				if(count($result)>0) {
					
				   $name     = $this->input->post('company_name');	
				   $to 	     = $this->input->post('email');
				   $password = $this->input->post('password');
				   $subject  = "Merchant Registration";
				   $message  = "Hello ".$name.", <br /><br />\r\n";
				   $message .= "You have been registerered at Restoranto.<br />\r\n";
				   $message .= "Your account is created . Your Email : ".$to." \nYour Password : ".$password."<br />\r\n\n";
				   $message .= "You may login to http://exceptionaire.co/restoranto/ using your email and password<br /><br /><br />\r\n\n";
				   $message .= "Thanks, \nRestoranto Team";
				   
				  $retval = emailSendtoUser($to,$subject,$message);
						   
				   if( $retval == true )  
				   {
						$msgArr['registermsg'] = "Registered Successfully. An email has been sent to merchant email address. The email contains their details.";
						$this->session->set_flashdata($msgArr);
						redirect("merchant-list");				
				   }
				   else
				   {
						$msgArr['registermsg'] = "Registered Email send Failed";
						$this->session->set_flashdata($msgArr);
						redirect("merchant-list");
				   }
		   
					
					
				} 	
			}
		} 
			
		
		//$data['title'] = 'Restoranto Merchant Registration';
        //$this->load->view('dashbord', $data);
        $this->template->write('title','Restoranto Merchant Registration');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'register', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}	
	public function merchantList(){
		$data = array();
		$result = $this->admin_model->merchantList();
		$data['merchantlist'] = $result;
		
		$data['title'] = 'Restoranto Merchant List';
        $this->template->write('title','Restoranto Merchant List');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'merchant-list', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
        $this->template->render();
	}
	public function cuisine($id=false){ 
		$data = array();
		
		//$result = $this->admin_model->foodCategory();
		//$data['company_food_type'] = $result;
		if($this->input->post('cuisine')) {
			$this->load->helper('general');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('name_dt', 'Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				
			}
			else
			{	
				if($id){
					$result = $this->admin_model->updateCuisine($id);
					if($result){
						$success_msg = "Cuisine Updated Successfully";
					}else{
						$error_msg = "Cuisine not get updated";
					}
				}else{
					$result = $this->admin_model->registerCuisine();
					if($result){
						$success_msg = "Cuisine Added Successfully";
					}else{
						$error_msg = "Cuisine not get added";
					}
				}
				if(count($result)>0) {
					$msgArr['cuisinemsg'] = $success_msg;
					$this->session->set_flashdata($msgArr);
					redirect("cuisine-list");	
				} else {
					$msgArr['cuisinemsg'] = $error_msg;
					$this->session->set_flashdata($msgArr);
					redirect("cuisine-list");
				}	
			}
		} 
			
		//$data['title'] = 'Restoranto Merchant Registration';
        //$this->load->view('dashbord', $data);
        if($id){
			$data['cuisine_details'] = $this->admin_model->get_cuisine_by_id($id);	
		}
        $this->template->write('title','Restoranto New Cuisine');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'cuisine', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}
	public function cuisineList(){
		$data = array();
		$result = $this->admin_model->cuisineList();
		$data['cuisinelist'] = $result;
		
		$data['title'] = 'Restoranto Cuisine List';
        $this->template->write('title','Restoranto Cuisine List');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'cuisine-list', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
        $this->template->render();
	}	
	
	public function delete_cusine($id){
		$delRes = $this->admin_model->delete_cusine($id);
		if($delRes) {
			$msgArr['cuisinemsg'] = "Cuisine Deleted Successfully";
			$this->session->set_flashdata($msgArr);
			redirect("cuisine-list");exit();	
		} else {
			$msgArr['cuisinemsg'] = "Cuisine Not-Deleted Successfully";
			$this->session->set_flashdata($msgArr);
			redirect("cuisine-list");exit();
		}	
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

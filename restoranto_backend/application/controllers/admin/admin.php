<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->helper('general');
		$this->load->model('admin_model');
		set_time_zone('Europe/Amsterdam');
		if($this->uri->segment(1) != 'register') { 
			if(!$this->session->userdata('islogin')) {  
				redirect("login");
			} 
		}
	}	  
	 
	 
	/*public function index()
	{	
		//$this->load->view('index');
		$data = array();
		$this->template->write('title','Whataday-Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view', $data, TRUE);
        $this->template->write_view('content', 'main-content', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->render();
	}*/
	public function dashboard(){ 
		$data = array();
		$data['title'] = 'Restoranto Login';
        //$this->load->view('dashbord', $data);
        $this->template->write('title','Restoranto Dashbord');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'main-content-admin', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}
	
	public function valid_url($url)
	{
		
		$CI = &get_instance();
		$CI->load->library('form_validation');
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		if (!preg_match($pattern, $url))
		{
			 $CI->form_validation->set_message('valid_url', 'Please enter a valid URL. Ex. http://abc.com');
			return false;
		}else{
				
			return 	true;
		}

		
	}
	public function register(){ 
		$data = array();
		$result = $this->admin_model->cuisineList();
		$data['company_food_type'] = $result;
		if($this->input->post('register')) { 
			$this->load->helper('general');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			//$this->form_validation->set_rules('company_name_dutch', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('company_loc', 'Company Location', 'trim|required');
			$this->form_validation->set_rules('company_phoneno', 'Company Phone no.', 'trim|required');
			$this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
			//$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required|alpha');
			$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
			$this->form_validation->set_rules('owner_phoneno', 'Owner Phone no.', 'trim|required|numeric');
			//$this->form_validation->set_rules('company_website', 'Company Website', 'trim|required|callback_valid_url');
			$this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique['.AMUSER.'.email]');			
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE)
			{
				
			}
			else
			{ 
				$result = $this->admin_model->merchantregistration(); 
				if(count($result)>0) {
					
				   $name     = $this->input->post('company_name');	
				   $to 	     = $this->input->post('email');
				   $password = $this->input->post('password');
				   $subject  = "Welcome on board";
				   $message  = "Hello ".$name.", <br /><br />\r\n";
				   $message .= "Thanks for joining Restoranto.<br /><br />";
				   $message .= "Your account is created.<br />Your login name : ".$to." <br /> You password : ".$password."<br /><br />";
				   $message .= "You may login to ".site_url()." using your email and password.<br /><br />";
				   $message .= "Your next steps are:<br /><br />1. Create an offer<br />2. Select the number of people that can claim your offer<br />3. Make a brief description about the deal and upload a picture of the meal or your restaurant<br />4. Submit the deal<br />5. Greet your clients<br /><br />";
				   $message .= "We hope you will have a great experience and we like to thank you for choosing Restoranto. We are really excited about working together to attract last-minute local diners to your restaurant.<br /><br />";
				   $message .= "Do you have any questions? Then just send us an email info@restoranto.com to give us feedback, share your stories or just to say hello!<br /><br />";
				   $message .= "Team Restoranto";
				   
				   //$headers  = 'MIME-Version: 1.0' . PHP_EOL;
				   //$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                   //$headers .= "From:noreply@restoranto.com​"; 
				   //$retval =  mail($to,$subject,$message,$headers);
				   
				   $config = Array('mailtype' => 'html');
				   $this->load->library('email',$config);
					$this->email->from('noreply@restoranto.com', 'Restoranto');
					$this->email->to($to);
					$this->email->subject($subject);
					$this->email->message($message);

					$retval = $this->email->send();

				 				  	
				  //$retval = emailSendtoUser($to,$subject,$message);
						   
				   if( $retval == true )  
				   {
						$msgArr['registermsg'] = "Registered Successfully. An email has been sent to merchant email address.";
						$this->session->set_flashdata($msgArr);
						if(!$this->session->userdata('islogin')){
							redirect("register");
						} else {	
							redirect("merchant-list");				
						}	
				   }
				   else
				   {
						$msgArr['registermsg'] = "Registered Successfully. Registered Email send Failed. ";
						$this->session->set_flashdata($msgArr);
						if(!$this->session->userdata('islogin')){
							redirect("register");
						} else {
							redirect("merchant-list");
						}			
				   }
		   
					
					
				} 	
			}
		} 
			
		
		//$data['title'] = 'Restoranto Merchant Registration';
        //$this->load->view('dashbord', $data);
        $this->template->write('title','Restoranto Merchant Registration');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		if($this->session->userdata('islogin')){
			$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
		}	
        $this->template->write_view('content', 'register', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}	
	public function merchantList(){
		$data = array();
		$result = $this->admin_model->merchantList();
		$data['merchantlist'] = $result;
		
		$data['title'] = 'Restoranto Merchant List';
        $this->template->write('title','Restoranto Merchant List');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'merchant-list', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
        $this->template->render();
	}
	public function userList(){
		$data = array();
		$result = $this->admin_model->userList();
		$data['userlist'] = $result;
		
		$data['title'] = 'Restoranto Users List';
        $this->template->write('title','Restoranto Users List');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'user-list', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
        $this->template->render();
	}
	public function cuisine($id=false){ 
		$data = array();
		
		//$result = $this->admin_model->foodCategory();
		//$data['company_food_type'] = $result;
		if($this->input->post('cuisine')) {
			$this->load->helper('general');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('name_dt', 'Name', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				
			}
			else
			{	
				if($id){
					$result = $this->admin_model->updateCuisine($id);
					if($result){
						$success_msg = "Cuisine Updated Successfully";
					}else{
						$error_msg = "Cuisine not get updated";
					}
				}else{
					$result = $this->admin_model->registerCuisine();
					if($result){
						$success_msg = "Cuisine Added Successfully";
					}else{
						$error_msg = "Cuisine not get added";
					}
				}
				if(count($result)>0) {
					$msgArr['cuisinemsg'] = $success_msg;
					$this->session->set_flashdata($msgArr);
					redirect("cuisine-list");	
				} else {
					$msgArr['cuisinemsg'] = $error_msg;
					$this->session->set_flashdata($msgArr);
					redirect("cuisine-list");
				}	
			}
		} 
			
		//$data['title'] = 'Restoranto Merchant Registration';
        //$this->load->view('dashbord', $data);
        if($id){
			$data['cuisine_details'] = $this->admin_model->get_cuisine_by_id($id);	
		}
        $this->template->write('title','Restoranto New Cuisine');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'cuisine', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
         $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
	}
	public function cuisineList(){
		$data = array();
		$result = $this->admin_model->cuisineList();
		$data['cuisinelist'] = $result;
		
		$data['title'] = 'Restoranto Cuisine List';
        $this->template->write('title','Restoranto Cuisine List');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'cuisine-list', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
        $this->template->render();
	}	
	
	public function delete_cusine($id){
		$delRes = $this->admin_model->delete_cusine($id);
		if($delRes) {
			$msgArr['cuisinemsg'] = "Cuisine Deleted Successfully";
			$this->session->set_flashdata($msgArr);
			redirect("cuisine-list");exit();	
		} else {
			$msgArr['cuisinemsg'] = "Cuisine Not-Deleted Successfully";
			$this->session->set_flashdata($msgArr);
			redirect("cuisine-list");exit();
		}	
	}
	
	public function change_cusine_status(){
		$id = $_POST['id'];
		$status = $_POST['status'];
		$statusRes = $this->admin_model->change_cusine_status($id,$status);
		if($statusRes){
				//$msg = 'User status change successfully';
				$msgArr['cuisinemsg'] = "Cuisine Status Change Successfully";
				$this->session->set_flashdata($msgArr);
				echo 1;die;
			}else{
				//$msg ='OOps! error try again';
				$msgArr['cuisinemsg'] = "OOps! error try again";
				$this->session->set_flashdata($msgArr);
				echo 0;exit();
			}
			
	}
	
	public function delete(){
			$userid = $_POST['id'];
			$delRes = $this->admin_model->delete_user($userid);
			
			if($delRes){
				//$msg = 'User deleted successfully';
				$msgArr['registermsg'] = "User deleted successfully";
				$this->session->set_flashdata($msgArr);
				
			}else{
				$msgArr['registermsg'] = "OOps! error try again";
				$this->session->set_flashdata($msgArr);
				
			}
			echo 1;
			exit();
	}
	public function change_user_status(){
		$userid = $_POST['id'];
		$status = $_POST['status'];
		$statusRes = $this->admin_model->change_status($userid,$status);
		if($statusRes){
				//$msg = 'User status change successfully';
				$msgArr['registermsg'] = "User status change successfully";
				$this->session->set_flashdata($msgArr);
				echo 1;die;
			}else{
				//$msg ='OOps! error try again';
				$msgArr['registermsg'] = "OOps! error try again";
				$this->session->set_flashdata($msgArr);
				echo 0;exit();
			}
			
	}
	//function for uploaded deals
	public function deals(){
			$data['title'] = 'Uploaded Deals';
			$this->template->write('title','Uploaded Deals');
			$data['result'] = $this->admin_model->upload_deals_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
			$this->template->write_view('content', 'admin-deals', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	
	public function payment_list(){
			$data['title'] = 'Uploaded Deals';
			$this->template->write('title','Uploaded Deals');
			$data['result'] = $this->admin_model->payment_list();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
			$this->template->write_view('content', 'payment-list', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}
	public function merchant_credits_details(){
			$data['title'] = 'Merchant Credit Details';
			$this->template->write('title','Merchant Credit Details');
			$data['result'] = $this->admin_model->merchant_credits_details();
			$this->template->write_view('header', 'includes/header', $data, TRUE);
			$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
			$this->template->write_view('content', 'merchant-credits-details', $data, TRUE);
			$this->template->write_view('footer', 'includes/footer', $data, TRUE);
			$this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
			$this->template->write_view('scripts', 'includes/tablescripts', $data, TRUE);
			$this->template->render();
	}	
	
	public function uploadDeal($id=false){
		$this->load->model('merchant_model');
		$data = array();
		$data['dealtype'] = $this->merchant_model->dealType();
		if($id){
				$data['dealRes'] = $this->merchant_model->get_deals($id);
		}
		
		
		if($this->input->post('submit')) { 
			$this->load->library('form_validation');
			$this->form_validation->set_rules('restaurant', 'Restaurant', 'trim|required');
			//$this->form_validation->set_rules('restaurant_dt', 'Restaurant', 'trim|required');
			$this->form_validation->set_rules('deal_type', 'Deal Type', 'trim|required');
			
			$this->form_validation->set_rules('deal_title', 'Deal Title', 'trim|required');
			$this->form_validation->set_rules('deal_title_dt', 'Deal Title', 'trim|required');
			
			$this->form_validation->set_rules('price_show_option', 'Select Price display option', 'trim|required');
			if($this->input->post('price_show_option') == 1) { 
				$this->form_validation->set_rules('old_price', 'Old Deal Price', 'trim|required');
				$this->form_validation->set_rules('new_price', 'New Deal Price', 'trim|required');
				$this->form_validation->set_rules('new_price', 'New Deal Price', 'trim|required|callback_valid_price');
				
			} else if($this->input->post('price_show_option') == 2) {
				$this->form_validation->set_rules('discount', 'Discount', 'trim|required');
				$this->form_validation->set_rules('discount_dt', 'Discount', 'trim|required');
			}
			
			/*if(!$id){
				$this->form_validation->set_rules('deal_image', 'Deal Image', 'trim|required');
			}*/	
			$this->form_validation->set_rules('noof_deals_claimed', 'Number of claimed deals', 'trim|numeric|required');
			//$this->form_validation->set_rules('noof_people_claimed_deal', 'Number of people which can be claimed deal', 'trim|required');
			$this->form_validation->set_rules('deal_date', 'Select deal date', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{	
			}
			else
			{
				if($id){ 
					$result = $this->merchant_model->update_deals($id);
					//echo $this->db->last_query();die;
					if(count($result)>0) {
						$msgArr['uploaddealmsg'] = "Deal updated successfully";
						$this->session->set_flashdata($msgArr);
						redirect("admin-deals");	
					}
				}else{ 
						$result = $this->merchant_model->upload_deals();
					if(count($result)>0) {
						$msgArr['uploaddealmsg'] = "Deal uploaded successfully";
						$this->session->set_flashdata($msgArr);
						redirect("admin-deals");	
					}
				}
				 
			}	
		}	
		$data['title'] = 'Restoranto Merchant Upload Deal';
		$this->template->write('title','Restoranto Merchant Upload Deal');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-admin', $data, TRUE);
        $this->template->write_view('content', 'includes/upload-deal', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();
        //$this->load->view('includes/header', $data);
        //$this->load->view('includes/upload-deal', $data);
	}
	
	public function credit_details(){
			
			$userid = (isset($_POST['id']) && $_POST['id']!='')?$_POST['id']:0; 
			$paid_amount_result = $this->admin_model->total_amount_paid($userid);
			$total_credits_used = $this->admin_model->total_credits_used($userid);
			$setting_details = $this->admin_model->setting_details($userid);
			
			if(isset($paid_amount_result) && count($paid_amount_result)>0):
				if($paid_amount_result[0]->totalamount)
					$data['paid_ammount'] =  $paid_amount_result[0]->totalamount." EURO";
				else 
					$data['paid_ammount'] =  "0 EURO";
			endif;
			
			$total_amount = str_replace(",","",$paid_amount_result[0]->totalamount);
			if(isset($setting_details) && count($setting_details)>0):
				$one_credit_value = $data['one_credit_value'] = $setting_details[0]->setting_value;
			else:
				$one_credit_value =	$data['one_credit_value'] =  "1";
			endif;
			
			$total_credits_for_paidamount = ($total_amount/$one_credit_value); 
			$data['total_credits_for_paidamount'] = $total_credits_for_paidamount;
			
			
			if(isset($total_credits_used) && count($total_credits_used)>0):
				if($total_credits_used[0]->totalpeople) 
					$data['total_used_credits'] = $total_credits_used[0]->totalpeople;
				else 
					$data['total_used_credits'] = "0";
			endif;
			
			if($total_credits_for_paidamount) 
				$remaining_credits = @$total_credits_for_paidamount - @$total_credits_used[0]->totalpeople;
			else 	
				$remaining_credits = 0;
			$data['remaining_credits'] = $remaining_credits;
			//$data['total_credits_for_paidamount'] = $total_credits_for_paidamount;
			//$data['total_used_credits'] = $total_credits_used[0]->totalpeople;
			
			//$data['remaining_credits'] = (isset($total_credits_for_paidamount))?$total_credits_for_paidamount:0 - (isset($total_used_credits))?$total_used_credits:0;
			
			echo json_encode($data);die;
			
	}
	
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

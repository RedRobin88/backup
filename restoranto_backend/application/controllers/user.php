<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		
		if(!$this->session->userdata('islogin')) {  
			redirect("login");
		} 
		
		$this->load->model('login_model');	
	}
	
	public function check_old_pass($str){
			$oldPassRes = $this->login_model->check_old_pass($str,$this->session->userdata('userId'));
			if(!$oldPassRes){
				$this->load->library('form_validation');
				 $this->form_validation->set_message('check_old_pass', 'Old Password not matches.');
				return false;
			}else{
					return true;
			}
	}
	
	public function change_password_form(){
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('pass', 'Old Password', 'required|callback_check_old_pass');
		$this->form_validation->set_rules('newpass', 'New Password', 'required|matches[confirmpass]');
		$this->form_validation->set_rules('confirmpass', 'Confirm Password', 'required');
		if ($this->form_validation->run() == FALSE){//check validation
			
		}else{	
				$newpass = $this->input->post('newpass');;
				$dataArr = array(
										'password'=>md5($newpass)
									);
				
					$insRec = $this->login_model->update_password($dataArr);
					
					if($insRec){
						
						$msgArr['passwordmsg'] = 'Merchand Password Reset Successfully.';
										
						$this->session->set_flashdata($msgArr);
						
					}else{
						$msgArr['passwordmsg'] = 'Opps! error please try agin.';
										
						$this->session->set_flashdata($msgArr);
						
					}
					redirect("user/change_password_form");
		}
		
		
		$data['title'] = 'Change Password';
		$this->template->write('title','Change Password');
		$this->template->write_view('header', 'includes/header', $data, TRUE);
		$this->template->write_view('left_bar','includes/left-view-merchant', $data, TRUE);
        $this->template->write_view('content', 'change-password', $data, TRUE);
        $this->template->write_view('footer', 'includes/footer', $data, TRUE);
        $this->template->write_view('right_bar', 'includes/right-view', $data, TRUE);
        $this->template->write_view('scripts', 'includes/allscripts', $data, TRUE);
        $this->template->render();	
	}	  
	
	public function change_password(){
		$data = array();
		$dataheader = array();
		$datafooter = array();
		
		
		if(trim($_POST['hform']) == '1'){
			
				$oldpass = $_POST['oldpassword'];
				$newpass = $_POST['newpassword'];
				$oldPassRes = $this->login_model->check_old_pass($oldpass,$this->session->userdata('userId'));
				if($oldPassRes){	
					$dataArr = array(
										'password'=>md5($newpass)
									);
				
					$insRec = $this->login_model->update_password($dataArr);
					$msg = 'Password Update successfully';
					
					if($insRec){
						$msg = $msg;
						$msg_type = 'success';
						$msgArr = array(
											'msg'=>$msg,
											'msg_type'=>$msg_type
										);
						$this->session->set_flashdata($msgArr);
						
					}else{
						$msg ='Opps! error please try agin';
						$msg_type = 'error';
						$msgArr = array(
											'msg'=>$msg,
											'msg_type'=>$msg_type
										);
						$this->session->set_flashdata($msgArr);
						
					}
				}else{
					echo 5;die;
				}
					
		}	
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';
$route['logout'] = "login/logout";
$route['register'] = "admin/admin/register";
$route['admin-dashboard'] = "admin/admin/dashboard";

$route['merchant-list'] = "admin/admin/merchantList";
$route['user-list'] = "admin/admin/userList";
$route['cuisine-list'] = "admin/admin/cuisineList";
$route['cuisine'] = "admin/admin/cuisine";
$route['cuisine/edit/(:num)'] = "admin/admin/cuisine/$1";
$route['cuisine/delete/(:num)'] = "admin/admin/delete_cusine/$1";
$route['change-cuisine-status'] = "admin/admin/change_cusine_status";

$route['merchant-dashboard'] = "merchant/merchant/dashboard";
$route['merchant-upload-deal'] = "merchant/merchant/uploadDeal";
$route['merchant-upload-deal/(:num)'] = "merchant/merchant/uploadDeal/$1";
$route['manage-deals/delete'] = "merchant/merchant/delete";
$route['manage-deals/change_status'] = "merchant/merchant/change_status";

$route['user-management/delete'] = "admin/admin/delete";
$route['user-management/change_user_status'] = "admin/admin/change_user_status";

$route['merchant-deals'] = "merchant/merchant/deals";
$route['merchant-claimeddeals'] = "merchant/merchant/claimeddeals";
$route['buy-credits'] = "merchant/merchant/by_credits";
$route['payment-status'] = "merchant/merchant/payment_status";

$route['admin-deals'] = "admin/admin/deals";
$route['payment-list'] = "admin/admin/payment_list";
$route['merchant-payment-list'] = "merchant/merchant/merchant_payment_list";
$route['merchant-credits-details'] = "admin/admin/merchant_credits_details";
$route['credit-details'] = "merchant/merchant/credit_details";

$route['edit-details/(:num)'] = "admin/admin/uploadDeal/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */

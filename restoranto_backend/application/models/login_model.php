<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	public function checkLogin(){
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$this->db->select('*');
		$this->db->from(AMUSER);
		//$this->db->where('userType',1);
		$this->db->where('status',1);
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
		//$res = $query->result();
		//print_r($res);
	}
	
	
	//function for check old password
	public function check_old_pass($password,$userid){
			$this->db->select('*');
			$this->db->from(AMUSER);
			$this->db->where('id',$userid);
			$this->db->where('password',md5($password));
			$query = $this->db->get();
			$recs = $query->result();
			if(count($recs)>0){
				return 1;
			}else{
					return 0;
			}
	}	
	
	//update user password in admin table
	public function update_password($dataArr){
			$this->db->where('id',$this->session->userdata('userId'));
			return $this->db->update(AMUSER,$dataArr);		
			
	}
	
}
?>

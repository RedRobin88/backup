<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model {
	
	public function get_settings(){
		$this->db->select('*');
		$this->db->from(SETTINGS);
		$q = $this->db->get();
		return $q->result();
	}
	
	public function update_setting(){
			$this->db->where('setting_key','credit_limit');
			$this->db->update(SETTINGS,array('setting_value'=>$this->input->post('credit')));
			
			$this->db->where('setting_key','fixed_deal_time_slot');
			return $this->db->update(SETTINGS,array('setting_value'=>$this->input->post('time_slot')));
	}
	
}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {
	public function merchantregistration(){
		$dataArr = array();
		//$dataArr = $_POST;
		
		$dataArr['company_name'] = $this->input->post('company_name');
		//$dataArr['company_name_dutch'] = $this->input->post('company_name_dutch');
		$dataArr['company_loc'] = $this->input->post('company_loc');
		$dataArr['company_food_type'] = $this->input->post('company_food_type');
		$dataArr['company_phoneno'] = $this->input->post('company_phoneno');
		$dataArr['company_website'] = $this->input->post('company_website');
		$dataArr['owner_name'] = $this->input->post('owner_name');
		$dataArr['owner_phoneno'] = $this->input->post('owner_phoneno');
		$dataArr['email'] = $this->input->post('email');
		$dataArr['password	'] = md5($this->input->post('password'));
		$dataArr['userType'] = '2';
		$dataArr['status'] = '1';
		$this->db->insert(AMUSER,$dataArr);
		return $this->db->insert_id();
	}	
	public function foodCategory(){
		$this->db->select('*');
		$this->db->from(FOOD);
		$this->db->where('status','1');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}	
	public function merchantList(){
		$this->db->select('*');
		$this->db->from(AMUSER);
		$this->db->where('status','1');
		$this->db->where('userType','2');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}	
	public function userList(){
		$this->db->select('u.*');
		$this->db->from(FRONT_USER.' as u');
		//$this->db->join(DEALTYPE.' as d','d.id=u.interest','inner');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}	
	public function cuisineList(){
		$this->db->select('*');
		$this->db->from(DEALTYPE);
		$this->db->where('status','1');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function registerCuisine(){
		$dataArr = array();
		//$dataArr = $_POST;
		
		$dataArr['name'] = $this->input->post('name');
		$dataArr['name_dt'] = $this->input->post('name_dt');
		$dataArr['status'] = $this->input->post('status');
		
		$this->db->insert(DEALTYPE,$dataArr);
		return $this->db->insert_id();
	}
	//function for update cuisine
	public function updateCuisine($id){
		$dataArr['name'] = $this->input->post('name');
		$dataArr['name_dt'] = $this->input->post('name_dt');
		$dataArr['status'] = $this->input->post('status');
		$this->db->where('id',$id);
		return $this->db->update(DEALTYPE,$dataArr);
	} 	
	//function for get cusine by id
	public function get_cuisine_by_id($id){
			$this->db->select('*');
			$this->db->from(DEALTYPE);
			$this->db->where('id',$id);
			$q = $this->db->get();
			return $q->result();
	}
	//function for deleted cusine
	public function delete_cusine($id){
		return $this->db->delete(DEALTYPE,array('id'=>$id));
	}
	
	//function for deleted user
	public function delete_user($id){
		return $this->db->delete(FRONT_USER,array('id'=>$id));
	}
	
	//function for chage user status
	public function change_status($userid,$status){
			
			$data = array(
               'status' => $status
            );

			$this->db->where('id', $userid);
			return $this->db->update(FRONT_USER, $data); 
	}
	public function change_cusine_status($id,$status){
			
			$data = array(
               'status' => $status
            );

			$this->db->where('id', $id);
			return $this->db->update(DEALTYPE, $data); 
	}
	public function upload_deals_list(){
		$userid = $this->session->userdata('userId');
		$current_time = date('Y-m-d H:i:s');
		
		//$query = $this->db->query("SELECT ud.*, d.name FROM (".UPLOAD_DEALS." as ud) INNER JOIN ".DEALTYPE." as d ON d.id=ud.type_of_meal WHERE ud.merchant_id = '$userid' AND ('$current_time' BETWEEN ADDTIME(ud.dealdate,'-24:00:00') AND ADDTIME(ud.dealdate,'04:00:00')) ORDER BY ud.id desc");
		$query = $this->db->query("SELECT ud.*, d.name,TIMEDIFF('$current_time',ud.dealdate) as timedif FROM (".UPLOAD_DEALS." as ud) INNER JOIN ".DEALTYPE." as d ON d.id=ud.type_of_meal ORDER BY ud.id desc");
		//echo $this->db->last_query();
		return $query->result();
	}
	
	public function payment_list(){
		$this->db->select('cd.*,mu.owner_name');
		$this->db->from(CREADIT_DETAILS .' as cd'); 
		$this->db->join(AMUSER .' as mu','cd.merhcant_id=mu.id','left outer');
		$q = $this->db->get();
		return $q->result();
	}
	public function merchant_credits_details(){
		/*$this->db->select('cd.*,mu.owner_name');
		$this->db->from(CREADIT_DETAILS .' as cd'); 
		$this->db->join(AMUSER .' as mu','cd.merhcant_id=mu.id','inner');
		$q = $this->db->get();
		return $q->result();
		*/
	}	
	
	public function total_amount_paid($userid){
			
			$this->db->select('sum(amount) as totalamount');
			$this->db->from(CREADIT_DETAILS .' as cd');
			$this->db->where('cd.merhcant_id',$userid);
			$q = $this->db->get();
			//echo $this->db->last_query();
			return $q->result();
	}
	public function total_credits_used($userid){
			$this->db->select('sum(no_of_people) as totalpeople');
			$this->db->from(CLAIMED_DEALS.' as cd');
			$this->db->join(UPLOAD_DEALS.' as ud','cd.deal_id=ud.id','inner');
			$this->db->where('ud.merchant_id',$userid);
			$q = $this->db->get();
			//echo $this->db->last_query();
			return $q->result();
	}	
	public function setting_details(){
			$this->db->select('*');
			$this->db->from(SETTINGS);
			$q = $this->db->get();
			//echo $this->db->last_query();
			return $q->result();
	}
}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_model extends CI_Model {
	public function dealType(){
		$this->db->select('*');
		$this->db->from(DEALTYPE);
		$this->db->where('status','1');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	/*public function uploadDeal(){ //deal_image
		$dataArr = array();		
		$dataArr['company_name'] = $this->input->post('company_name');
		$dataArr['company_loc'] = $this->input->post('company_loc');
		$dataArr['company_food_type'] = $this->input->post('company_food_type');
		$dataArr['company_phoneno'] = $this->input->post('company_phoneno');
		$dataArr['company_website'] = $this->input->post('company_website');
		$dataArr['owner_name'] = $this->input->post('owner_name');
		$dataArr['owner_phoneno'] = $this->input->post('owner_phoneno');
		$dataArr['email'] = $this->input->post('email');
		$dataArr['password	'] = md5($this->input->post('password'));
		$dataArr['merchant_id'] = $this->session->user_data('userId');
		$dataArr['userType'] = '2';
		$dataArr['status'] = '1';
		$this->db->insert(MERCHANT_DEALS,$dataArr);
		return $this->db->insert_id();
	}	*/
	
	public function upload_deals(){ //image
		$dataArr = array();		
		
		if($_FILES['deal_image']['name']) {
			$this->config =  array(
					  'upload_path'     => APPPATH."../assets/images/deals",
					  'upload_url'      => base_url()."files/",
					  'allowed_types'   => "gif|jpg|png|jpeg",
					  'overwrite'       => TRUE,
					  'max_size'        => "1000KB",
					  'max_height'      => "768",
					  'max_width'       => "1024"  
					);
					
				$this->load->library('upload', $this->config);
				if($this->upload->do_upload('deal_image'))
				{
					$data = array('upload_data' => $this->upload->data());
					
					
					$dataArr['image'] = site_url().'assets/images/deals/'.$data['upload_data']['file_name'];
					
					
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = APPPATH."../assets/images/deals/".$data['upload_data']['file_name'];
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 232;
					$config['height'] = 160;
					$new_image_path = site_url()."assets/images/deals/thumb_".$data['upload_data']['file_name'];
					$config['new_image'] = APPPATH."../assets/images/deals/thumb_".$data['upload_data']['file_name'];

					$this->load->library('image_lib', $config);

					if ( ! $this->image_lib->resize())
					{
						
						$msgArr['uploadimgmsg'] = $this->image_lib->display_errors();
						$this->session->set_flashdata($msgArr);
						redirect("restoranto/merchant-upload-deal");
					}else{
						$dataArr['image'] = $new_image_path;
					}
					
				}
				else
				{
				  // echo $this->upload->display_errors();die;
				   
					$msgArr['uploadimgmsg'] = $this->upload->display_errors();
					$this->session->set_flashdata($msgArr);
					redirect("restoranto/merchant-upload-deal");
				}
		}
		$dataArr['restaurant'] = $this->input->post('restaurant');
		$dataArr['restaurant_dt'] = $this->input->post('restaurant_dt');
		$dataArr['type_of_meal'] = $this->input->post('deal_type');
		$dataArr['other_type_of_meal'] = $this->input->post('other_deal_type');
		$dataArr['discount'] = $this->input->post('discount');
		$dataArr['discount_dt'] = $this->input->post('discount_dt');
		$dataArr['n_of_d_claimed'] = $this->input->post('noof_deals_claimed');
		//$dataArr['n_of_people_claimed'] = $this->input->post('noof_people_claimed_deal');
		$dataArr['dealdate'] = $this->input->post('deal_date');
		$dataArr['lat'] = $this->input->post('lat');
		$dataArr['lng'] = $this->input->post('lng');
		$dataArr['merchant_id'] = $this->session->userdata('userId');
		$dataArr['status'] = '1';
		$dataArr['added_date'] = date('Y-m-d H:i:s');
		$dataArr['price_show_option'] = $this->input->post('price_show_option');
		$dataArr['old_price'] = $this->input->post('old_price');
		$dataArr['new_price'] = $this->input->post('new_price');
		$dataArr['deal_title'] = $this->input->post('deal_title');
		$dataArr['deal_title_dt'] = $this->input->post('deal_title_dt');
		$this->db->insert(UPLOAD_DEALS,$dataArr);
		
		return $this->db->insert_id();
	}
	
	public function update_deals($id){
			$dataArr = array();		
		
		if($_FILES['deal_image']['name'] !=''){
			$this->config =  array(
					  'upload_path'     => APPPATH."../assets/images/deals",
					  'upload_url'      => base_url()."files/",
					  'allowed_types'   => "gif|jpg|png|jpeg",
					  'overwrite'       => TRUE,
					  'max_size'        => "1000KB",
					  'max_height'      => "768",
					  'max_width'       => "1024"  
					);
					
				$this->load->library('upload', $this->config);
				if($this->upload->do_upload('deal_image'))
				{
					$data = array('upload_data' => $this->upload->data());
					
					
					$dataArr['image'] = site_url().'assets/images/deals/'.$data['upload_data']['file_name'];
					
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = APPPATH."../assets/images/deals/".$data['upload_data']['file_name'];
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 232;
					$config['height'] = 160;
					$new_image_path = site_url()."assets/images/deals/thumb_".$data['upload_data']['file_name'];
					$config['new_image'] = APPPATH."../assets/images/deals/thumb_".$data['upload_data']['file_name'];

					$this->load->library('image_lib', $config);

					if ( ! $this->image_lib->resize())
					{
						
						$msgArr['uploadimgmsg'] = $this->image_lib->display_errors();
						$this->session->set_flashdata($msgArr);
						redirect("merchant-upload-deal/".$id);
					}else{
						$dataArr['image'] = $new_image_path;
					}
					
					
				}
				else
				{
					$msgArr['uploadimgmsg'] = $this->upload->display_errors();
					$this->session->set_flashdata($msgArr);
					redirect("merchant-upload-deal/".$id);
				  
				}
		}else{
			$dataArr['image'] = $this->input->post('hidden_img');
		}
		
		$dataArr['restaurant'] = $this->input->post('restaurant');
		$dataArr['restaurant_dt'] = $this->input->post('restaurant_dt');
		$dataArr['type_of_meal'] = $this->input->post('deal_type');
		$dataArr['other_type_of_meal'] = $this->input->post('other_deal_type');
		$dataArr['discount'] = $this->input->post('discount');
		$dataArr['discount_dt'] = $this->input->post('discount_dt');
		$dataArr['n_of_d_claimed'] = $this->input->post('noof_deals_claimed');
		//$dataArr['n_of_people_claimed'] = $this->input->post('noof_people_claimed_deal');
		$dataArr['dealdate'] = $this->input->post('deal_date');
		$dataArr['merchant_id'] = $this->session->userdata('userId');
		$dataArr['lat'] = $this->input->post('lat');
		$dataArr['lng'] = $this->input->post('lng');
		$dataArr['added_date'] = date('Y-m-d H:i:s');
		$dataArr['price_show_option'] = $this->input->post('price_show_option');
		$dataArr['old_price'] = $this->input->post('old_price');
		$dataArr['new_price'] = $this->input->post('new_price');
		$dataArr['deal_title'] = $this->input->post('deal_title');
		$dataArr['deal_title_dt'] = $this->input->post('deal_title_dt');
		$this->db->where("id",$id);
		return $this->db->update(UPLOAD_DEALS,$dataArr);
	}
	
	public function upload_deals_list(){
		$userid = $this->session->userdata('userId');
		$current_time = date('Y-m-d H:i:s');
		
		/*$this->db->select('ud.*,d.name');
		$this->db->from(UPLOAD_DEALS.' as ud');
		$this->db->join(DEALTYPE.' as d','d.id=ud.type_of_meal','inner');
		$this->db->where('ud.merchant_id',$userid);
		$this->db->where($current_time." BETWEEN ud.dealdate AND ADDTIME(dealdate,'04:00:00')");
		$this->db->order_by('ud.id','desc');
		$query = $this->db->get();*/
		//echo "SELECT ud.*, d.name FROM (".UPLOAD_DEALS." as ud) INNER JOIN ".DEALTYPE." as d ON d.id=ud.type_of_meal WHERE ud.merchant_id = '$userid' AND ('$current_time' BETWEEN ADDTIME(ud.dealdate,'-24:00:00') AND ADDTIME(ud.dealdate,'04:00:00')) ORDER BY ud.id desc";
		$query = $this->db->query("SELECT ud.*, d.name FROM (".UPLOAD_DEALS." as ud) INNER JOIN ".DEALTYPE." as d ON d.id=ud.type_of_meal WHERE ud.merchant_id = '$userid' AND ('$current_time' BETWEEN ADDTIME(ud.dealdate,'-24:00:00') AND ADDTIME(ud.dealdate,'04:00:00')) ORDER BY ud.id desc");
		//echo $this->db->last_query(); die;
		return $query->result();
	}
	public function claimed_deals_list(){
		$userid = $this->session->userdata('userId');
		
		$this->db->select('ud.*,dt.name,d.userid,d.claimed_datetime');
		$this->db->from(UPLOAD_DEALS.' as ud');
		$this->db->join(CLAIMED_DEALS.' as d','d.deal_id=ud.id','inner');
		$this->db->join(DEALTYPE.' as dt','dt.id=ud.type_of_meal','inner');
		$this->db->where('ud.merchant_id',$userid);
		$this->db->order_by('ud.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	//function for getting deal information
	public function get_deals($id){
		$this->db->select('*');
		$this->db->from(UPLOAD_DEALS);
		$this->db->where('id',$id);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	//function for deactive deals
	
	public function change_status($id,$status){
		$dataArr['status'] = ($status == 1)?0:1;
		$this->db->where("id",$id);
		return $this->db->update(UPLOAD_DEALS,$dataArr);
	}
	
	//function for delete deals
	
	public function delete($id){
		return $this->db->delete(UPLOAD_DEALS,array('id'=>$id));
	}
	
	// To fetch userinfo corresponding to userid
	public function userinfo($id){
		$this->db->select('*');
		$this->db->from(FRONT_USER);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	
}
?>

<?php
/* Function to check error on form **/
function check_error($name){
	if(form_error($name)) { 
		$error_class = 'has-error';
	} else {
		$error_class = '';
	}
	return $error_class;
}	
function emailSendtoUser($email,$subject,$message){
   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
   $headers .= "From:noreply@restoranto.com​\r\n";
   $retval 	 = mail($email,$subject,$message,$headers);
   return $retval;
}	
function userinfo($userid){
	$CI = &get_instance();
	$CI->load->model('merchant_model');
	return $CI->merchant_model->userinfo($userid);
}	
function merchantinfo($id){
	$CI = &get_instance();
	$CI->load->model('merchant_model');
	return $CI->merchant_model->merchantinfo($id);
}
function set_time_zone($timezone){
		return date_default_timezone_set($timezone);
}
function userRegistrationCount(){
	$CI = &get_instance();
	//$CI->load->model('admin_model');
	//return $CI->merchant_model->userinfo($userid);
	$CI->db->select('count(*) as totaluser');
	$CI->db->from(FRONT_USER);
	$CI->db->where('status','1');
	$CI->db->where('userType','1');
	$query = $CI->db->get();
	//echo $this->db->last_query();
	return $query->result();
}
function activeDealCount(){
	$CI = &get_instance();
	//echo $this->db->last_query();
	$current_time = date('Y-m-d H:i:s');
	//$query = $CI->db->query("SELECT count(*) as totalactivedeal FROM (".UPLOAD_DEALS." as ud) INNER JOIN ".DEALTYPE." as d ON d.id=ud.type_of_meal WHERE ADDTIME(ud.dealdate,'04:00:00') > '$current_time' ORDER BY ud.id desc");
	$query = $CI->db->query("SELECT count(*) as totalactivedeal FROM ".UPLOAD_DEALS." as ud WHERE (ADDTIME(ud.dealdate,'04:00:00') > '$current_time') ORDER BY ud.id desc");
	//echo $CI->db->last_query(); die;
	//echo $CI->db->count_all_results(); die;
	return $query->result();
	
}
function deactiveDealCount(){
}
function claimedDealCount(){
}	
function idealPaymentStaus($dataArr){
	$CI = &get_instance();
	//$CI->load->model('merchant_model');
	$CI->db->insert('restoranto_payment_status',$dataArr);
	return $CI->db->insert_id();
}


?>

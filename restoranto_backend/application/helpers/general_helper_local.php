<?php
/* Function to check error on form **/
function check_error($name){
	if(form_error($name)) { 
		$error_class = 'has-error';
	} else {
		$error_class = '';
	}
	return $error_class;
}	

function emailSendtoUser($email,$subject,$message){
   $headers  = 'MIME-Version: 1.0' . PHP_EOL;
   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

  // More headers
   $headers .= "From:shobha@exceptionaire.co \r\n";
   $retval 	 = mail($email,$subject,$message,$headers);
   return $retval;
}	

function userinfo($userid){
	$CI = &get_instance();
	$CI->load->model('merchant_model');
	return $CI->merchant_model->userinfo($userid);
}	
?>

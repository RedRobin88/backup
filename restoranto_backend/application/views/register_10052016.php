<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant Registration
            <small>Form</small>
            <small class="gn"><?php echo $this->session->flashdata('registermsg');?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Merchant</a></li>
            <li class="active">New</li>
          </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning fl">
				<form action="<?php echo site_url('register')?>" method="post">
				
				<div class="col-md-6">
					<div class="box-header with-border">
					  <h3 class="box-title">Company </h3>
					</div><!-- /.box-header -->
					
					<div class="box-body">
									<!-- text input -->
									
									<div class="form-group <?php echo check_error('company_name');?>">
									  <label>Name</label>
									  <input type="text" class="form-control" id="inputError" placeholder="Enter in English ..." name="company_name" value="<?php echo $this->input->post('company_name');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_name'); ?></label>
									  <input type="text" class="form-control" id="inputError" placeholder="Enter in Dutch ..." name="company_name_dutch" value="<?php echo $this->input->post('company_name_dutch');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_name_dutch'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_loc');?>">
									  <label>Location (Put in Street + number, Zipcode + city, Country format)</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_loc" value="<?php echo $this->input->post('company_loc');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_loc'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_food_type');?>">
									  <label>Food Type</label>
									  <select class="form-control" name="company_food_type" onchange="select_other_deal(this)">
										<?php if(count($company_food_type)>0){
											foreach($company_food_type as $food) { ?>
											<option value="<?php echo $food->id;?>"><?php echo $food->name;?></option>	
										<?php	}	
										}
										?>
									  </select>
									  <label class="control-label" for="inputError"><?php echo form_error('company_food_type'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_phoneno');?>">
									  <label>Phone Number</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_phoneno" value="<?php echo $this->input->post('company_phoneno');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_phoneno'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_website');?>">
									  <label>Website</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_website" value="<?php echo $this->input->post('company_website');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_website'); ?></label>
									</div>
								</div><!-- /.box-body -->
								
				</div>
				
				<div class="col-md-6">
								<div class="box-body">
									<!-- text input -->
									<div class="box-header with-border">
									  <h3 class="box-title">Owner </h3>
									</div><!-- /.box-header -->
									<div class="form-group <?php echo check_error('owner_name');?>">
									  <label>Name</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="owner_name" value="<?php echo $this->input->post('owner_name');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('owner_name'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('owner_phoneno');?>">
									  <label>Phone Number</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="owner_phoneno" value="<?php echo $this->input->post('owner_phoneno');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('owner_phoneno'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('email');?>">
									  <label>Email</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="email" value="<?php echo $this->input->post('email');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('email'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('password');?>">
									  <label>Password</label>
									  <input type="password" class="form-control" placeholder="Enter ..." name="password">
									  <label class="control-label" for="inputError"><?php echo form_error('password'); ?></label>
									</div>
								</div><!-- /.box-body -->
				</div>
			
				<div class="col-md-12">
					<div class="box-body">
						<div class="box-footer submit_register">
							<button class="btn btn-primary" type="submit" name="register" value="register">Submit</button>
						</div>
					</div>
				</div>	
				</form>
				
			</div><!-- End of Box -->
		</div>	<!-- End ofcol-md-12 -->		 
		
		
		
	</div><!-- End of row -->
</section>	        

	
</div>
<?php
if(!$this->session->userdata('islogin')){
?>
<style>
	.content-wrapper, .right-side, .main-footer { margin: 0 auto !important;  width: 90%;}
</style>
<?php		
}
?>

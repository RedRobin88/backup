<?php 
	$file_base_path = site_url().base_url_file;
	$editid = (isset($dealRes[0]->id) && $dealRes[0]->id !='')? '/'.$dealRes[0]->id:'';
	$merchantid = $this->session->userdata('userId');
	$merchantinfo = merchantinfo($merchantid);
?>


<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>assets/css/jquery.datetimepicker.css"/>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Buy Credits
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Buy Credits</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	<div class="col-md-12">
		<div class="box box-warning">
			<div class="box-header with-border">
			  <h3 class="box-title"> Buy Credits </h3>
			</div><!-- /.box-header -->
			<div class="box-body" style="height:100%;">
				
			<?php
			use iDEALConnector\iDEALConnector;
			use iDEALConnector\Configuration\DefaultConfiguration;
			use iDEALConnector\Exceptions\SerializationException;

			use iDEALConnector\Exceptions\SecurityException;
			use iDEALConnector\Exceptions\ValidationException;
			use iDEALConnector\Exceptions\iDEALException;

			use iDEALConnector\Entities\Transaction;
			use iDEALConnector\Entities\AcquirerTransactionResponse;
			
			use iDEALConnector\Entities\DirectoryResponse;

			date_default_timezone_set('UTC');

			require_once(APPPATH."third_party/ideal/iDEALConnector.php");

			$config = new DefaultConfiguration(APPPATH."third_party/ideal/config.conf");
			$actionType = "";

			$errorCode = 0;
			$errorMsg = "";
			$consumerMessage = "";

			//$issuerId = "INGBNL2A";
			$purchaseId = "1234567890123456";
			$amount = 11.0;
			$description = "Restoranto Credits Payment";
			$entranceCode = "1234567890123456789012345678901234567890";
			$merchantReturnUrl = $config->getMerchantReturnURL();
			$expirationPeriod = $config->getExpirationPeriod();

			$acquirerID = "";
			$issuerAuthenticationURL = "";
			$transactionID = "";
			
			if (isset($_POST["submitted"]))
				$actionType = $_POST["submitted"];

			if (isset($_POST["issuerId"]))
				$issuerId = $_POST["issuerId"];

			if (isset($_POST["purchaseId"]))
				$purchaseId = $_POST["purchaseId"];

			if (isset($_POST["amount"]))
				$amount = floatVal($_POST["amount"]);

			if (isset($_POST["description"]))
				$description = $_POST["description"];

			if (isset($_POST["entranceCode"]))
				$entranceCode = $_POST["entranceCode"];

			if (isset($_POST["merchantReturnURL"]))
				$merchantReturnUrl = $_POST["merchantReturnURL"];

			if (isset($_POST["expirationPeriod"]))
				$expirationPeriod = intVal($_POST["expirationPeriod"]);

			$iDEALConnector = iDEALConnector::getDefaultInstance(APPPATH."third_party/ideal/config.conf");
			if ($actionType == "Buy Credits") {
				
				
				try
				{
					$response = $iDEALConnector->startTransaction(
						$issuerId,
						new Transaction(
							$amount,
							$description,
							$entranceCode,
							$expirationPeriod,
							$purchaseId,
							'EUR',
							'nl'
						),
						$merchantReturnUrl
					);

					/* @var $response AcquirerTransactionResponse */
					$acquirerID = $response->getAcquirerID();
					$issuerAuthenticationURL = $response->getIssuerAuthenticationURL();
					$transactionID = $response->getTransactionID();
					?>
					<script>
					window.location.replace("<?php echo $issuerAuthenticationURL ?>");
					</script>
					<?php
				}
				catch (SerializationException $ex)
				{
					echo '<b style="color:red">Serialization:'.$ex->getMessage().'</b>';
				}
				catch (SecurityException $ex)
				{
					echo '<b style="color:red">Security:'.$ex->getMessage().'</b>';
				}
				catch(ValidationException $ex)
				{
					echo '<b style="color:red">Validation:'.$ex->getMessage().'</b>';
				}
				catch (iDEALException $ex)
				{
					$errorCode = $ex->getErrorCode();
					$errorMsg = $ex->getMessage();
					$consumerMessage = $ex->getConsumerMessage();

				}
				catch (Exception $ex)
				{
					echo '<b style="color:red">Exception:'.$ex->getMessage().'</b>';
				}
			}
			?>	
			  <form method="post" id="payment-form">
				<div class="col-md-6">
					<div class="form-group">
					  <label>How many credits would you like? One credit equals €1,-.<span class="required_star">*</span></label>
					  <input type="text" class="form-control" size="60" name="amount" id="amount" value="<?php //echo $amount; ?>">
					  <label class="control-label" id="amount-error" for="inputError" style="color:red;"></label>
					  <input type="hidden" size="60" name="purchaseId" value="<?php echo @$purchaseId; ?>">	
						<input type="hidden" size="60" name="entranceCode" value="<?php echo @$entranceCode; ?>">
						<input type="hidden" size="60" name="merchantReturnURL" value="<?php echo @$merchantReturnUrl ?>">
						<input type="hidden" size="60" name="expirationPeriod" value="<?php echo @$expirationPeriod; ?>">
					  <label class="control-label" for="inputError"><?php //echo form_error('amount'); ?></label>
					</div>
					<div class="form-group">
						  <label>Select Bank<span class="required_star">*</span></label>
							<?php
									$response_issuer = $iDEALConnector->getIssuers();
									$issuerList = "";
									$acquirerID = "";
									$responseDatetime = null;
									$each_entry = $response_issuer->getCountries();
									//echo "<pre>"; print_r($each_entry);
									/* @var $response DirectoryResponse*/
									foreach ($response_issuer->getCountries() as $country)
									{
										$issuerList .= "<optgroup label=\"" . $country->getCountryNames() . "\">";

										foreach ($country->getIssuers() as $issuer) {
											$issuerList .= "<option value=\"" . $issuer->getId() . "\">"
												. $issuer->getName() . "</option>";
										}
										$issuerList .= "</optgroup>";

										$acquirerID = $response_issuer->getAcquirerID();
										$responseDatetime = $response_issuer->getDirectoryDate();
									}
							?>
							<select class="form-control" id="IssuerIDs" name="issuerId"><?php echo $issuerList; ?></select>
						  <label class="control-label" for="inputError"><?php echo form_error('deal_type'); ?></label>
					</div>
					<div class="form-group">
						<input type="submit" name="submitted" value="Buy Credits" class="btn btn-primary">
					</div>		
				</div>	
			  </form>	  			 
			  <!-- 
			  <table class="box" width="100%">
				<tbody>
				<tr>
					<td colspan="2"><i style="text-decoration: underline;">Result:</i></td>
				</tr>
				<?php  if ($errorCode != "") { ?>
				<tr>
					<td width="200">Error Code</td>
					<td><?php echo $errorCode; ?></td>
				</tr>
				<tr>
					<td>Error Message</td>
					<td><?php echo $errorMsg; ?></td>
				</tr>
				<tr>
					<td>Consumer Message</td>
					<td><?php echo $consumerMessage; ?></td>
				</tr>
					<?php } else { ?>
				<tr>
					<td width="200">Acquirer ID:</td>
					<td><?php echo $acquirerID; ?>
					</td>
				</tr>
				<tr>
					<td width="200">Transaction ID:</td>
					<td><?php echo $transactionID; ?>
					</td>
				</tr>
				<tr>
					<td width="200">Issuer Authentication URL:</td>
					<td><?php echo $issuerAuthenticationURL; ?>
					</td>
				</tr>
				<tr>
					<td width="200">Transaction status:</td>
					<td><?php echo @$status; ?>
					</td>
				</tr>
				
					<?php } ?>
				</tbody>
			</table>
			<br>
			

			<table class="box" width="100%">
				<tbody>
				<tr>
					<td style="margin:0;padding:0">
						<form method="post">
							<div style="text-align: center;">
							<input id="issuerAuthentication" type="button" name="submitted" value="Issuer Authentication"
								   <?php if ($issuerAuthenticationURL == "") { ?>disabled="disabled"<?php } ?>>
							</div>
						</form>
					</td>
				</tr>
				</tbody>
			</table>
			-->
			</div><!-- /.box-body -->
		</div><!-- /.box -->
				
		</div>
	</div>
</section>	        
<script>
	$("#payment-form").submit(function(){
		var amount = $("#amount").val();
		if(amount == ''){
			$("#amount-error").html('Please Enter Amount');
			return false;
		}
		
		if(isNaN(amount)){ 
			$("#amount-error").html('Please Enter Numbers only');
			return false;
		}
	});
</script>

<!--
<script>
    window.onload = function(){
        document.getElementById("issuerAuthentication").onclick = function(){
            window.location.replace("<?php echo $issuerAuthenticationURL ?>");
        }
    }

</script>
-->
</div>


<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Change Password
            <small>Form</small>
            <small class="gm"><?php echo $this->session->flashdata('passwordmsg');?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Change Password</a></li>
          </ol>
</section>

<div class="box-body" style="height:100%;">
		  <div class="login-box">
			  
			  <div class="login-box-body">
				
				<form action="" method="post">
				  <div class="form-group has-feedback">
					<input type="password" name="pass" class="form-control" placeholder="Old Password" value="<?php echo $this->input->post('pass');?>">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					<span class="error"><?php echo form_error('pass'); ?></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="password" name="newpass" class="form-control" placeholder="New Password" value="<?php echo $this->input->post('newpass');?>">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					<span class="error"><?php echo form_error('newpass'); ?></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="password" name="confirmpass" class="form-control" placeholder="Confirm Password" value="<?php echo $this->input->post('confirmpass');?>">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					<span class="error"><?php echo form_error('confirmpass'); ?></span>
				  </div>
				  <div class="row">
					
					<div class="col-xs-4 pull-right">
					  <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
					</div><!-- /.col -->
				  </div>
				</form>

				

			  </div><!-- /.login-box-body -->
			</div><!-- /.login-box -->
		</div><!-- /.box-body -->
	</div><!-- /.box -->
	
</div>

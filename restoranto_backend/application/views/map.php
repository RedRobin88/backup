<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple markers</title>
    <style>
      html, body {
        height: 100%;
        width: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 50%;
        width: 50%;
        margin: 0 auto;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    Lat : <input type="text" id="lat" name="lat" value="-25.363"> Lang : <input type="text" id="lng" name="lang" value="131.044">
    <script>

      function initMap() {
        var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!',
          draggable: true
        });
        
         marker.addListener('drag',function(event) {
			document.getElementById('lat').value = event.latLng.lat();
			document.getElementById('lng').value = event.latLng.lng();
		});
      }
    </script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEoeSxzxaJd0kxFCoNoaxWYoXnUa9J1kY&callback=initMap"async defer></script>
  </body>
</html>

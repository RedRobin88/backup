<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/admin/dataTables.bootstrap.css">
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>
		 Uploaded Deals
	  </h1>
	  <span class="user-message"><?php echo $this->session->flashdata('uploaddealmsg');?></span>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#"> Uploaded Deals</a></li>
	  </ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
	  
	  <div class="row">
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title"> Uploaded Deals</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
			  <div class="table-responsive">
			  <table id="example1" class="table table-bordered table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Restaurant</th>
					<th>Deal</th>
					<th>Option to Display Price</th>
					<th>Image</th>
					<th>Number of people which can be claimed</th>
					<th>Status</th>
					<th>Deal Datetime</th>
					<th>Action</th>
				 </tr>
				</thead>
				<tbody>
				  <?php if(isset($result) && count($result)>0):?>
						<?php $i=1; foreach($result as $key=>$userdata):?>
							<tr class="odd gradeX">
								<td><?php echo $i;?></td>
								<td><?php echo $userdata->restaurant;?></td>
								<td><?php if($userdata->type_of_meal == 14) { echo @$userdata->other_type_of_meal;} else { echo $userdata->name;}?></td>
								<td><?php 
									if($userdata->price_show_option ==1){
											echo "Old and New Price <br>";
											echo 'Old:'.$userdata->old_price.' New:'.$userdata->new_price;
									}else if($userdata->price_show_option ==2){
											echo "Discount <br>";
											echo $userdata->discount;
									}
									?></td>
								<td><?php if($userdata->image) { $image = $userdata->image;} else { $image = site_url().'assets/images/deals/no-deal-image.jpg';}?>
								<img src="<?php echo $image?>" width="100px"></td>
								<td class="center"><?php echo $userdata->n_of_d_claimed;?></td>
								<td class="center"><?php echo ($userdata->status== 1)?'Active':'Deactive';?></td>
								<td class="center"><?php echo $userdata->dealdate;?></td>
								<td class="center">
									<!--
									<select name="userstatus" id="userstatus" onchange="change_user_status('<?php echo $userdata->id;?>',this)">
										<option value="1" <?php echo ($userdata->status== 1)?'selected=selected':'';?>>Active</option>
										<option value="0" <?php echo ($userdata->status== 0)?'selected=selected':'';?>>Deactive</option>
									</select>
									&nbsp;&nbsp;&nbsp;
									-->
									<a href="<?php echo site_url('merchant-upload-deal/'.$userdata->id);?>"  class="btn btn-primary btn-xs "><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									&nbsp;&nbsp;&nbsp;
									<a href="javascript::void(0)" onclick="delete_user('<?php echo $userdata->id;?>')" class="btn btn-primary btn-xs "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									
								</td>
							</tr>
						<?php $i++; endforeach;?>
					<?php endif;?>
				</tbody>
				<tfoot>
				  <tr>
					<th>#</th>
					<th>Restaurant</th>
					<th>Deal</th>
					<th>Option to Display Price</th>
					<th>Image</th>
					<th>Number of people which can be claimed</th>
					<th>Status</th>
					<th>Deal Datetime</th>
					<th>Action</th>
				  </tr>
				</tfoot>
			  </table>
			  </div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- /.col -->
	  </div><!-- /.row -->
	</section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<script src="<?php echo site_url()?>assets/js/admin/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url()?>assets/js/admin/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url()?>assets/js/manage_deals.js"></script>
<script>
$(document).ready(function() {
	$("#example1").DataTable();
});
</script>

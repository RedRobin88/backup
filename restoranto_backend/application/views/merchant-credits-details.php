<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
           Merchant Payment Details
            <small>All</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Merchant Payment Details</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Merchant Payment Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive"> 	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>#</th>
						<th>Merchant ID</th>
						<th>Merchant Name</th>
						<th>Transaction ID</th>
						<th>Amount</th>
						<!--<th>Status DateTime</th>-->
						<th>Status</th>
						<th>Payment Date Time</th>
					  </tr>
                    </thead>
                    <tbody>
					 <?php if(isset($result) && count($result)>0):?>
						<?php $i=1; foreach($result as $key=>$userdata):?>
						<tr class="odd gradeX">
								<td><?php echo $i;?></td>
								<td><?php echo $userdata->merhcant_id;?></td>
								<td><?php echo $userdata->owner_name;?></td>
								<td><?php echo $userdata->transaction_id;?></td>							
								<td><?php echo $userdata->amount;?></td>
								<!--<td><?php echo $userdata->statusDateTime;?></td>-->
								<td><?php echo $userdata->status;?></td>
								<td><?php echo $userdata->creation_time;?></td>
							</tr>
						<?php $i++; endforeach;?>
                     <?php endif;?>
                    </tbody>
                    <tfoot>
                      <tr>
                       <th>#</th>
						<th>Merchant ID</th>
						<th>Merchant Name</th>
						<th>Transaction ID</th>
						<th>Amount</th>
						<!--<th>Status DateTime</th>-->
						<th>Status</th>
						<th>Payment Date Time</th>
					  </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>    



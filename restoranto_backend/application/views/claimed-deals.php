<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/admin/dataTables.bootstrap.css">
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>
		 Claimed Deals
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#"> Claimed Deals</a></li>
	  </ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
	  
	  <div class="row">
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title"> Claimed Deals</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
			  <div class="table-responsive">	
			  <table id="example1" class="table table-bordered table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Restaurant</th>
					<th>Deal</th>
					<th>Option to Display Price</th>
					<th>Image</th>
					<th>Number of people which can be claimed</th>
					<th>Claimed by User</th>
					<th>User Email</th>
					<th>User Phone no.</th>
					<th>Claimed Time</th>
					<!--<th>Action</th>-->
				 </tr>
				</thead>
				<tbody>
				  <?php if(isset($result) && count($result)>0):?>
						<?php $i=1; foreach($result as $key=>$userdata):?>
							<tr class="odd gradeX">
								<td><?php echo $i;?></td>
								<td><?php echo $userdata->restaurant;?></td>
								<td><?php if($userdata->type_of_meal == 14) { echo @$userdata->other_type_of_meal;} else { echo $userdata->name;}?></td>
								<td><?php 
									if($userdata->price_show_option ==1){
											echo "Old and New Price <br>";
											echo 'Old:'.$userdata->old_price.' New:'.$userdata->new_price;
									}else if($userdata->price_show_option ==2){
											echo "Discount <br>";
											echo $userdata->discount;
									}
									?></td>
								<td><img src="<?php echo $userdata->image;?>" width="100px"></td>
								<td class="center"><?php echo $userdata->n_of_d_claimed;?></td>
								<td class="center"><?php $userinfo = userinfo($userdata->userid); echo (isset($userinfo[0]->name))?$userinfo[0]->name:'';?></td>
								<td class="center"><?php echo (isset($userinfo[0]->email))?$userinfo[0]->email:'-';?></td>
								<td class="center"><?php echo (isset($userinfo[0]->phoneno))?$userinfo[0]->phoneno:'-';?></td>
								<td class="center"><?php echo $userdata->claimed_datetime;?></td>
							</tr>
						<?php $i++; endforeach;?>
					<?php endif;?>
				</tbody>
				<tfoot>
				  <tr>
					<th>#</th>
					<th>Restaurant</th>
					<th>Deal</th>
					<th>Option to Display Price</th>
					<th>Image</th>
					<th>Number of people which can be claimed</th>
					<th>Claimed by User</th>
					<th>User Email</th>
					<th>User Phone no.</th>
					<th>Claimed Time</th>
					<!--<th>Action</th>-->
				  </tr>
				</tfoot>
			  </table>
			  </div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- /.col -->
	  </div><!-- /.row -->
	</section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<script src="<?php echo site_url()?>assets/js/admin/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url()?>assets/js/admin/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url()?>assets/js/manage_deals.js"></script>
<script>
$(document).ready(function() {
	$("#example1").DataTable();
});
</script>

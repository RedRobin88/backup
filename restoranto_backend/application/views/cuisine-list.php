<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Cuisine List
            <small>All</small>
            <small class="gn"><?php echo $this->session->flashdata('cuisinemsg');?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cuisine</a></li>
            <li class="active">List</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Cuisine List with All Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive">	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
						<th>Sr. No.</th>  
                        <th>Name in English</th>
                        <th>Name in Dutch</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php if(count($cuisinelist)>0){
							$j = 1;
							foreach($cuisinelist as $cuisine){
								?>
								<tr>
									<td><?php echo $j;?></td>
									<td><?php echo $cuisine->name?></td>
									<td><?php echo $cuisine->name_dt?></td>
									<td><?php if($cuisine->status == 1) { echo "Active"; } else { echo "Not Active"; } ?></td>
									<td>
										<select onchange="change_cusine_status('<?php echo $cuisine->id;?>',this,'front-end-user')" id="cusinestatus" name="cusinestatus">
											<option <?php echo ($cuisine->status==1)?'selected=selected':'';?> value="1">Active</option>
											<option <?php echo ($cuisine->status==0)?'selected=selected':'';?> value="0">Deactive</option>
										</select>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="<?php echo site_url('cuisine/edit/'.$cuisine->id);?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
										<a href="<?php echo site_url('cuisine/delete/'.$cuisine->id);?>" class="btn btn-primary btn-xs " onclick="return confirm('Are you sure you want to delete ?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
								$j++;
							}	
						}   
					?> 	
                      
                    </tbody>
                    <tfoot>
                      <tr>
						<th>Sr. No.</th>   
                        <th>Name in English</th>
                        <th>Name in Dutch</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>  
<script>      
	function change_cusine_status(id,status){
		var status_s = status.value;
		
			$.ajax({
				url:site_url+'change-cuisine-status',
				data:{id:id,status:status_s},
				type:'post',
				success:function(data){
					window.location.reload();
					return false;
					
				}
			});
			
	}
</script>      



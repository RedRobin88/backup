<?php 
	$file_base_path = site_url().base_url_file;
	//echo '<pre>';
	//print_r($this->session->userdata('userType'));die;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
	<section class="content-header">
          <h1>
            Uploaded Deals
            <small>All</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Deal</a></li>
            <li class="active">List</li>
          </ol>
	</section>


	<section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Deal List with All Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>#</th>
						<th>Restaurant</th>
						<th>Deal</th>
						<th>Option to Display Price</th>
						<th>Image</th>
						<th>Number of People which can be claimed</th>
						<th>Status</th>
						<th>Deal Start Datetime</th>
						<th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					 <?php if(isset($result) && count($result)>0):?>
						<?php $i=1; foreach($result as $key=>$userdata):?>
						<tr class="odd gradeX">
								<td><?php echo $i;?></td>
								<td><?php echo $userdata->restaurant;?></td>
								<td><?php if($userdata->type_of_meal == 14) { echo @$userdata->other_type_of_meal;} else { echo $userdata->name;}?></td>
								<td><?php 
									if($userdata->price_show_option ==1){
											echo "Old and New Price <br>";
											echo 'Old:'.$userdata->old_price.' New:'.$userdata->new_price;
									}else if($userdata->price_show_option ==2){
											echo "Discount <br>";
											echo $userdata->discount;
									}
									?></td>
								<td><img src="<?php echo ($userdata->image!='')?$userdata->image:site_url().'assets/images/deals/no-deal-image.jpg';?>" width="100px"></td>
								<td class="center"><?php echo $userdata->n_of_d_claimed;?></td>
								<td class="center">
									<?php //echo ($userdata->status== 1)?'Active':'Deactive';
									//echo $userdata->timedif."<br />";
									$timedif = explode(":",$userdata->timedif);
									if($timedif[0] <= 4 && $timedif[0] >= 0) {  
										if($timedif[0] == 4 && $timedif[1] > 0) {
											echo "Deactive";
										} else {				
											echo "Active";								
										}	
									} else {
										echo "Deactive";
									}		
									?>
								</td>
								<td class="center"><?php echo $userdata->dealdate;?></td>
								<td class="center"><a href="<?php echo site_url();?>edit-details/<?php echo $userdata->id;?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
							</tr>
						<?php $i++; endforeach;?>
                     <?php endif;?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>#</th>
						<th>Restaurant</th>
						<th>Deal</th>
						<th>Option to Display Price</th>
						<th>Image</th>
						<th>Number of People which can be claimed</th>
						<th>Status</th>
						<th>Deal Start Datetime</th>
						<th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>    
<script>
	 $('#example').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
</script>
<script src="<?php echo site_url()?>assets/js/manage_user.js"></script>


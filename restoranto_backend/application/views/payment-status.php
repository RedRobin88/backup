<?php 
	$file_base_path = site_url().base_url_file;
	$editid = (isset($dealRes[0]->id) && $dealRes[0]->id !='')? '/'.$dealRes[0]->id:'';
	$merchantid = $this->session->userdata('userId');
	$merchantinfo = merchantinfo($merchantid);
?>


<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>assets/css/jquery.datetimepicker.css"/>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Payment Status
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Payment Status</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	<div class="col-md-12">
		<div class="box box-warning">
			<div class="box-header with-border">
			  <h3 class="box-title">  Payment Status </h3>
			</div><!-- /.box-header -->
			<div class="box-body" style="height:100%;">
				
			<?php
			use iDEALConnector\iDEALConnector;
			use iDEALConnector\Exceptions\SerializationException;
			use iDEALConnector\Configuration\DefaultConfiguration;

			use iDEALConnector\Exceptions\SecurityException;
			use iDEALConnector\Exceptions\ValidationException;

			use iDEALConnector\Exceptions\iDEALException;

			use iDEALConnector\Entities\AcquirerStatusResponse;
			date_default_timezone_set('UTC');

			require_once(APPPATH."third_party/ideal/iDEALConnector.php");

			$config = new DefaultConfiguration(APPPATH."third_party/ideal/config.conf");
			$actionType = "";

			$errorCode = 0;
			$errorMsg = "";
			$consumerMessage = "";
			$transactionID = "";

			$acquirerID = "";
			$consumerName = "";
			$consumerIBAN = "";
			$consumerBIC = "";
			$amount = "";
			$currency = "";
			$statusDateTime = null;
			$status = "";
					
			//print_r($_REQUEST);		
			$transactionID = $_GET['trxid'];
			if($transactionID) { 
			
				try
				{
					$iDEALConnector = iDEALConnector::getDefaultInstance(APPPATH."third_party/ideal/config.conf");			
					$response = $iDEALConnector->getTransactionStatus($transactionID);

					/* @var $response AcquirerStatusResponse */
					$acquirerID = $response->getAcquirerID();
					$consumerName = $response->getConsumerName();
					$consumerIBAN = $response->getConsumerIBAN();
					$consumerBIC = $response->getConsumerBIC();
					$amount = $response->getAmount();
					$currency = $response->getCurrency();
					$statusDateTime = $response->getStatusTimestamp();
					$transactionID = $response->getTransactionID();
					$status = $response->getStatus();
					
					$dataArr['merhcant_id'] = $merchantid;
					$dataArr['transaction_id'] = $transactionID;
					$dataArr['amount'] = $amount;
					$dataArr['acquirerID'] = $acquirerID;
					$dataArr['consumerName'] = $consumerName;
					$dataArr['consumerIBAN'] = $consumerIBAN;
					$dataArr['currency'] = $currency;
					$dataArr['creation_time'] = date('Y-m-d H:i:s');
					if(@trim(@$statusDateTime)) { 
						$dataArr['statusDateTime'] = $statusDateTime;
					} else {
						$dataArr['statusDateTime'] = '';
					}		
					$dataArr['status'] = $status;
					
					idealPaymentStaus($dataArr);
				}
				catch (SerializationException $ex)
				{
					echo '<b style="color:red">Serialization:'.$ex->getMessage().'</b>';
				}
				catch (SecurityException $ex)
				{
					echo '<b style="color:red">Security:'.$ex->getMessage().'</b>';
				}
				catch(ValidationException $ex)
				{
					echo '<b style="color:red">Validation:'.$ex->getMessage().'</b>';
				}
				catch (iDEALException $ex)
				{
					$errorCode = $ex->getErrorCode();
					$errorMsg = $ex->getMessage();
					$consumerMessage = $ex->getConsumerMessage();
				}
				catch (Exception $ex)
				{
					echo '<b style="color:red">Exception:'.$ex->getMessage().'</b>';
				}
			
			?>	
			 
			  
			  <table class="box" width="100%">
				<tbody>
				<tr>
					<td colspan="2"><i style="text-decoration: underline;">Result:</i></td>
				</tr>
				<?php  if ($errorCode != "") { ?>
				<tr>
					<td width="200">Error Code</td>
					<td><?php echo $errorCode; ?></td>
				</tr>
				<tr>
					<td>Error Message</td>
					<td><?php echo $errorMsg; ?></td>
				</tr>
				<tr>
					<td>Consumer Message</td>
					<td><?php echo $consumerMessage; ?></td>
				</tr>
					<?php } else { ?>
				<tr>
					<td width="200">Transaction ID:</td>
					<td><?php echo $transactionID; ?>
					</td>
				</tr>
				
				<tr>
					<td width="200">Status Amount:</td>
					<td><?php echo $amount; ?>
					</td>
				</tr>
				<tr>
					<td width="200">Transaction status:</td>
					<td><?php echo @$status; ?>
					</td>
				</tr>
				
					<?php } ?>
				</tbody>
			</table>
			<br>
			<?php } ?>
			

			</div><!-- /.box-body -->
		</div><!-- /.box -->
				
		</div>
	</div>
</section>	        
</div>


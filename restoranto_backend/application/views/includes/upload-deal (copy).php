<?php 
	$file_base_path = site_url().base_url_file;
	
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>assets/css/jquery.datetimepicker.css"/>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant Upload Deal
            <small>Form</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Deal Management</a></li>
            <li class="active">Upload Deal</li>
          </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-6">
							<div class="box box-warning">
								<div class="box-header with-border">
								  <h3 class="box-title">Deal </h3>
								</div><!-- /.box-header -->
								<div class="box-body">
								  <form action="<?php echo site_url('merchant/merchant/uploadDeal')?>" method="post" enctype="multipart/form-data">
									<!-- text input -->
									
									<div class="form-group <?php echo check_error('deal_type');?>">
									  <label>Type of Deal</label>
									  <select class="form-control" name="deal_type" onchange="select_other_deal(this)">
										<?php if(count($dealtype)>0){
											foreach($dealtype as $dealtype) { ?>
											<option value="<?php echo $dealtype->id;?>" <?php echo (isset($dealRes[0]->id) && $dealRes[0]->id == $dealtype->id)?'selected=selected':''?>><?php echo $dealtype->type;?></option>	
										<?php	}	
										}
										?>
									  </select>
									  <label class="control-label" for="inputError"><?php echo form_error('deal_type'); ?></label>
									</div>
									
									<div id="other_deal_type_outer" style="display:none" class="form-group <?php echo check_error('other_deal_type');?>">
									  <label>Other Deal Type</label>
									  <input   type="text" class="form-control" placeholder="Enter ..." name="other_deal_type" value="<?php echo $this->input->post('other_deal_type');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('other_deal_type'); ?></label>
									</div>
									
									<div class="form-group">
									  <label>Discount</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="discount" value="<?php echo (isset($dealRes[0]->discount) && $dealRes[0]->discount!='')?$dealRes[0]->discount:$this->input->post('discount');?>">
									  <label class="control-label <?php echo check_error('discount');?>" for="inputError"><?php echo form_error('discount'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('deal_image');?>">
									  <label>Image</label>
									  <input type="file" name="deal_image">
									  <label class="control-label" for="inputError"><?php echo form_error('deal_image'); ?></label>
									</div>
									
									<div class="form-group <?php echo check_error('noof_deals_claimed');?>">
									  <label>Number of deals which can be claimed</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="noof_deals_claimed" value="<?php echo (isset($dealRes[0]->n_of_d_claimed) && $dealRes[0]->n_of_d_claimed!='')?$dealRes[0]->n_of_d_claimed:$this->input->post('noof_deals_claimed');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('noof_deals_claimed'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('noof_people_claimed_deal');?>">
									  <label>Number of people for a deal which can be claimed</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="noof_people_claimed_deal" value="<?php echo (isset($dealRes[0]->n_of_people_claimed) && $dealRes[0]->n_of_people_claimed!='')?$dealRes[0]->n_of_people_claimed:$this->input->post('noof_people_claimed_deal');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('noof_people_claimed_deal'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('deal_date');?>">
									  <label>Select deal date</label>
									  <input type="text" id="deal_date" class="form-control" placeholder="Enter ..." name="deal_date" value="<?php echo (isset($dealRes[0]->dealdate) && $dealRes[0]->dealdate!='')?$dealRes[0]->dealdate:$this->input->post('deal_date');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('deal_date'); ?></label>
									</div>
									
									<div class="box-footer">
										<button class="btn btn-primary" type="submit" name="submit" value="Submit">Submit</button>
									 </div>
								  </form>
								  
							</section>	        

								
							</div>

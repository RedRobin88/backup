<?php 
	$file_base_path = site_url().base_url_file;
	$editid = (isset($dealRes[0]->id) && $dealRes[0]->id !='')? '/'.$dealRes[0]->id:'';
	
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>assets/css/jquery.datetimepicker.css"/>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant Upload Deal
            <small>Form</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Deal Management</a></li>
            <li class="active">Upload Deal</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	<div class="col-md-12">
		<div class="box box-warning">
			<div class="box-header with-border">
			  <h3 class="box-title">Deal </h3>
			</div><!-- /.box-header -->
			<div class="box-body" style="height:100%;">
			  <form action="<?php echo site_url('merchant/merchant/uploadDeal'.$editid)?>" method="post" enctype="multipart/form-data">
				<!-- text input -->
				<div class="col-md-6">
					<div class="form-group <?php echo check_error('restaurant');?> <?php echo check_error('restaurant_dt');?>">
					  <label>Restaurant</label>
					  <input  type="text" class="form-control" placeholder="Enter in English..." name="restaurant" value="<?php echo (isset($dealRes[0]->restaurant) && $dealRes[0]->restaurant !='')?$dealRes[0]->restaurant:$this->input->post('restaurant');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('restaurant'); ?></label>
					  <input  type="text" class="form-control" placeholder="Enter in Dutch..." name="restaurant_dt" value="<?php echo (isset($dealRes[0]->restaurant_dt) && $dealRes[0]->restaurant_dt !='')?$dealRes[0]->restaurant_dt:$this->input->post('restaurant_dt');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('restaurant_dt'); ?></label>
					</div>
					
					
					<div class="form-group <?php echo check_error('deal_image');?>">
					  <label>Image</label>
					  <input type="file" name="deal_image">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_image'); ?></label>
					  <label class="control-label" for="inputError"><?php echo $this->session->flashdata('uploadimgmsg'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('price_show_option');?> <?php echo check_error('price_show_option');?>">
					  <label>Select option to display</label>
					   <div class="radio">
                        <label>
                          <input type="radio" value="1<?php //echo (isset($dealRes[0]->price_show_option) && $dealRes[0]->price_show_option !='')?$dealRes[0]->price_show_option:(isset($_POST['price_show_option']) ? $_POST['price_show_option'] : 1);?>" id="optionsRadios1" name="price_show_option" <?php if(isset($dealRes[0]->price_show_option) == 1) { echo "checked=checked"; }?>>
                          Old and new
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" value="2<?php //echo (isset($dealRes[0]->price_show_option) && $dealRes[0]->price_show_option !='')?$dealRes[0]->price_show_option:(isset($_POST['price_show_option']) ? $_POST['price_show_option'] : 2);?>" id="optionsRadios2" name="price_show_option" <?php if(isset($dealRes[0]->price_show_option) == 2) { echo "checked=checked"; } ?>>
                          Discount in %
                        </label>
                      </div>
					  <label class="control-label" for="inputError"><?php echo form_error('price_show_option'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('old_price');?> <?php echo check_error('new_price');?>">
					 <label>Set Old and New Price in Euro</label>
					 <div class="row">
						<div class="col-xs-4">
						  <input type="text" name="old_price" placeholder="Old Price" class="form-control" value="<?php echo (isset($dealRes[0]->old_price) && $dealRes[0]->old_price !='')?$dealRes[0]->old_price:$this->input->post('old_price');?>">
						</div>
						<div class="col-xs-4">
						  <input type="text" name="new_price" placeholder="New Price" class="form-control" value="<?php echo (isset($dealRes[0]->new_price) && $dealRes[0]->new_price !='')?$dealRes[0]->new_price:$this->input->post('new_price');?>">
						</div>
					</div>	
						<label class="control-label" for="inputError"><?php echo form_error('new_price'); ?></label>
                  </div>
                  
					<div class="form-group <?php echo check_error('discount');?> <?php echo check_error('discount_dt');?>">
					  <label>Discount</label>
					  <input type="text" class="form-control" placeholder="Enter in English..." name="discount" value="<?php echo (isset($dealRes[0]->discount) && $dealRes[0]->discount !='')?$dealRes[0]->discount:$this->input->post('discount');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('discount'); ?></label>
					  <input type="text" class="form-control" placeholder="Enter in Dutch..." name="discount_dt" value="<?php echo (isset($dealRes[0]->discount_dt) && $dealRes[0]->discount_dt !='')?$dealRes[0]->discount_dt:$this->input->post('discount_dt');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('discount_dt'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('deal_date');?>">
					  <label>Select deal date</label>
					  <input type="text" id="deal_date" class="form-control" placeholder="Enter ..." name="deal_date" value="<?php echo (isset($dealRes[0]->dealdate) && $dealRes[0]->dealdate !='')?$dealRes[0]->dealdate:$this->input->post('deal_date');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_date'); ?></label>
					</div>
					
				</div>
				
				<div class="col-md-6">
					<div class="form-group <?php echo check_error('noof_deals_claimed');?>">
					  <label>Number of deals which can be claimed</label>
					  <input type="text" class="form-control" placeholder="Enter ..." name="noof_deals_claimed" value="<?php echo (isset($dealRes[0]->n_of_d_claimed) && $dealRes[0]->n_of_d_claimed !='')?$dealRes[0]->n_of_d_claimed:$this->input->post('noof_deals_claimed');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('noof_deals_claimed'); ?></label>
					</div>
					<div class="form-group <?php echo check_error('noof_people_claimed_deal');?>">
					  <label>Number of people for a deal which can be claimed</label>
					  <input type="text" class="form-control" placeholder="Enter ..." name="noof_people_claimed_deal" value="<?php echo (isset($dealRes[0]->n_of_people_claimed) && $dealRes[0]->n_of_people_claimed !='')?$dealRes[0]->n_of_people_claimed:$this->input->post('noof_people_claimed_deal');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('noof_people_claimed_deal'); ?></label>
					</div>
					<div class="form-group <?php echo check_error('deal_type');?>">
					  <label>Type of Meal</label>
					  <select class="form-control" name="deal_type" onchange="select_other_deal(this)">
						<?php if(count($dealtype)>0){
							foreach($dealtype as $dealtype) { ?>
							<option value="<?php echo $dealtype->id;?>" <?php echo (isset($dealRes[0]->type_of_meal) && $dealRes[0]->type_of_meal == $dealtype->id)?"selected=selected":'';?>><?php echo $dealtype->name;?></option>	
						<?php	}	
						}
						?>
					  </select>
					  <label class="control-label" for="inputError"><?php echo form_error('deal_type'); ?></label>
					</div>
					<?php
						if(isset($dealRes[0]->type_of_meal) && $dealRes[0]->type_of_meal == 14){
								$display = "display:block";
						}else{
							$display = "display:none";
						}
					?>
					<div id="other_deal_type_outer" style="<?php echo $display;?>" class="form-group <?php echo check_error('other_deal_type');?>">
					  <label>Other Meal Type</label>
					  <input   type="text" class="form-control" placeholder="Enter ..." name="other_deal_type" value="<?php echo (isset($dealRes[0]->other_type_of_deal) && $dealRes[0]->other_type_of_deal !='')?$dealRes[0]->other_type_of_deal:$this->input->post('other_deal_type');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('other_deal_type'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('deal_title');?> <?php echo check_error('deal_title_dt');?>">
					  <label>Deal Title</label>
					  <input type="text" class="form-control" placeholder="Enter in English..." name="deal_title" value="<?php echo (isset($dealRes[0]->deal_title) && $dealRes[0]->deal_title !='')?$dealRes[0]->deal_title:$this->input->post('deal_title');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_title'); ?></label>
					  <input type="text" class="form-control" placeholder="Enter in Dutch..." name="deal_title_dt" value="<?php echo (isset($dealRes[0]->deal_title_dt) && $dealRes[0]->deal_title_dt !='')?$dealRes[0]->deal_title_dt:$this->input->post('deal_title_dt');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_title_dt'); ?></label>
					</div>
					
				</div>
				
				<div class="form-group <?php echo check_error('lat');?> col-md-6">
				  <label>Latitude</label>
				  <input type="text" id="lat" class="form-control" placeholder="Enter ..." name="lat" value="<?php echo (isset($dealRes[0]->lat) && $dealRes[0]->lat !='')?$dealRes[0]->lat:-25.363;?>">
				  <label class="control-label" for="inputError"><?php echo form_error('lat'); ?></label>
				</div>
				<div class="form-group <?php echo check_error('lng');?> col-md-6">
				  <label>Longitude</label>
				  <input type="text" id="lng" class="form-control" placeholder="Enter ..." name="lng" value="<?php echo (isset($dealRes[0]->lng) && $dealRes[0]->lng !='')?$dealRes[0]->lng:131.044;?>">
				  <label class="control-label" for="inputError"><?php echo form_error('lng'); ?></label>
				</div>									 
				<div class="clearfix"></div>
				 <script src="http://maps.googleapis.com/maps/api/js"></script>
				<script src="<?php echo site_url('assets/js/google.js');?>"></script>
				
				<div id="map" style="width:100%;height:380px;"></div>
				
				<?php if(isset($dealRes[0]->image)):?>
					<input type="hidden" name="hidden_img" id="hidden_img" value="<?php echo $dealRes[0]->image;?>">
				<?php endif;?>
				<div class="box-footer">
					<button class="btn btn-primary" type="submit" name="submit" value="Submit">Submit</button>
				 </div>
			  </form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
				
		</div>
	</div>
</section>	        

	
</div>

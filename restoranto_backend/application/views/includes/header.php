<?php 
	$file_base_path = site_url().base_url_file;
?>
<header class="main-header">
<!-- Logo -->
<a href="<?php echo site_url();?>" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b></b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>RESTORANTO</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <?php if($this->session->userdata('islogin')) { ?>
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	<span class="sr-only">Toggle navigation</span>
  </a>
  <?php } ?>
  <div class="navbar-custom-menu">
	<ul class="nav navbar-nav">
	  <!-- User Account: style can be found in dropdown.less -->
	  <?php if($this->session->userdata('islogin')) { ?>
	  <li class="dropdown user user-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		  <img src="<?php echo $file_base_path;?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
		  <span class="hidden-xs"><?php if($this->session->userdata('islogin')) { echo $this->session->userdata('company_name');}?></span>
		</a>
		<ul class="dropdown-menu">
		  <!-- User image -->
		  <li class="user-header">
			<img src="<?php echo $file_base_path;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
			<p>
			  <?php if($this->session->userdata('islogin')) { echo $this->session->userdata('company_name');}?>
			  <small>Restoranto</small>
			</p>
		  </li>
		  <!-- Menu Body -->
		  <!-- Menu Footer-->
		  <li class="user-footer">
			<div class="pull-left">
			  <a href="<?php echo site_url()?>user/change_password_form" class="btn btn-default btn-flat">Change Password</a>
			</div>
			
			<div class="pull-right">
			  <a href="<?php echo site_url()?>logout" class="btn btn-default btn-flat">Sign out</a>
			</div>
		  </li>
		</ul>
	  </li>
	  <?php } ?>
	  <!-- Control Sidebar Toggle Button -->
	  
	  
	  <!--
	  <li>
		<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
	  </li>
	  -->
	</ul>
  </div>
</nav>
</header>

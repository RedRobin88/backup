<?php 
	$file_base_path = site_url().base_url_file;
	$editid = (isset($dealRes[0]->id) && $dealRes[0]->id !='')? '/'.$dealRes[0]->id:'';
	$merchantid = $this->session->userdata('userId');
	$merchantinfo = merchantinfo($merchantid);
	if($this->session->userdata('userType') ==1){
			$formurl = site_url('edit-details'.$editid);
	}else{
			$formurl = site_url('merchant-upload-deal'.$editid);
	}
?>

<script>
	$( window ).load(function() { 
	<?php if(!$editid) { ?>	
			if (navigator.geolocation) {
			var test = navigator.geolocation.getCurrentPosition(showPosition);
			setMap(52.1326,5.2913);
			} else { 
				 
			}
	<?php } else { ?>	
		var lat = <?php echo $dealRes[0]->lat?>;
		var lng = <?php echo $dealRes[0]->lng?>;
		setMap(lat,lng);
	<?php } ?>
});	

function showPosition(position) {
    document.getElementById('lat').value = position.coords.latitude;
    document.getElementById('lng').value = position.coords.longitude;
    setMap(position.coords.latitude,position.coords.longitude);
}
function select_price_option(id){ 
		if(id.value == 1){
			$("#show_old_new_price").show();
			$("#show_discount_price").hide();
		}else if(id.value == 2){
			$("#show_discount_price").show();
			$("#show_old_new_price").hide();
		}
}
</script>

<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>assets/css/jquery.datetimepicker.css"/>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant Upload Deal
            <small>Form</small>
          </h1><span class="user-message"><?php echo $this->session->flashdata('uploaddealmsg');?></span>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Deal Management</a></li>
            <li class="active">Upload Deal</li>
          </ol>
</section>

<section class="content">
	<div class="row">
	<div class="col-md-12">
		<div class="box box-warning">
			<div class="box-header with-border">
			  <h3 class="box-title">Deal </h3>
			</div><!-- /.box-header -->
			<div class="box-body" style="height:100%;">
			  <form action="<?php echo $formurl;?>" id="upload-deal-form" method="post" enctype="multipart/form-data">
				<!-- text input -->
				<div class="col-md-6">
					<div class="form-group <?php echo check_error('restaurant');?>">
					  <label>Restaurant<span class="required_star">*</span></label>
					  <input  type="text" class="form-control" placeholder="Enter in English..." name="restaurant" value="<?php echo (isset($dealRes[0]->restaurant) && $dealRes[0]->restaurant !='')?$dealRes[0]->restaurant:$merchantinfo[0]->company_name;?>">
					  <label class="control-label" for="inputError"><?php echo form_error('restaurant'); ?></label>
					</div>
					
					
					<div class="form-group <?php echo check_error('deal_image');?>">
					  <label>Image (Size is less than 1MB)<span class="required_star">*</span></label>
					  <input type="file" name="deal_image" id="deal_image">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_image'); ?></label>
					  <label class="control-label" id="file-error" for="inputError" style="color:red;"><?php echo $this->session->flashdata('uploadimgmsg'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('price_show_option');?> <?php echo check_error('price_show_option');?>">
					  <label>Select option to display deal price<span class="required_star">*</span></label>
					   <div class="radio">
                        <label>
                          <input type="radio" onclick="select_price_option(this)" value="1<?php //echo (isset($dealRes[0]->price_show_option) && $dealRes[0]->price_show_option !='')?$dealRes[0]->price_show_option:(isset($_POST['price_show_option']) ? $_POST['price_show_option'] : 1);?>" id="optionsRadios1" name="price_show_option" <?php if((@$dealRes[0]->price_show_option == 1) || (@$this->input->post('price_show_option') == 1)) { echo "checked=checked"; }?>>
                          Old and new price format
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" onclick="select_price_option(this)" value="2<?php //echo (isset($dealRes[0]->price_show_option) && $dealRes[0]->price_show_option !='')?$dealRes[0]->price_show_option:(isset($_POST['price_show_option']) ? $_POST['price_show_option'] : 2);?>" id="optionsRadios2" name="price_show_option" <?php if((@$dealRes[0]->price_show_option == 2) || (@$this->input->post('price_show_option') == 2)) { echo "checked=checked"; } ?>>
                          Discount in % format
                        </label>
                      </div>
					  <label class="control-label" for="inputError"><?php echo form_error('price_show_option'); ?></label>
					</div>
					
					<div style="<?php if((@$dealRes[0]->price_show_option == 1) || (@$this->input->post('price_show_option') == 1)) { ?>display:block;<?php } else { ?>display:none;<?php } ?>" id="show_old_new_price" class="form-group <?php echo check_error('old_price');?> <?php echo check_error('new_price');?>">
					 <label>Set Old and New Price in Euro<span class="required_star">*</span></label>
					 <div class="row">
						<div class="col-xs-4">
						  <input type="text" name="old_price" placeholder="Old Price" class="form-control" value="<?php echo (isset($dealRes[0]->old_price) && $dealRes[0]->old_price !='')?$dealRes[0]->old_price:$this->input->post('old_price');?>">
						</div>
						<div class="col-xs-4">
						  <input type="text" name="new_price" placeholder="New Price" class="form-control" value="<?php echo (isset($dealRes[0]->new_price) && $dealRes[0]->new_price !='')?$dealRes[0]->new_price:$this->input->post('new_price');?>">
						</div>
					</div>	
						<label class="control-label" for="inputError"><?php echo form_error('new_price'); ?></label>
                  </div>
                  
					<div style="<?php if((@$dealRes[0]->price_show_option == 2) || (@$this->input->post('price_show_option') == 2)) { ?>display:block;<?php } else { ?>display:none;<?php } ?>" id="show_discount_price" class="form-group <?php echo check_error('discount');?> <?php echo check_error('discount_dt');?>">
					  <label>Discount<span class="required_star">*</span></label>
					  <input type="text" class="form-control" placeholder="Enter in English..." name="discount" value="<?php echo (isset($dealRes[0]->discount) && $dealRes[0]->discount !='')?$dealRes[0]->discount:$this->input->post('discount');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('discount'); ?></label>
					  <input type="text" class="form-control" placeholder="Enter in Dutch..." name="discount_dt" value="<?php echo (isset($dealRes[0]->discount_dt) && $dealRes[0]->discount_dt !='')?$dealRes[0]->discount_dt:$this->input->post('discount_dt');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('discount_dt'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('deal_date');?>">
					  <label>Select deal start date and time<span class="required_star">*</span></label>
					  <input type="text" id="deal_date" class="form-control" placeholder="Enter ..." name="deal_date" value="<?php echo (isset($dealRes[0]->dealdate) && $dealRes[0]->dealdate !='')?$dealRes[0]->dealdate:$this->input->post('deal_date');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_date'); ?></label>
					</div>
						
				</div>
				
				<div class="col-md-6 bt_no">
					<div class="form-group <?php echo check_error('noof_deals_claimed');?>">
					  <label>Number of people for a deal which can be claimed<span class="required_star">*</span></label>
					  <input type="text" class="form-control" placeholder="Enter ..." name="noof_deals_claimed" value="<?php echo (isset($dealRes[0]->n_of_d_claimed) && $dealRes[0]->n_of_d_claimed !='')?$dealRes[0]->n_of_d_claimed:$this->input->post('noof_deals_claimed');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('noof_deals_claimed'); ?></label>
					</div>
					<!--
					<div class="form-group <?php echo check_error('noof_people_claimed_deal');?>">
					  <label>Number of people for a deal which can be claimed</label>
					  <input type="text" class="form-control" placeholder="Enter ..." name="noof_people_claimed_deal" value="<?php echo (isset($dealRes[0]->n_of_people_claimed) && $dealRes[0]->n_of_people_claimed !='')?$dealRes[0]->n_of_people_claimed:$this->input->post('noof_people_claimed_deal');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('noof_people_claimed_deal'); ?></label>
					</div>
					-->
					<div class="form-group <?php echo check_error('deal_type');?>">
					  <label>Type of Meal<span class="required_star">*</span></label>
					  <select class="form-control" name="deal_type" onchange="select_other_deal(this)">
						<?php if(count($dealtype)>0){
							foreach($dealtype as $dealtype) { ?>
							<option value="<?php echo $dealtype->id;?>" <?php echo (isset($dealRes[0]->type_of_meal) && $dealRes[0]->type_of_meal == $dealtype->id)?"selected=selected":'';?>><?php echo $dealtype->name;?></option>	
						<?php	}	
						}
						?>
					  </select>
					  <label class="control-label" for="inputError"><?php echo form_error('deal_type'); ?></label>
					</div>
					<?php
						if(isset($dealRes[0]->type_of_meal) && $dealRes[0]->type_of_meal == 14){
								$display = "display:block";
						}else{
							$display = "display:none";
						}
					?>
					<div id="other_deal_type_outer" style="<?php echo $display;?>" class="form-group <?php echo check_error('other_deal_type');?>">
					  <label>Other Meal Type<span class="required_star">*</span></label>
					  <input type="text" class="form-control" placeholder="Enter ..." name="other_deal_type" value="<?php echo (isset($dealRes[0]->other_type_of_meal) && $dealRes[0]->other_type_of_meal !='')?$dealRes[0]->other_type_of_meal:$this->input->post('other_deal_type');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('other_deal_type'); ?></label>
					</div>
					
					<div class="form-group <?php echo check_error('deal_title');?> <?php echo check_error('deal_title_dt');?>">
					  <label>Deal Title<span class="required_star">*</span></label>
					  <input type="text" class="form-control" placeholder="Enter in English..." name="deal_title" value="<?php echo (isset($dealRes[0]->deal_title) && $dealRes[0]->deal_title !='')?$dealRes[0]->deal_title:$this->input->post('deal_title');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_title'); ?></label>
					  <input type="text" class="form-control" placeholder="Enter in Dutch..." name="deal_title_dt" value="<?php echo (isset($dealRes[0]->deal_title_dt) && $dealRes[0]->deal_title_dt !='')?$dealRes[0]->deal_title_dt:$this->input->post('deal_title_dt');?>">
					  <label class="control-label" for="inputError"><?php echo form_error('deal_title_dt'); ?></label>
					</div>
					<div class="form-group <?php echo check_error('lat');?>">
					  <label>Latitude and Longitude is the deal place that will be shown in the application. You can change the place by dragging the map pointer. <br />Latitude<span class="required_star">*</span></label>
					  <input type="text" id="lat" class="form-control" placeholder="Enter ..." name="lat" value="<?php echo (isset($dealRes[0]->lat) && $dealRes[0]->lat !='')?$dealRes[0]->lat:52.1326;?>">
					  <label class="control-label" for="inputError"><?php echo form_error('lat'); ?></label>
					</div>
					<div class="form-group <?php echo check_error('lng');?>">
					  <label>Longitude<span class="required_star">*</span></label>
					  <input type="text" id="lng" class="form-control" placeholder="Enter ..." name="lng" value="<?php echo (isset($dealRes[0]->lng) && $dealRes[0]->lng !='')?$dealRes[0]->lng:5.2913;?>">
					  <label class="control-label" for="inputError"><?php echo form_error('lng'); ?></label>
					</div>	
				</div>

				<?php if(isset($dealRes[0]->image)):?>
						<input type="hidden" name="hidden_img" id="hidden_img" value="<?php echo $dealRes[0]->image;?>">
				<?php endif;?>
				<div class="box-footer">
					<button class="btn btn-primary" type="submit" name="submit" value="Submit">Submit</button>
				</div>							 
				<div class="clearfix"></div>
				 <script src="http://maps.googleapis.com/maps/api/js"></script>
				<script src="<?php echo site_url('assets/js/google.js');?>"></script>
				<div id="map" style="width:100%;height:380px;"></div>
			  </form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
				
		</div>
	</div>
</section>	        

	
</div>
<?php if(!$editid){ ?>
<script>
	$("#upload-deal-form").submit(function(){
		if($("#deal_image").val() == ''){
			$("#file-error").html('Please Select Image');
			return false;
		}
	});
</script>
<?php } ?>

 <?php 
	$file_base_path = site_url().base_url_file;
?>
  <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $file_base_path;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php if($this->session->userdata('islogin')) { echo $this->session->userdata('company_name');}?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
         
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Merchant</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url()."register"?>"><i class="fa fa-circle-o"></i> New</a></li>
                <li><a href="<?php echo site_url()."merchant-list"?>"><i class="fa fa-circle-o"></i> List</a></li>
                <!--<li><a href="#"><i class="fa fa-circle-o"></i> Credit</a></li>-->
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?php echo site_url()."user-list"?>"><i class="fa fa-circle-o"></i>List</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Deal</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url()."admin-deals"?>"><i class="fa fa-circle-o"></i>Deals</a></li>
              </ul>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Cuisine</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
				<li class="active"><a href="cuisine-list"><i class="fa fa-circle-o"></i>Manage Cuisine</a></li>
				<li><a href="cuisine"><i class="fa fa-circle-o"></i>Add Cuisine</a></li>
              </ul>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Merchant Credits</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?php echo site_url('payment-list');?>"><i class="fa fa-circle-o"></i>Payment List</a></li>
                <!--<li class="active"><a href="<?php echo site_url('merchant-credits-details');?>"><i class="fa fa-circle-o"></i>Merchant Credits Details</a></li>-->
              </ul>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?php echo site_url();?>settings"><i class="fa fa-circle-o"></i>Basic</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

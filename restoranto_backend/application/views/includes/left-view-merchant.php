 <?php 
	$file_base_path = site_url().base_url_file;
?>
  <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $file_base_path;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php if($this->session->userdata('islogin')) { echo $this->session->userdata('company_name');}?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
         <p></p>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Deal Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?php echo site_url()."merchant-upload-deal"?>"><i class="fa fa-circle-o"></i> Upload Deal</a></li>
                <li><a href="<?php echo site_url()?>merchant-deals"><i class="fa fa-circle-o"></i> Uploaded Deals</a></li>
                <li><a href="<?php echo site_url()?>merchant-claimeddeals"><i class="fa fa-circle-o"></i> Claimed Deals</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Merchant Credits</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?php echo site_url('buy-credits');?>"><i class="fa fa-circle-o"></i>Buy Credits</a></li>
                <li class="active"><a href="<?php echo site_url('merchant-payment-list');?>"><i class="fa fa-circle-o"></i>Payment Details</a></li>
                <li class="active"><a href="<?php echo site_url('credit-details');?>"><i class="fa fa-circle-o"></i>Credits Details</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

<?php 
	$file_base_path = site_url().base_url_file;
?>


  <!-- Morris.js charts -->
    
    <!-- Sparkline -->
    <script src="<?php echo $file_base_path;?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo $file_base_path;?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo $file_base_path;?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo $file_base_path;?>plugins/knob/jquery.knob.js"></script>
    <!-- DataTables -->
    <script src="<?php echo $file_base_path;?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $file_base_path;?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo $file_base_path;?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo $file_base_path;?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo $file_base_path;?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo $file_base_path;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $file_base_path;?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $file_base_path;?>dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $file_base_path;?>dist/js/demo.js"></script>
    
    <script src="<?php echo site_url()?>assets/js/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo site_url()?>assets/js/upload_deals.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  

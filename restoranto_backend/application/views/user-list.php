<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            User List
            <small>All</small>
            <small class="gn"><?php echo $this->session->flashdata('registermsg');?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">List</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">User List with All Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive">	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone no.</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php if(count($userlist)>0){
							foreach($userlist as $user){
								?>
								<tr>
									<td><?php echo $user->name?></td>
									<td><?php echo $user->email?></td>
									<td><?php echo $user->phoneno?></td>
									<td class="center">
										<select onchange="change_user_status('<?php echo $user->id;?>',this,'front-end-user')" id="userstatus" name="userstatus">
										<option <?php echo ($user->status==1)?'selected=selected':'';?> value="1">Active</option>
										<option <?php echo ($user->status==0)?'selected=selected':'';?> value="0">Deactive</option>
										</select>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a class="btn btn-primary btn-xs " onclick="delete_user('<?php echo $user->id;?>','front-end-user')" href="javascript::void(0)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
							}	
						}   
					?> 	
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone no.</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>    

<script src="<?php echo site_url()?>assets/js/manage_user.js"></script>


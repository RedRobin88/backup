<?php 
	$file_base_path = site_url().base_url_file;
	if(isset($cuisine_details[0]->id) && $cuisine_details[0]->id!=''){
			$editid = "/".$cuisine_details[0]->id;
	}else{
		$editid = "";
	}
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            New Cuisine
            <small>Form</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cuisine</a></li>
            <li class="active">New</li>
          </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning fl">
				<form action="<?php echo site_url('admin/admin/cuisine'.$editid)?>" method="post">
				
				<div class="col-md-6">
					<div class="box-header with-border">
					  <h3 class="box-title">Add </h3>
					</div><!-- /.box-header -->
					
					<div class="box-body">
							<!-- text input -->
							<div class="form-group <?php echo check_error('name');?> <?php echo check_error('name_dt');?>">
							  <label>Name</label>
							  <input type="text" class="form-control" id="inputError" placeholder="Enter in English ..." name="name" value="<?php echo (isset($cuisine_details) && $cuisine_details[0]->name!='')?$cuisine_details[0]->name:$this->input->post('name');?>">
							  <label class="control-label" for="inputError"><?php echo form_error('name'); ?></label>
							  <input type="text" class="form-control" id="inputError" placeholder="Enter in Dutch ..." name="name_dt" value="<?php echo (isset($cuisine_details) && $cuisine_details[0]->name_dt!='')?$cuisine_details[0]->name_dt:$this->input->post('name_dt');?>">
							  <label class="control-label" for="inputError"><?php echo form_error('name_dt'); ?></label>
							</div>
							<div class="form-group <?php echo check_error('status');?>">
							  <label>Status</label>
							  <select class="form-control" name="status">
								  <option value="1" <?php if(isset($cuisine_details) && $cuisine_details[0]->status==1){ echo "selected=selected"; } ?> >Active</option>
								  <option value="0" <?php if(isset($cuisine_details) && $cuisine_details[0]->status==0){ echo "selected=selected"; } ?> >Not Active</option>
							  </select>	  
							  <label class="control-label" for="inputError"><?php echo form_error('status'); ?></label>
							</div>
						</div><!-- /.box-body -->		
				</div>
				
				
				<div class="col-md-12">
					<div class="box-body">
						<div class="box-footer">
							<button class="btn btn-primary" type="submit" name="cuisine" value="cuisine">Submit</button>
						</div>
					</div>
				</div>	
				</form>
				
			</div><!-- End of Box -->
		</div>	<!-- End ofcol-md-12 -->		 
		
		
		
	</div><!-- End of row -->
</section>	        

	
</div>

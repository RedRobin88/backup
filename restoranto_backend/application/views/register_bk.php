<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant Registration
            <small>Form</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Merchant</a></li>
            <li class="active">New</li>
          </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-6">
							<div class="box box-warning">
								<div class="box-header with-border">
								  <h3 class="box-title">Company </h3>
								</div><!-- /.box-header -->
								<div class="box-body">
								  <form action="<?php echo site_url('admin/admin/register')?>" method="post">
									<!-- text input -->
									
									<div class="form-group <?php echo check_error('company_name');?>">
									  <label>Name</label>
									  <input type="text" class="form-control" id="inputError" placeholder="Enter ..." name="company_name" value="<?php echo $this->input->post('company_name');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_name'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_loc');?>">
									  <label>Location</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_loc" value="<?php echo $this->input->post('company_loc');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_loc'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_food_type');?>">
									  <label>Food Type</label>
									  <select class="form-control" name="company_food_type">
										<?php if(count($company_food_type)>0){
											foreach($company_food_type as $food) { ?>
											<option value="<?php echo $food->id;?>" <?php if($this->input->post('company_food_type') == 1) echo "selected=selected";?>><?php echo $food->name;?></option>	
										<?php	}	
										}
										?>
									  </select>
									  <label class="control-label" for="inputError"><?php echo form_error('company_food_type'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_phoneno');?>">
									  <label>Phone Number</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_phoneno" value="<?php echo $this->input->post('company_phoneno');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_phoneno'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('company_website');?>">
									  <label>Website</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="company_website" value="<?php echo $this->input->post('company_website');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('company_website'); ?></label>
									</div>
									<div class="box-header with-border">
									  <h3 class="box-title">Owner </h3>
									</div><!-- /.box-header -->
									<div class="form-group <?php echo check_error('owner_name');?>">
									  <label>Name</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="owner_name" value="<?php echo $this->input->post('owner_name');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('owner_name'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('owner_phoneno');?>">
									  <label>Phone Number</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="owner_phoneno" value="<?php echo $this->input->post('owner_phoneno');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('owner_phoneno'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('email');?>">
									  <label>Email</label>
									  <input type="text" class="form-control" placeholder="Enter ..." name="email" value="<?php echo $this->input->post('email');?>">
									  <label class="control-label" for="inputError"><?php echo form_error('email'); ?></label>
									</div>
									<div class="form-group <?php echo check_error('password');?>">
									  <label>Password</label>
									  <input type="password" class="form-control" placeholder="Enter ..." name="password">
									  <label class="control-label" for="inputError"><?php echo form_error('password'); ?></label>
									</div>
									<div class="box-footer">
										<button class="btn btn-primary" type="submit" name="register" value="register">Submit</button>
									 </div>
								  </form>
								</div><!-- /.box-body -->
				</div><!-- /.box -->
				
		</div>
	</div>
</section>	        

	
</div>

<?php 
	$file_base_path = site_url().base_url_file;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $file_base_path?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $file_base_path?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $file_base_path?>plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
		var site_url = '<?php echo site_url();?>';
    </script>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
	<div class="login_error_msg"><?php echo $this->session->flashdata('loginerror');?></div>
      <div class="login-logo">
        <a href="<?php echo site_url();?>"><b>RESTORANTO </b>Sign In</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg login-form-id">Sign in to start your session</p>
		<div class="login-form-id">	
			<form action="" method="post" id="admin-merchand-login">
			  <div class="form-group has-feedback">
				<input type="email" class="form-control" placeholder="Email" name="email">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<span class="error"><?php echo form_error('email'); ?></span>   
			  </div>
			  <div class="form-group has-feedback">
				<input type="password" class="form-control" placeholder="Password" name="password">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<span class="error"><?php echo form_error('password'); ?></span>  
			  </div>
			  <div class="row">
				
				<div class="pull-right">
				  <button type="submit" name="login" id="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div><!-- /.col -->
			  </div>
			  <div class="row">
				<div class="col-xs-12">
					<label><a href="register">Merchant Registration</a></label>
				</div><!-- /.col -->
				<div class="col-xs-12">
					<label><a href="javascript:void(0)" id="forgot-click">Forgot Password</a></label>
				</div><!-- /.col -->
			  </div>
			</form>
		</div>
		<div id="admin-login-forgot" style="display:none">
			<p class="login-box-msg">Forgot Password</p>
			 <form action="<?php echo site_url()?>user/forgotpassword" id="forgotpsw" name="forgotpsw" method="post">
			  <div class="form-group has-feedback">            
				<?php echo form_input('femailid','','placeholder="E-mail" id="femailid" class="form-control" autofocus');?> 
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<span class="forgot-email-error error"><?php echo form_error('femailid'); ?></span>
			  </div>
			  
			  <div class="row">
				<div class="col-xs-8">
				  
				</div><!-- /.col -->
				<div class="pull-right">
				  <?php echo form_submit('submit','Submit',' id="forgot-pass"  class="btn btn-primary btn-block btn-flat"');?>
				</div><!-- /.col -->
			  </div>
			<?php echo form_close();?>
			<a href="#" id="login-link-back">Back</a><br>
		</div>
       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $file_base_path?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $file_base_path?>bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo $file_base_path?>plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
      $("#forgot-click").click(function(){
			$(".login-form-id").hide();
			$("#admin-login-forgot").show();
		});
     
      $("#login-link-back").click(function(){
			$(".login-form-id").show();
			$("#admin-login-forgot").hide();
		});
		
	$("#forgotpsw").submit(function(){ 
		var emailid = $('[name="femailid"]').val().trim();
		
		if($('[name="femailid"]').val().trim() ==''){ 
				$(".forgot-email-error").html("Please Enter Email ID");
				return false;
		}
		
		$.ajax({
				url: site_url+'login/forgotpassword',
				data:{emailid:emailid},
				type:"post",
				async:true,
				success: function(data){
					//alert(data);return false;
					if(data.trim() !=1){
						$(".forgot-email-error").html(data);	
					}else{
							//return false;
							location.reload();
					}
				}
		});
		return false;
	});
    </script>
  </body>
</html>

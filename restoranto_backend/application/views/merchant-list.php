<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
            Merchant List
            <small>All</small>
            <small class="gn"><?php echo $this->session->flashdata('registermsg');?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Merchant</a></li>
            <li class="active">List</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Merchant List with All Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive">	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Company Name</th>
                        <th>Company Location</th>
                        <th>Company Phone no.</th>
                        <th>Company Website</th>
                        <th>Owner Name</th>
                        <th>Owner Phone no.</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php if(count($merchantlist)>0){
							foreach($merchantlist as $merchant){
								?>
								<tr>
									<td><?php echo $merchant->company_name?></td>
									<td><?php echo $merchant->company_loc?></td>
									<td><?php echo $merchant->company_phoneno?></td>
									<td><?php echo $merchant->company_website?></td>
									<td><?php echo $merchant->owner_name?></td>
									<td><?php echo $merchant->owner_phoneno?></td>
									<td><?php echo $merchant->email?></td>
								</tr>
								<?php
							}	
						}   
					?> 	
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Company Name</th>
                        <th>Company Location</th>
                        <th>Company Phone no.</th>
                        <th>Company Website</th>
                        <th>Owner Name</th>
                        <th>Owner Phone no.</th>
                        <th>Email</th>
                      </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>        



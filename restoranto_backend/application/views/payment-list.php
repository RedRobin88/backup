<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
           Merchant Payment Details
            <small>All</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Merchant Payment Details</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Merchant Payment Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="table-responsive"> 	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                       <th>#</th>
						<th>Merchant ID</th>
						<th>Merchant Name</th>
						<th>Transaction ID</th>
						<th>Amount</th>
						<!--<th>Status DateTime</th>-->
						<th>Status</th>
						<th>Payment Date Time</th>
						<th>Action</th>
					  </tr>
                    </thead>
                    <tbody>
					 <?php if(isset($result) && count($result)>0):?>
						<?php $i=1; foreach($result as $key=>$userdata):?>
						<tr class="odd gradeX">
								<td><?php echo $i;?></td>
								<td><?php echo $userdata->merhcant_id;?></td>
								<td><?php echo $userdata->owner_name;?></td>
								<td><?php echo $userdata->transaction_id;?></td>							
								<td><?php echo $userdata->amount;?></td>
								<!--<td><?php echo $userdata->statusDateTime;?></td>-->
								<td><?php echo $userdata->status;?></td>
								<td><?php echo $userdata->creation_time;?></td>
								<td><a href="javascript:void(0)" id="" onclick="credit_details(<?php echo $userdata->merhcant_id;?>)" class="btn btn-primary btn-xs" >Credit</a></td>
							</tr>
						<?php $i++; endforeach;?>
                     <?php endif;?>
                    </tbody>
                    <tfoot>
                      <tr>
                       <th>#</th>
						<th>Merchant ID</th>
						<th>Merchant Name</th>
						<th>Transaction ID</th>
						<th>Amount</th>
						<!--<th>Status DateTime</th>-->
						<th>Status</th>
						<th>Payment Date Time</th>
						<th>Action</th>
					  </tr>
                    </tfoot>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
   <div class="example-modal">
	<div class="modal fade" id="credit_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Credit Details</h4>
		  </div>
		  <div class="modal-body">
			<div class="row lable-value-wrp">
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Total Amount Paid</span></div> 
						  <div class="value-ctg"> 
							<span class="badge badge-primary" id="total-amount-paid">
								0
							</span>
						  </div>
					  </div>
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Total Credits for Paid Amount</span></div> 
						  <div class="value-ctg">
							  <span class="badge badge-primary " id="total-credits-for-paid">
								  0
							  </span>
						  </div>
					  </div>  
					 
					</div>
					
					<div class="row lable-value-wrp">
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Used Credits</span></div> 
						  <div class="value-ctg">
							<span class="badge badge-primary" id="used-credits">
								0
							</span> 
						  </div>
					  </div>
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Remaining Credits</span></div> 
						  <div class="value-ctg">
							  <span class="badge badge-primary " id="remaining-credits">
								  0
							  </span>
						   </div>
					  </div>  
					 
					</div>	
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
  </div><!-- /.example-modal -->
</div>   
<script> 
	function credit_details(creditid){
			//$("#credit_model").modal('show');
			$.ajax({
					url : site_url+'admin/admin/credit_details',
					data:{id:creditid},
					type:'post',
					success:function(data){
						if(data){
							var jsonparse = JSON.parse(data);
								//alert(jsonparse.paid_ammount); return false;
								$("#total-amount-paid").html(jsonparse.paid_ammount);
								$("#total-credits-for-paid").html(jsonparse.total_credits_for_paidamount);
								$("#used-credits").html(jsonparse.total_used_credits);
								$("#remaining-credits").html(jsonparse.remaining_credits);
								$("#credit_model").modal('show');
						}	
					}
			});
	}
</script> 



<?php 
	$file_base_path = site_url().base_url_file;
?>
<div class="content-wrapper">
<!-- Content Wrapper. Contains page content -->
<section class="content-header">
          <h1>
           Credit Details
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
            <li class="active">Credit Details</li>
          </ol>
</section>


   <section class="content">
        <div class="row">
            <div class="col-xs-12">
					 <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Credit Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					
					
					<div class="row lable-value-wrp">
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Total Amount Paid</span></div> 
						  <div class="value-ctg"> 
							<span class="badge badge-primary">
								<?php if(isset($paid_amount_result) && count($paid_amount_result)>0):
									if($paid_amount_result[0]->totalamount)
										echo $paid_amount_result[0]->totalamount." EURO";
									else 
										echo "0 EURO";
								endif;?>
							</span>
						  </div>
					  </div>
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Total Credits for Paid Amount</span></div> 
						  <div class="value-ctg">
							  <span class="badge badge-primary ">
								  <?php 
								    $total_amount = str_replace(",","",$paid_amount_result[0]->totalamount);
								    if(isset($setting_details) && count($setting_details)>0):
										$one_credit_value = $setting_details[0]->setting_value;
								    endif;
									echo $total_credits_for_paidamount = ($total_amount/$one_credit_value); 
								    ?>
							  </span>
						  </div>
					  </div>  
					 
					</div>
					
					<div class="row lable-value-wrp">
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Used Credits</span></div> 
						  <div class="value-ctg">
							<span class="badge badge-primary">
								<?php if(isset($total_credits_used) && count($total_credits_used)>0):
									if($total_credits_used[0]->totalpeople) 
										echo $total_used_credits = $total_credits_used[0]->totalpeople;
									else 
										echo "0";
								endif;?>
							</span> 
						  </div>
					  </div>
					  <div class="col-xs-12 col-sm-12 cl-md-6 col-lg-6">
						  <div class="lable-ctg"> <span class="label label-primary">Remaining Credits</span></div> 
						  <div class="value-ctg">
							  <span class="badge badge-primary ">
								  <?php echo $remaining_credits = @$total_credits_for_paidamount - @$total_used_credits; ?>
							  </span>
						   </div>
					  </div>  
					 
					</div>					
						
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
	</section>			       
  
</div>    



/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.etpl.restoranto;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml

        //printHashKey(MainActivity.this);
        loadUrl(launchUrl);

        try {
            PackageInfo info = this.getPackageInfo(this, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
               //Toast.makeText(this, "printHashKey() Hash Key: " + hashKey,Toast.LENGTH_LONG).show();
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }

    }
    public static PackageInfo getPackageInfo( Context pContext, int pFlags ) {
        try {
            PackageManager _pm = pContext.getPackageManager();
            return _pm.getPackageInfo( pContext.getPackageName(), pFlags);
        } catch( PackageManager.NameNotFoundException e ) {
            Log.e( TAG, "getPackageInfo()", e );
        } catch( Exception e ) {
            Log.e( TAG, "getPackageInfo()", e );
        }
        return null;
    }


}

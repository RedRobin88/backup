//controllers are packed into a module
angular.module('deepBlue.controllers', [])
// googleplace directive to get google map autocomplete location
.directive('googleplace', function() {
  return {
    require: 'ngModel',
    link: function(scope,element, attrs, model) {
      var options = {
        types: [],
        componentRestrictions: {}
      };
      scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

      google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                  var geoComponents = scope.gPlace.getPlace();
                  var latitude = geoComponents.geometry.location.lat();
                  var longitude = geoComponents.geometry.location.lng();
                  scope.userdata['lat']=latitude;
                  scope.userdata['lng']=longitude;
                  scope.intiMap(scope.userdata);    
               });
    }
  };
})
// for textbox focus...
.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        if(scope.setfocus){
          element[0].focus();
          element[0].click();
        }
      },1000);
    }
  };
})

/**top view controller  is it main and parant controller of app... sidemenu and header is mangaed from this controller.**/
.controller('AppCtrl', function($scope, $rootScope, $state,loginService,$ionicPopover,$translate,$ionicLoading) {
  // #SIMPLIFIED-IMPLEMENTATION:
  // Simplified handling and logout/setview function.
  $scope.lang='en';
  /**
  * : disableTap function to set google's search string into textbox 
  */
  $scope.disableTap = function(){
    container = document.getElementsByClassName('pac-container');
        // disable ionic data tab
        angular.element(container).attr('data-tap-disabled', 'true');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function(){
          document.getElementById('locationtext').blur();
        });
      };
      // popover language selection
      $ionicPopover.fromTemplateUrl('templates/popover.html', {
        scope: $scope,
      }).then(function(popover) {
        $scope.popover = popover;
      });

/**
  * : setLang set and store users default language into app ;
  */
  $scope.setLang=function(checklang){
   if($scope.isInternet()){
    if(checklang==='EN')
    {
      $translate.use('en');  
      window.localStorage.setItem('lang','en');
      $scope.popover.hide();
    }
    else if(checklang=="DT"){
      $translate.use('dt');
      window.localStorage.setItem('lang','dt');
      $scope.popover.hide();
    }

    if($state.current.name==='app.listview'){
      $scope.loading = $ionicLoading.show({content: 'Loading...',}); 
      $rootScope.userCurrentLatlong();
    }

    if($state.current.name==='app.mapview'){
                        $rootScope.userCurrentLatlongM();
                      }    
                    }else{ 

                    }  

                  }   


                  $rootScope.$on('$stateChangeStart', function(event, $state,toState, toParams, fromState, fromParams){

       // : Conditions of app states for hide sidemenu view  and  toggle searchbox
       $scope.search=[];
       if($state.name==='app.listview'){
        $rootScope.list=true;
      }else{
        $rootScope.list=false;

      }
    });
/**
  * : logout function to destroy userdata from app. 
  */
  $scope.logout = function(){
    $rootScope.setfocus=true;
    $rootScope.userdata = {};
    loginService.destroy('userdata');
    facebookConnectPlugin.logout(function (response) { 
            // logout success action....
          },function (response) {
             // logout error action....
           });
    $state.go('signin');
  };
      /**
  * : setView function is to set users current view like map or list (in progress). 
  */ 
  $scope.setView=function(){
   if($rootScope.viewtype==='app.mapview'){
     $rootScope.viewtype='app.listview';
     window.localStorage.setItem('viewtype','app.listview');
     $state.go($rootScope.viewtype);
   }else{
     $rootScope.viewtype='app.mapview'
     window.localStorage.setItem('viewtype','app.mapview');
     $state.go($rootScope.viewtype);
   }
 }
})

/*------ signupCtrl is  normal user's (not Facebook)  controller... we getInterest and signup from here-----*/

.controller('signupCtrl', function ($scope, $state, $rootScope,$http,$ionicPopup,signupservice,$ionicLoading,loginService,$cordovaOauth) {
//$rootScope.scrolval=false;
$(function(){

  $("#password").on('focus',function(){
    document.getElementById("content").scrollTop=0;
  })
  $('input').focus(function(){
   $(this).data('placeholder',$(this).attr('placeholder'))
   .attr('placeholder','');
 }).blur(function(){
   $(this).attr('placeholder',$(this).data('placeholder'));
 });
 $('p').on('click',function(){
  $event.preventDefault(); 
});
});

$scope.intrests={};
$scope.userdata={};
/**
* : getInterest 
* get  deals Intrest from server.
* 
*/

/**
* : signup 
* send data to signupservice's signup method.
* return  status Success or Failed.
*/
$scope.signup=function(){
  if($scope.isInternet()){
   $scope.loading = $ionicLoading.show({content: 'Loading...',}); 
   $scope.userdata.usertype='3';
   signupservice.signup($scope.userdata).then(function(data){
    if(data){
     if(data.status==="Success"){
      $scope.loading = $ionicLoading.hide();
      var alertPopup = $ionicPopup.alert({
       title: "Restoranto",
       template: data.msg
     });
      alertPopup.then(function(res) {
        loginService.set('userdata',JSON.stringify($scope.userdata));
        signupservice.check_mail($scope.userdata.email).then( function(data){
          if(data.status==='Success'){
            loginService.set('userdata',JSON.stringify(data));
            $rootScope.userdata=data;
            $scope.loading = $ionicLoading.hide();
            $state.go('app.mapview');
          }
        });  
      });
    }else{
      $scope.loading = $ionicLoading.hide();
      $ionicPopup.alert({
         // title: data.Status,
         title: "Restoranto",
         template: data.msg
       });
    }
  }

});
 }

}
/**
* : getProfile Method 
* get data from facebook and set it to database using fbsignup, if data is already available move to view screen.
* return  user's profile  OR  Failed.
*/
$scope.getProfile = function() {
  if($scope.isInternet()){
    $scope.loading = $ionicLoading.show({content: 'Loading...',});
    var access_token=window.localStorage.getItem('access_token');
    if(access_token) {
      $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: access_token, fields: "id,name,email", format: "json" }}).then(function(result) {
        $scope.profileData = result.data;
        window.localStorage.removeItem('access_token');
        $rootScope.name=$scope.profileData.name;
        $rootScope.email=$scope.profileData.email;
        signupservice.check_mail($scope.profileData.email).then( function(data){
          if(data.status==='Success'){
            loginService.set('userdata',JSON.stringify(data));
            $rootScope.userdata=data;
            $scope.loading = $ionicLoading.hide();
            $state.go('app.mapview');
          }else{
            $state.go('fbsignup');
          }
        });    
      }, function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Restoranto',
          template: '<center>There was a problem getting your profile.</center>',
        })
      });
    } else {
     $scope.loading = $ionicLoading.hide();
     var alertPopup = $ionicPopup.alert({
      title: 'Restoranto',
      template: '<center> User not logged in.</center>',
    })
   }
 }

};
/**
* : fblogin Method 
* retrive login page in appbrowser to login  .
* return  redirect to mapview or listview.
*/
$scope.fblogin=function(){
   if($scope.isInternet()){
    facebookConnectPlugin.login( ["email"],
    function (response) {
      if(response.status="connected"){
        window.localStorage.setItem("access_token", response.authResponse.accessToken);
        $scope.getProfile();
      }else{
        var alertPopup = $ionicPopup.alert({
          title: 'Restoranto',
          template: '<center>The sign in flow was Cancelled.</center>',
        })
      }
    });
   }
};

})


// facebook user and normal user login  controller.
.controller('signinCtrl', function($scope,$state,$rootScope,$ionicActionSheet,$ionicLoading,$cordovaOauth, $translate,$http,$state,signupservice,loginService,$ionicPopup,$stateParams) {


/**
* : userLogin 
* send data to loginService's login method.
* return  status Success and user's data or Failed.
*/
$scope.userl={};
$scope.setlang=true;
$rootScope.setfocus=false;
$scope.setPreLang=function(lang){
  if(lang==='en'){
    $scope.userl.language2='';
  }else{
    $scope.userl.language1='';
  }
  window.localStorage.setItem('lang',lang);
  $translate.use(lang);
  $scope.setBtn(lang);
   // $state.go('signin');
 }

 $scope.setBtn=function(lang){
  if($scope.userl.language1===true || $scope.userl.language2===true){
    $scope.setlang=false;
  }else{
    $scope.setlang=true;
  }
}

$(function(){
  $('input').focus(function(){
   $(this).data('placeholder',$(this).attr('placeholder'))
   .attr('placeholder','');
 }).blur(function(){
   $(this).attr('placeholder',$(this).data('placeholder'));
 });
  $('p').on('click',function(){
    $event.preventDefault(); // fix p clicked nothing.
  });
});
$scope.login={};
$scope.userLogin=function(){
  if($scope.isInternet())
   $scope.loading = $ionicLoading.show({content: 'Loading...',});
 loginService.login($scope.login).then( function(data){
  if(data)
    if(data.status==='Success'){
     $scope.loading = $ionicLoading.hide();
     loginService.set('userdata',JSON.stringify(data));
     $rootScope.userdata=data;
     $state.go('app.mapview');
   }else{
    $scope.loading = $ionicLoading.hide();
    $ionicPopup.alert({
       // title: data.Status,
       title: "Restoranto",
       template: data.msg
     });
  }
}); 
}
$scope.data = {};
/**
* : forgot Method 
* send forget data to loginService's forgot method.
* return  status Success and move to OTP page OR  Failed.
*/
$scope.forgot=function(){

  if($("#pass").val()!==$("#pass2").val()){
   var alertPopup = $ionicPopup.alert({
    title: 'Restoranto',cssClass: 'customPopup',
    template: '<center>Password not match.</center>',
  });
   return false;
 }

 var myPopup = $ionicPopup.show({
  template: '<input type="email" ng-model="data.email">',
    // title: 'Restoranto',
    title: 'Enter your email address.',
    scope: $scope,
    cssClass: 'customPopup',
    buttons: [
    { text: 'Cancel' },
    {
      text: '<b>Submit</b>',
      type: 'button-positive',
      onTap: function(e) {
        if (!$scope.data.email) {
         var alertPopup = $ionicPopup.alert({
          title: 'Restoranto',cssClass: 'customPopup',
          template: '<center>Input valid email.</center>',
        })
         e.preventDefault();
       } else {
        if($scope.isInternet())
          $scope.loading = $ionicLoading.show({content: 'Loading...',});
        loginService.forgot($scope.data.email).then(function(data){
          $scope.loading = $ionicLoading.hide();
          if(data.status==="Failed"){
           $scope.data.email='';
           var alertPopup = $ionicPopup.alert({
            title: 'Restoranto',
              template: '<center>' + data.msg + '</center>',
          })
         }else{
          $rootScope.forgetid=data.id;
          $state.go('forget');
        }
      }); 
      }
    }
  }
  ]
});
}
$scope.forget={};
$scope.passerr=false;
/**
* : setPassword Method 
* send forget data to loginService's setPassword method.
* return  status Success  OR  Failed.
*/
$scope.setPassword=function(){
  if($scope.isInternet()){
    if($scope.forget.rpassword!==$scope.forget.password){
      $scope.passerr=true;
      return false;
    }
    $scope.loading = $ionicLoading.show({content: 'Loading...',});
    $scope.forget.id=$rootScope.forgetid;
    loginService.setPassword($scope.forget).then(function(data){
      $scope.loading = $ionicLoading.hide();
      $scope.forget={}
      var alertPopup = $ionicPopup.alert({
        title: 'Restoranto',
        template: '<center>' + data.msg + '</center>',
      })
      $state.go('signin');
    })
  }

}

/**
* : fblogin Method 
* retrive login page in appbrowser to login  .
* return  redirect to mapview or listview.
*/

$scope.fblogin=function(){
   if($scope.isInternet()){
    facebookConnectPlugin.login( ["email"],
    function (response) {
      if(response.status="connected"){
        window.localStorage.setItem("access_token", response.authResponse.accessToken);
        $scope.getProfile();
      }else{
        var alertPopup = $ionicPopup.alert({
          title: 'Restoranto',
          template: '<center>The sign in flow was Cancelled.</center>',
        })
      }
    });
   }

};

/**
* : getProfile Method 
* get data from facebook and set it to database using fbsignup, if data is already available move to view screen.
* return  user's profile  OR  Failed.
*/
$scope.getProfile = function() {
  if($scope.isInternet()){
    $scope.loading = $ionicLoading.show({content: 'Loading...',});
    var access_token=window.localStorage.getItem('access_token');
    if(access_token) {
      $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: access_token, fields: "id,name,email", format: "json" }}).then(function(result) {
        $scope.profileData = result.data;
        window.localStorage.removeItem('access_token');
        $rootScope.name=$scope.profileData.name;
        $rootScope.email=$scope.profileData.email;
        signupservice.check_mail($scope.profileData.email).then( function(data){

          if(data.status==='Success'){
            loginService.set('userdata',JSON.stringify(data));
            $rootScope.userdata=data;
            $scope.loading = $ionicLoading.hide();
            $state.go('app.mapview');
          }else{
            $state.go('fbsignup');
          }
        });    
      }, function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Restoranto',
          template: '<center>There was a problem getting your profile.</center>',
        })
      });
    } else {
     $scope.loading = $ionicLoading.hide();
     var alertPopup = $ionicPopup.alert({
      title: 'Restoranto',
      template: '<center> User not logged in.</center>',
    })
   }
 }

};

})

// facebook user signup if users data not in database
.controller('fbsignupCtrl', function ($scope, $state, $rootScope,$http,$ionicPopup,signupservice,$ionicLoading,loginService) {
  $scope.intrests={};
  $scope.fbuserdata={};
  $scope.fbuserdata={
    'name':$rootScope.name,
    'email':$rootScope.email
  };
  $(function(){
    $('p').on('click',function(){
    $event.preventDefault(); // fix p clicked nothing.
  });
  });
/**
* getInterest function get the intrest for ne facebook user.
* return data.status="Success" and list of intrest or Faild
*/
$scope.loading = $ionicLoading.hide();

/**
* :signup function set data to for signupservice's  signup methos for new facebook user
* otherewise checkmail to know user is already exits  or not 
* return data.status="Success" aof intrest or Faild
*/
$scope.signup=function(){
  if($scope.isInternet()){
   $scope.loading = $ionicLoading.show({ content: 'Loading...'});
   $scope.fbuserdata.usertype='4';
   signupservice.signup($scope.fbuserdata).then(function(data){
    if(data){
     if(data.status==="Success"){
       signupservice.check_mail($scope.fbuserdata.email).then( function(data){
        if(data.status==='Success'){
          loginService.set('userdata',JSON.stringify(data));
          $rootScope.userdata=$scope.fbuserdata;
          $scope.loading = $ionicLoading.hide();
        }

      }); 
       var alertPopup = $ionicPopup.alert({
         title: "Restoranto",
         template: data.msg
       });
       alertPopup.then(function(res) {
         $state.go('app.mapview');
       });

     }else{
       $scope.loading = $ionicLoading.hide();
       $ionicPopup.alert({
           title: "Restoranto",
           template: data.msg
         });
     }
   }
 });
 }
}
})
// listview controller to set list of deal into app.
.controller('listVCtrl', function($scope, $ionicActionSheet,$rootScope,dealService,mapService,$ionicLoading,$cordovaPush,$cordovaBadge) {
  $rootScope.dealcount=0;
  $scope.mydeals=[];
  $scope.deals=[];
  $scope.noresult=false;
  $scope.userlatlnt={
    }


/**
* :setlist function get the new deal from Server.
* and set lis to view
* return status Sucess or Failed
*/
$scope.setlist=function(userlatlnt){
  $scope.deals=[];
  $scope.userlatlnt=userlatlnt;
  dealService.getDeals($scope.userdata).then( function(data){
   $scope.mydeals=[];
   $scope.loading = $ionicLoading.hide();
   if(data.status!=='Failed'){
    $scope.deals=data;
    $scope.noresult=false;
  }else{
     if(data.status==='Failed'){
      $scope.noresult=true;
    }
  }
            $rootScope.dealcount=0;
            for (i = 0; i < $scope.deals.length; i++){
             if($scope.deals[i].offer)
               for(var j=0;j<$scope.deals[i].offer.deal.length;j++){ 
                $rootScope.dealcount++;
                var lefttime=0;  
                if ($scope.deals[i].offer.deal[j].price_show_option==='1') {
                  $scope.deals[i].offer.deal[j].discountext=true;
                }else if ($scope.deals[i].offer.deal[j].price_show_option==='2'){
                 $scope.deals[i].offer.deal[j].discountext=false;
               }      
               time=$scope.deals[i].offer.deal[j].dealdate.split(' ');
               $scope.deals[i].offer.deal[j]['id']=$scope.deals[i].offer.deal[j].id;
               $scope.deals[i].offer.deal[j]['index']=i;
               $scope.deals[i].offer.deal[j]['object']=j;
               lefttime=$scope.deals[i].offer.deal[j].deal_left_minutes;
               $scope.deals[i].offer.deal[j]['leftsec']= lefttime * 60;
               var distance=$scope.deals[i].distance;
               $scope.deals[i].offer.deal[j]['distance']=parseFloat(distance).toFixed(2) + ' Km';
               $scope.mydeals.push($scope.deals[i].offer.deal[j]);  
             }
           } 
         });
}
/**
* :doRefresh function for pull refresh funactnality.
* return new deal data
*/
$scope.doRefresh = function() {
  if($scope.isInternet()){
   $rootScope.userCurrentLatlong();
   $scope.$broadcast('scroll.refreshComplete');
   $rootScope.dealcount=0;}
 };
/**
* :userCurrentLatlong function call mapService's getUserLocation method to get current location of user.
* return lat and lng aof intrest or Faild
*/
$rootScope.userCurrentLatlong=function(){
  $scope.userlatlnt={};
  if($scope.isInternet()){
    $scope.loading = $ionicLoading.show(); 
    mapService.getUserLocation().then(function(data){
      $scope.userlatlnt=data;
      $scope.userdata['lat']=data.lat;
      $scope.userdata['lng']=data.lng;
      $scope.setlist($scope.userdata);
    });
  }
}
$rootScope.userCurrentLatlong();
/**
* :timeLeft function to remove list item from listview when left time end.
*/
$scope.timeLeft=function(i){
  $("#deal"+i).remove();
  $scope.mydeals.splice(i,1);
}
})



// mapview controller.... to set map marker of user also restorant and location.
.controller('mapviewCtrl', function($scope,$state,$rootScope,dealService,mapService,$ionicLoading,$cordovaBadge) { 
  $rootScope.dealcount=0;
  $rootScope.deals={}
  $scope.userlatlnt={
      // 'interestid':$scope.userdata.interest
    }

/**
* :intiMap function get deals from server, call initialise map function to plant map markers.
* render map
* return status Sucess and list of deal or Failed
*/
$rootScope.intiMap=function(latdata){
  $scope.userlatlnt={};
  $scope.myLocation=[];
  $scope.userlatlnt.lat=latdata.lat;
  $scope.userlatlnt.lng=latdata.lng;
  $scope.userdata['lat']=latdata.lat;
  $scope.userdata['lng']=latdata.lng;
  $scope.loading = $ionicLoading.show();  
  $rootScope.deals={};
  dealService.getDeals($scope.userlatlnt).then( function(data){
   $scope.loading = $ionicLoading.hide();
   if(data.status!=='Failed'){
    $rootScope.deals=data;
  }else{
    google.maps.event.addDomListener(window, 'load', $scope.initialise());
  }  
  google.maps.event.addDomListener(window, 'load', $scope.initialise());
});
}
/**
* :userCurrentLatlong function get GPS lat and lng from positioning service .
* call intimap
* return JSON
*/
$rootScope.userCurrentLatlongM=function(){
  $scope.userlatlnt={};
  if($scope.isInternet()){
   $scope.loading = $ionicLoading.show();
   mapService.getUserLocation().then(function(data){
    $scope.loading = $ionicLoading.hide();
    $scope.userlatlnt=data;    // enable when want actual lat lng.
    $scope.userdata['lat']=data.lat;
    $scope.userdata['lng']=data.lng;
    $scope.intiMap($scope.userdata);
  });
 }

}
$rootScope.userCurrentLatlongM();
/**
* :getreloadmap function plant restorant marker on map .
* call initialise funtion to refresh maop
* return null
*/
$scope.getreloadmap=function(){
  $scope.initialise = function(){
    $rootScope.dealcount=0;
    var mapOptions = {
      center: $scope.userlatlnt,
      zoom: 11 ,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    map.setCenter($scope.userlatlnt);
    var myicon = {
      url: "img/pointer.png",
      scaledSize: new google.maps.Size(16, 30),
      origin: new google.maps.Point(0,0), 
      anchor: new google.maps.Point(0, 0) 
    };
    $scope.myLocation = new google.maps.Marker({
      position: new google.maps.LatLng($scope.userlatlnt),
      map: map,
      animation: google.maps.Animation.DROP,
      title: "Restoranto",
      icon:myicon,
    });
    $scope.map = map;
    var userinfoWindow = new google.maps.InfoWindow({
      maxWidth: 400
    });
    $scope.myLocation.content='<div id="iw-container"><div class="iw-title">Restoranto<div class="clo"><i class="fa fa-times clocsebtn1"></i></div> </div><div class="twi_list" ><a class="item item-avatar" href="#"><p>Current location</p></a></div></div>';
    google.maps.event.addListener($scope.myLocation, 'click', function(){
      infoWindow.close();
      userinfoWindow.setContent($scope.myLocation.content);
      userinfoWindow.open(map, $scope.myLocation);
    });

    google.maps.event.addListener(userinfoWindow, 'domready', function() {
      var iwOuter = $('.gm-style-iw');
      var iwBackground = iwOuter.prev();
      iwBackground.children(':nth-child(2)').css({'display' : 'none'});
      iwBackground.children(':nth-child(4)').css({'display' : 'none'});
      iwOuter.parent().parent().css({left: '30px'});
      iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 85px !important;'});
      iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 85px !important;'});
      iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
      var iwCloseBtn = iwOuter.next();
      iwCloseBtn.css({
        'opacity': '0', 
        'right': '-113px', 
        'top': '20px', 
        'height':'15px',
        'width':'15px',
        'border': '1px solid #a00000', 
        'background-color': '#ffffff',
        'border-radius': '100%'
      });
      $('.clocsebtn1').on('click',function(){
        userinfoWindow.close();
      });
      if($('.iw-content').height() < 140){
        $('.iw-bottom-gradient').css({display: 'none'});
      }
    });
    google.maps.event.addListener(map, 'click', function() {
      userinfoWindow.close();
    });
            // Additional Markers //
            $scope.markers = [];
            var  infoWindow = new google.maps.InfoWindow({
              maxWidth: 400
            });
            var icon = {
              url: "img/icoon_restaurant.png", 
            };      
            var createMarker = function (info,index){
              var marker = new google.maps.Marker({
                position: new google.maps.LatLng(info.lat, info.lng),
                map: $scope.map,
                animation: google.maps.Animation.DROP,
                title: info.city,
                icon:icon
              });
              $scope.createOfferlist=function(offer,index){  

                if(offer!==null){
                  var returnoffer='<div class="iw-title">'+offer.deal[0].restaurant+'<div class="clo"><i class="fa fa-times clocsebtn"></i></div> </div><div class="twi_list">';            
                  for(var i=0;i<offer.deal.length;i++){
                    $rootScope.dealcount++;
                        var datetime=offer.deal[i].dealdate.split(' ');
                        returnoffer+='<a class="item item-avatar" href="#/deatailView/'+offer.deal[i].id+'/'+index+'/'+i+'">'+
                        '<img src="'+offer.deal[i].image+'">'+
                        '<p>'+offer.deal[i].deal_title+'</p>'+
                        '</a>';
                      }
                      returnoffer+='</div>'
                      return returnoffer;
                    } 
                  }
                  marker.content ='<div id="iw-container">' +$scope.createOfferlist(info.offer,index)+'</div>';
                  google.maps.event.addListener(marker, 'click', function(){
                   userinfoWindow.close();
                   infoWindow.setContent(marker.content);
                   infoWindow.open($scope.map, marker);
                 });
                  google.maps.event.addListener(infoWindow, 'domready', function() {
                    var iwOuter = $('.gm-style-iw');
                    var iwBackground = iwOuter.prev();
                    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
                    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                    iwOuter.parent().parent().css({left: '30px'});
                    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 85px !important;'});
                    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 85px !important;'});
                    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
                    var iwCloseBtn = iwOuter.next();
                    iwCloseBtn.css({
                      'opacity': '0', 
                      'right': '-113px', 
                      'top': '20px', 
                      'height':'15px',
                      'width':'15px',
                      'border': '1px solid #a00000', 
                      'background-color': '#ffffff',
                      'border-radius': '100%'
                    });
                    $('.clocsebtn').on('click',function(){
                      infoWindow.close();
                    });
                    if($('.iw-content').height() < 140){
                      $('.iw-bottom-gradient').css({display: 'none'});
                    }
                  });
                  google.maps.event.addListener(map, 'click', function() {
                    infoWindow.close();
                  });
                  $scope.markers.push(marker);
                }
                $rootScope.dealcount=0;
                for (i = 0; i < $rootScope.deals.length; i++){
                  if($rootScope.deals[i].offer){
                    createMarker($rootScope.deals[i],i);
                  }
                } 
              };
            }
             $scope.getreloadmap();
         })

/**
* :deatailView Controller filter selected deal and show whole detail to user with countdown.
* 
* return null
*/ 
.controller('deatailView', ['$scope','$stateParams','$rootScope','dealService','$ionicLoading','mapService',function($scope,$stateParams,$rootScope,dealService,$ionicLoading,mapService){

  $scope.userlatlnt={ };

/**
* :loadDealData function  filter selected deal and show whole detail to user with countdown.
* 
* return null
*/
$scope.currentdeal=[];
$scope.id=$stateParams.id;
$scope.ofindex=$stateParams.ofindex;
$scope.index=$stateParams.index;
$scope.loadDealData=function(ltlng){

 if($scope.isInternet()){
  $scope.userlatlnt.lat=ltlng.lat;
  $scope.userlatlnt.lng=ltlng.lng;
  $scope.loading = $ionicLoading.show(); 
  dealService.getDeals($scope.userlatlnt).then( function(data){
    $scope.loading = $ionicLoading.hide();
    if(data.status!=='Failed'){
      $scope.allDeals=data;
      $scope.deals=$scope.allDeals[$stateParams.index];
      $scope.currentdeal=$scope.deals.offer.deal[$scope.ofindex];
      $scope.currentdeal['leftsecond'] = $scope.currentdeal.deal_left_minutes * 60; 
      if ($scope.currentdeal.price_show_option==='1') {
        $scope.discountext=true;
              }else if ($scope.currentdeal.price_show_option==='2'){
                 
                 $scope.discountext=false;
               } 
             }
           });
}
}

 /**
* :userCurrentLatlong function call mapService's getUserLocation method to get current location of user.
* return lat and lng aof intrest or Faild
*/
$scope.userCurrentLatlong=function(){
  if($scope.isInternet()){
    $scope.loadDealData($scope.userdata);
    
  }
}
$scope.userCurrentLatlong();
$scope.timeLeft=function(){
 $("#btnbook").attr('disabled', true);
}

}])


// confirm deal controller render confrim view with data....
.controller('confirmDeal', ['$scope','$stateParams', '$rootScope','dealService','$state','$ionicPopup','$ionicLoading','$rootScope','mapService','signupservice', function($scope,$stateParams,$rootScope,dealService,$state,$ionicPopup,$ionicLoading,$rootScope,mapService,signupservice){
 var limit=0;
 $scope.start=1;
 $scope.userlatlnt={ };
 $scope.currentdeal=[];
 $scope.setData={}
 $scope.id=$stateParams.id;
 $scope.ofindex=$stateParams.ofindex;
 $scope.index=$stateParams.index;
 $scope.loadDealData=function(ltlng){
   if($scope.isInternet()){
    $scope.userlatlnt.lat=ltlng.lat;
    $scope.userlatlnt.lng=ltlng.lng;
    $scope.loading = $ionicLoading.show(); 
    $scope.loading = $ionicLoading.show(); 
    dealService.getDeals($scope.userlatlnt).then( function(data){
      $scope.loading = $ionicLoading.hide();
      if(data.status!=='Failed'){
        $scope.allDeals=data;
        $scope.deals=$scope.allDeals[$stateParams.index];
        $scope.currentdeal=$scope.deals.offer.deal[$scope.ofindex];
        $scope.currentdeal['leftsecond'] = $scope.currentdeal.deal_left_minutes * 60; 
        limit=$scope.currentdeal.remaining_deal;
      }
    });
  }
}


$scope.plus_people=function(keyword,event){
  $scope.manage_people(keyword,event);
}
$scope.minus_people=function(keyword,event){
  $scope.manage_people(keyword,event);
}

$scope.manage_people=function(keyword,event){

  if(keyword==='minus'){
    if($scope.start > 1){
      $scope.start=parseInt($scope.start) - 1;
    }
  }else if(keyword==='plus'){
    if($scope.start<limit){
     $scope.start=parseInt($scope.start) + 1;
   }else if($scope.start===limit){
     event.stopPropagation();
     event.preventDefault();
   }
 }
}

 /**
* :userCurrentLatlong function call mapService's getUserLocation method to get current location of user.
* return lat and lng aof intrest or Faild
*/
$scope.userCurrentLatlong=function(){
  if($scope.isInternet()){
    $scope.loadDealData($scope.userdata);
  }
}
$scope.userCurrentLatlong();
$scope.timeLeft=function(){
 $("#btncbook").attr('disabled', true);
}
/**
* :confirmDeal function  send deail detail to dealService claimedDeal .
* deal id and userid
* return Status
*/
$scope.confirmDeal=function(){
  if($scope.isInternet()){
    $scope.loading = $ionicLoading.show({content: 'Loading...',});
$scope.setData={
          'deal_id':$scope.currentdeal.id ,
          'userid':$scope.userdata.id,
          'no_of_people':$scope.start
        };
    dealService.claimedDeal($scope.setData).then(function(data){
         $scope.loading = $ionicLoading.hide();
         if(data.status==='Success'){
           $state.go('dealconfirmscreen');
         }else{
           var alertPopup = $ionicPopup.alert({
             title: "Restoranto",
             template: data.msg
           });
         }
       }); 
  }
}

}])
// after deal submited...
.controller('dealconfirmscreen', ['$scope','$state', function($scope,$state){

  $scope.backToList=function(){
    $state.go('app.mapview');
  }
}])
/**
* :noloactionCtrl function  call from mapService if GPS turn off.
*/
.controller('noloactionCtrl', ['$scope','$state','$stateParams','$rootScope','$ionicLoading','loginService', function($scope,$state,$stateParams,$rootScope,$ionicLoading,loginService){
  $scope.msg=$stateParams.msg;
  $scope.backToList=function(){
    var loggedu=JSON.parse(loginService.get('userdata'));
    if(loggedu){
     $state.go('app.mapview');
   }else{
    if(window.localStorage.getItem('lang')==null){
     $state.go('language');
   }else{
    $state.go('signin');
  }
}
}
/**
* :CheckGPS function  check lat and lng of user every 3 sec.
* 
* data : lat long
*/
$scope.CheckGPS=function(){
  if($scope.isInternet()){
   mapService.getUserLocation().then(function(data){
    if(data!==undefined){
      $scope.backToList();
    }
  });
 }
}
}])
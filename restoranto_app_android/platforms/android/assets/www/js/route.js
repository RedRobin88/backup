angular.module('deepBlue.route', [])
.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
// application routes with state.
$ionicConfigProvider.views.maxCache(0);
$stateProvider

.state('app', {
  url: '/app',
  abstract: true,
  templateUrl: 'templates/menu.html',
  controller: 'AppCtrl'
})

.state('language', {
 url: '/language',
 cached : false,
 templateUrl: 'templates/language.html',
 controller : 'signinCtrl'
 
})

.state('signin', {
 url: '/signin',
 cached : false,
 templateUrl: 'templates/sign_in.html',
 controller : 'signinCtrl'
 
})

.state('forget', {
 url: '/forget:data',
 cached : false,
 templateUrl: 'templates/forgot.html',
 controller : 'signinCtrl'
 
})         
.state('signup', {
  url: '/signup',
  cached : false,
  templateUrl: 'templates/sign_up.html',
  controller : 'signupCtrl'
})

.state('fbsignup', {
  url: '/fbsignup',
  cached : false,
  templateUrl: 'templates/fb_sign_up.html',
  controller : 'fbsignupCtrl', 
})

.state('app.listview', {
  url: '/listview',
  cache : false,
  views: {
    'menuContent': {
      templateUrl: 'templates/listview.html',
      controller : 'listVCtrl'
    }
  }
})
.state('app.mapview', {
  url: '/mapview',
  cache : false,
  views: {
    'menuContent': {
      templateUrl: 'templates/mapview.html',
      controller : 'mapviewCtrl'
    }
  }
}) 

.state('deatailView', {
  url: '/deatailView/:id/:index/:ofindex',
  cache : false,
  templateUrl: 'templates/details.html',
  controller : 'deatailView'
})



.state('confirmDeal', {
  url: '/confirmDeal/:id/:index/:ofindex',
    // url: '/confirmDeal',
    cache : false,
    templateUrl: 'templates/confirmed.html',
    controller : 'confirmDeal'
  })

.state('dealconfirmscreen', {
  url: '/dealconfirmscreen',
  cache : false,
  templateUrl: 'templates/confirmdeal.html',
  controller : 'dealconfirmscreen'
})

.state('noloaction', {
  url: '/noloaction/:msg',
  cache : false,
      templateUrl: 'templates/nolocation.html',
      controller : 'noloactionCtrl'
})

.state('nointernet', {
  url: '/nointernet',
  cache : false,
      templateUrl: 'templates/nointernet.html',
      controller : 'noloactionCtrl'
}) 

// If none of the above states are matched, use this as the fallback

$urlRouterProvider.otherwise('/mapview');




});



/**
* Bilingual details
* both laguages keyword define here.
*/

app.config(function($translateProvider) {

 $translateProvider.translations('en', {
  lang:'EN',
  lang1:'DU',
  Sign_Up:"Sign Up",
  Sign_In:"Sign In",
  Forgot_Password:"Forgot Password",
  Select_interest:"Select interest",
  Welcome:"Welcome",
  Map_View:"Map View",
  List_View:"List View",
  Logout:"Logout",
  Distance:"Distance",
  Forgot_Password:"Forgot Password",
  Reset:"Reset",
  Claim_now:"Claim now",
  Book_Now:"Book Now",
  Time_left:"TIME LEFT",
  Confirm_booking:"Confirm Booking",
  Confirmed:"Confirmed",
  Book_Now:"Book now",
  Time_Left:"Time Left",
  Confirm:"Confirm",
  Deal_Confirmed:"Deal Confirmed",
  no_deals:"No deals are available.",
  Email:"Email",
  Password:"Password",
  Name:"Name",
  Phone_no:"Phone number",
  Type_of_cuisine:"Type of cuisine",
  Additional_detail:"Additional detail",
  Reset_Password:"Reset Password",
  Retype_password:"Retype password",
  Please_enter_name:"Please enter name",
  name_sign_contain:"Name contain at least 3 characters",
  name_only_chars:"Name contain only characters",
  valid_email:"Please enter valid email address",
  valid_mobile:"Mobile no must be 10 digits",
  password_req:"Please enter password",
  password_mst:"Password must be 8 to 15 characters long",
  select_mst:"Please select your favorite type of cuisine",
  both_password:"Password must same in both field",
  valid_otp:"Please use valid otp",
  Enter_email:"Enter your email address",
  Try_again:"Try again",
  location_system:"Your location service is turn off, please turn it ON and try again.",
  gps_sytem:"Please turn on GPS!",
  no_internate:"Please turn on internet.",
  Data_head:"Internet unavailable",
  internet_off:"Your internet service is turn off, please turn it ON and try again.",
  fb_In:"Sign in with Facebook",
  fb_up:"Sign up with Facebook",
  Booking:"Booking",
  Only:"Only",
  deals_left:"deals left",
  total_deals:"You selected total deals",
  one_deal:"1 deal is available",
  Price:"Price",
  search_deal:"Search deals",
  enter_ur_address:"Enter your address"
  
});

 $translateProvider.translations('dt', {
  lang:'DU',
  lang1:'EN',
  Sign_Up:"Registreren",
  Sign_In:"Registreren",
  Forgot_Password:"Wachtwoord vergeten",
  Select_interest:"Select rente",
  Welcome:"Welkom",
  Map_View:"Kaart bekijken",
  List_View:"Lijstweergave",
  Logout:"Uitloggen",
  Distance:"Afstand",
  Forgot_Password:"Wachtwoord vergeten",
  Reset:"Reset",
  Claim_now:"Claim nu",
  Book_Now:"Claim deal",
  Time_left:"TIJD OVER",
  Confirm_booking:"Bevestigen",
  Confirmed:"Bevestigd",
  Book_Now:"Claim deal",
  Time_Left:"Tijd over",
  Confirm:"Bevestigen",
  Deal_Confirmed:"Deal bevestigd",
  no_deals:"Helaas, er zijn momenteel geen deals beschikbaar",
  Email:"E-mail",
  Password:"Wachtwoord",
  Name:"Naam",
  Phone_no:"Telefoonnummer",
  Type_of_cuisine:"Soort keuken",
  Additional_detail:"Extra informatie",
  Reset_Password:"Wachtwoord reset",
  Retype_password:"Geef nogmaals het wachtwoord",
  Please_enter_name:"Vul naam",
  name_sign_contain:"Naam ten minste 3 karakters",
  name_only_chars:"Naam bevat alleen tekens",
  valid_email:"Vul alstublieft een geldig e-mailadres in",
  valid_mobile:"Mobile niet moet 10 cijfers",
  password_req:"Voer wachtwoord in alstublieft",
  password_mst:"Het wachtwoord moet 8 tot 15 tekens lang zijn",
  select_mst:"Selecteer uw favoriete soort keuken",
  both_password:"Wachtwoord moet hetzelfde in beide gebied",
  valid_otp:"Gebruik een geldig otp",
  Enter_email:"Vul je e-mail adres in",
  Try_again:"Probeer het nog eens",
  location_system:"Uw locatie dienst uit te schakelen , gelieve zet hem op en probeer het opnieuw.",
  gps_sytem:"Schakel over GPS !",
  no_internate:"Schakel op het internet.",
  Data_head:"Internet beschikbaar",
  internet_off:"Uw internet service is uit te schakelen , gelieve zet hem op en probeer het opnieuw.",
  fb_In:"Meld je aan met Facebook",
  fb_up:"Aanmelden met Facebook",
  Booking:"Deal boeking",
  Only:"NOG",
  deals_left:"DEALS BESCHIKBAAR",
  total_deals:"Je selecteerde in totaal aanbiedingen",
  one_deal:"1 sale beschikbaar",
  Price:"Prijs",  
  search_deal:"Deals zoeken",
  enter_ur_address:"Vul uw adres in"
 });

 $translateProvider.preferredLanguage('en');
 $translateProvider.fallbackLanguage('en');

});




var app=angular.module('Restoranto', ['ionic', 'deepBlue.controllers', 'deepBlue.services','ngCordovaOauth','ngCordova','deepBlue.route','deepBlue.validation','deepBlue.mapservice','deepBlue.ionicLazyLoad','pascalprecht.translate','ion-place-tools','timer'])

.run(function($ionicPlatform, $rootScope, $timeout, $state,loginService, $cordovaStatusbar,$cordovaGeolocation,mapService,$cordovaNetwork, $ionicPopup,$translate,$cordovaBadge,dealService,$cordovaPush) {

  $rootScope.viewtype='';
  $rootScope.deals=[];
  $rootScope.forgetid='';
  $rootScope.scrolval=false;  
  $rootScope.userdata={};
  $rootScope.timeronoff=false;  
  $rootScope.setfocus=false;
  $rootScope.dealcount=0;
  $rootScope.backgroundScan=false;

/**
* : $ionicPlatform.ready 
*  setting when device is ready.
*/
$ionicPlatform.ready(function() {
  window.localStorage.setItem("device_type",ionic.Platform.platform());
  // navigator.splashscreen.hide()
  // Disable BACK button on home
  $ionicPlatform.registerBackButtonAction(function(event) {
    if ($state.current.name==='app.listview' || $state.current.name==='app.mapview' || $state.current.name==='signin' || $state.current.name==='signup') {
      $ionicPopup.confirm({
        title: 'Restoranto',
        template: 'are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
    }
  }, 100);
   // check user is logged or not is logged then set userdata to global variable.
   var loggedu=JSON.parse(loginService.get('userdata'));
   if(loggedu){
        $rootScope.userdata=loggedu;
        $state.go('app.mapview');
      }else{
        if(window.localStorage.getItem('lang')==null){
         $state.go('language');
       }else{
        $state.go('signin');
      }
   }

  $rootScope.pushSet=function(){
     var config = {
            "senderID": "603562739715"
        };
     $cordovaPush.register(config).then(function(deviceToken) {

     });

    $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
      console.log("device_token :" +notification.regid);
       window.localStorage.setItem("device_token",notification.regid);
         switch(notification.event) {
        case 'registered':
          if (notification.regid.length > 0 ) {
          }
          break;

        case 'message':
          // this is the actual push notification. its format depends on the data model from the push server
          $ionicPopup.alert({
         title: 'Restoranto',
         template: notification.message
       });
          break;

        case 'error':
        $ionicPopup.alert({
         title: 'Restoranto',
         template: notification.msg
       });
          break;

        default:
        $ionicPopup.alert({
         title: 'Restoranto',
         template: 'An unknown GCM event has occurred'
       });
          break;
      }

      if (notification.alert) {
       $ionicPopup.alert({
         title: 'Restoranto',
         template: notification.alert
       });
     }

     if (notification.sound) {
      var snd = new Media(event.sound);
      snd.play();
    }
  });
}

$rootScope.pushSet();

// window.localStorage.removeItem('lang')

    if(window.localStorage.getItem('lang')==='dt'){
      window.localStorage.setItem('lang','dt');
       $translate.use('dt');
    }else if(window.localStorage.getItem('lang')==='en'){
      window.localStorage.setItem('lang','en');
       $translate.use('en');
    }

  $rootScope.timeronoff=false;  
  /**
  * : $cordovaNetwork plugin to check network connection is on or off. 
  *  
  */
  $rootScope.isInternet=function(){
   if(!$cordovaNetwork.isOnline())
   {
       $state.go('nointernet');
     return false;
   }else{
    return true;
   }

 }



/** code for bade icon on homescreen of mobile.  **/
 $rootScope.cdeals={};
$rootScope.curentlatlng={};
$rootScope.userCurrentLatlongBagde=function(){
  if($rootScope.isInternet()){
     mapService.getUserLocation().then(function(data){
      $rootScope.curentlatlng=data;
      $rootScope.chkDeal();
    });
  }
}

$rootScope.chkDeal=function(){
 var count=0;
  $rootScope.backgroundScan=false;
    dealService.getDeals($rootScope.curentlatlng).then( function(data){
      if (data)
       if(data.status!=='Failed'){
        $rootScope.cdeals=data;
        for (i = 0; i < $rootScope.cdeals.length; i++){
          if($rootScope.cdeals[i].offer)
         for(var j=0;j<$rootScope.cdeals[i].offer.deal.length;j++){  
           count++;
        }
      } 
      $rootScope.setBagdeicon(count);
      $rootScope.backgroundScan=true; 
    } 
    
  }); 
    
}


$rootScope.setBagdeicon=function(count){
    cordova.plugins.notification.badge.set(count);
     $rootScope.dealcount=count;
}
// this called as soon as app in background

cordova.plugins.backgroundMode.setDefaults({
     silent: true
})
cordova.plugins.backgroundMode.enable(); // enable background service 
 cordova.plugins.backgroundMode.onactivate = function(){
  $rootScope.backgroundScan=true;
    setInterval(function(){
      count=0;
      if($rootScope.backgroundScan===true){
          $rootScope.userCurrentLatlongBagde();
       }
     }, 30000);
}

// after app init, this get called
cordova.plugins.backgroundMode.ondeactivate = function(){
  $rootScope.backgroundScan=false;
  cordova.plugins.notification.badge.clear();
};


/****  badge icon code end   */




      $cordovaStatusbar.styleColor('black'); // status bar background color.
     // set default view of users first screen.
      $rootScope.viewtype=window.localStorage.getItem('viewtype');
      if($rootScope.viewtype===null || $rootScope.viewtype===''){
        $rootScope.viewtype='app.mapview';
      }

     






      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.disableScroll(false);
       }
      ionic.Platform.isFullScreen = true;
  });

})



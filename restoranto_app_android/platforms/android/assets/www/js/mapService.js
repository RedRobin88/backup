
  angular.module('deepBlue.mapservice', [])

.factory('mapService', ['$http','$q','$cordovaGeolocation','$state','$ionicLoading', 'loginService', function($http,$q,$cordovaGeolocation,$state,$ionicLoading,loginService){

  return{
  /**
* :getUserLocation function  get current location of user.
* 
* return : lat long
*/
      getUserLocation:function(data){    
       
        var def = $q.defer();
       options = { timeout:10000,enableHighAccuracy: true };
        $cordovaGeolocation.getCurrentPosition(options).then(function(pos) {
        var latlng={
          'lat':pos.coords.latitude,
          'lng':pos.coords.longitude
        }
        window.localStorage.setItem('location',latlng);
        def.resolve(latlng);

      },function(err){
       $ionicLoading.hide();
       $state.go('noloaction',{msg:err.message});
      });
        return def.promise;
      }


  };  

}])
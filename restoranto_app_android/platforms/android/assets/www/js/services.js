

angular.module('deepBlue.services', [])

  // manage logged user data mgt....

  .factory('loginService', [ '$http','$q', function ($http,$q) {
// getter setter for login
return {

  set:function(key,value){
    return window.localStorage.setItem(key,value);
    
  },

  get:function(key){
    return window.localStorage.getItem(key);
    
  },

  destroy:function(key){
    return window.localStorage.removeItem(key);
  },

  login:function(data){
    var def = $q.defer();  
    var fd = new FormData();
    fd.append("email", data.email);
    fd.append("password", data.password);
    fd.append("device_token", window.localStorage.getItem('device_token'));
    fd.append("device_type", window.localStorage.getItem('device_type'));
    $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
    $http.post(base_url+'login',fd).success(function(data){
     def.resolve(data);
   }).error(function(data){
     def.resolve(data);
   });
   return def.promise;
 },

/**
* :forgot function  request OTP in mail from server.
* 
* return : status
*/
forgot:function(email){
 var def = $q.defer();
 var fd = new FormData();
 fd.append("email",email);
 $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
 $http.post(base_url+'forgotPassword',fd).success(function(data){
   def.resolve(data);
 }).error(function(data){
   def.resolve(data);
 });
 return def.promise;
},

/**
* :setPassword function  request to change password.
* 
* return : status
*/
setPassword:function(data){
 var def = $q.defer();
 var fd = new FormData();
 fd.append("password", data.password);
 fd.append("otp", data.otp);
 $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
 $http.post(base_url+'setPassword',fd).success(function(data){
   def.resolve(data);
 }).error(function(data){
   def.resolve(data);
 });
 return def.promise;
}

};

}])




  .factory('signupservice', ['$http','$q',function ($http,$q) {

    return{

    // get intrest list form API
    showIntrest:function(){
     var def = $q.defer();
     var fd = new FormData();
     fd.append("lang", window.localStorage.getItem('lang'));
     $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
     $http.post(base_url+'getInterest',fd).success(function(data){
       def.resolve(data);
     }).error(function(data){
       def.resolve(data);
     });
     return def.promise;
   },

    // sign up form submition
    signup:function(data){
      var def = $q.defer();
      var fd = new FormData();
      fd.append("name", data.name);
      fd.append("phoneno", data.phoneno);
      fd.append("email", data.email);
      fd.append("password", data.password);
      fd.append("interest", data.interest);
      fd.append("usertype", data.usertype);
      fd.append("lang", window.localStorage.getItem('lang'));
      fd.append("device_token", window.localStorage.getItem('device_token'));
      fd.append("device_type", window.localStorage.getItem('device_type'));
      $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
      $http.post(base_url+'createUser',fd).success(function(data){
       def.resolve(data);
     }).error(function(data){
       def.resolve(data);
     });
     return def.promise;
   },
   
   // check user already exits or not. for facebook login.
   check_mail:function(email){
    var def = $q.defer();
    var fd = new FormData();
    fd.append("email",email);
    $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
    $http.post(base_url+'check_mail',fd).success(function(data){
     def.resolve(data);
   }).error(function(data){
     def.resolve(data);
   });
   return def.promise;

 }

};


}])


  .factory('dealService', ['$http','$q',  function($http,$q){

    return{


/**
* :getDeals function deal list from server.
* 
* return : status and deal list
*/
getDeals:function(data){
 var def = $q.defer();
 var fd = new FormData();
       // fd.append("userid", data.userid);
       fd.append("lat", data.lat);
       fd.append("lng", data.lng);
       fd.append("interestid", data.interestid);
       fd.append("lang", window.localStorage.getItem('lang'));
       $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
       $http.post(base_url+'showDeals',fd).success(function(data){
         def.resolve(data);
       }).error(function(data){
         def.resolve(data);
       });
       return def.promise;
     } ,

     /**
* :claimedDeal function set deal for current user.
* 
* return : status 
*/

claimedDeal:function(data){
 var def = $q.defer();
 var fd = new FormData();
 fd.append("deal_id", data.deal_id);
 fd.append("userid", data.userid);
 fd.append("no_of_people", data.no_of_people);
 fd.append("lang", window.localStorage.getItem('lang'));
        // console.log(JSON.stringify(data));
        $http.defaults.headers.post={ 'X-Api-Token':'Eq57dwypZaFW4f2xxRzFaGjwCYinOn6l13Mvds00P2ZzgdMPTk'};
        $http.post(base_url+'claimedDeal',fd).success(function(data){
         def.resolve(data);
       }).error(function(data){
         def.resolve(data);
       });
       return def.promise;
     }
   };  

 }])
